

@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
       @include('layouts.sidebar')
        
        <div class="col-md-10">
            <div class="container mt-5">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Create  Starry Eyes User</div>
    
                            <div class="card-body">
                                <form method="POST"  id="custom-form" action="{{ route('starry.update', $user->id) }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                            
                                    <div class="mb-3">
                                        <label for="name" class="form-label">Name</label>
                                        <input type="text" name="name" value="{{ $user->name }}" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="description" class="form-label">Description</label>
                                     <!--    <input type="text" name="description" value="{{ $user->description }}" class="form-control" > -->
                                        <textarea name="description" class="form-control" style="overflow: auto;" rows="4" cols="40">{{ $user->description }}</textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="mobile" class="form-label">Mobile</label>
                                        <input type="text" name="mobile" value="{{ $user->mobile }}" class="form-control">
                                    </div>
                            
                                    <div class="mb-3">
                                        <label for="email" class="form-label">Email</label>
                                        <input type="text" name="email" value="{{ $user->email }}" class="form-control">
                                    </div>
                                 
                            
                                    <div class="mb-3">
                                        <label>Current Avatar</label><br>
                                        @php
                                            $imagePath = $user->media()->where('collection_name', 'avatar')->first();
                                            $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                                        @endphp
                                        @if ($imageUrl)
                                            <img src="{{ $imageUrl }}" alt="Current Avatar" style="max-width: 100px">
                                        @else
                                            <p>No avatar available</p>
                                        @endif
                                    </div>
                                    
                                    
                                    <div class="mb-3">
                                        <label for="avatar" class="form-label">Avatar</label>
                                        <input type="file" name="avatar" class="form-control">
                                    </div>
                            
                                    <!-- Display the current avatar image -->
                            
                            
                                    <div class="mb-3">
                                        <label>Current image 1</label><br>
                                        @php
                                            $imagePath = $user->media()->where('collection_name', 'user-image1')->first();
                                            $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                                        @endphp
                                        @if ($imageUrl)
                                            <img src="{{ $imageUrl }}" alt="Current image 1" style="max-width: 100px">
                                        @else
                                            <p>No avatar available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="image1" class="form-label">Image 1</label>
                                        <input type="file" name="image1" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label>Current image 2</label><br>
                                        @php
                                            $imagePath = $user->media()->where('collection_name', 'user-image2')->first();
                                            $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                                        @endphp
                                        @if ($imageUrl)
                                            <img src="{{ $imageUrl }}" alt="Current image 2" style="max-width: 100px">
                                        @else
                                            <p>No avatar available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="image2" class="form-label">Image 2</label>
                                        <input type="file" name="image2" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label>Current image 3</label><br>
                                        @php
                                            $imagePath = $user->media()->where('collection_name', 'user-image3')->first();
                                            $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                                        @endphp
                                        @if ($imageUrl)
                                            <img src="{{ $imageUrl }}" alt="Current image3" style="max-width: 100px">
                                        @else
                                            <p>No avatar available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="imag3" class="form-label">Image 3</label>
                                        <input type="file" name="image3" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label>Current image 4</label><br>
                                        @php
                                            $imagePath = $user->media()->where('collection_name', 'user-image4')->first();
                                            $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                                        @endphp
                                        @if ($imageUrl)
                                            <img src="{{ $imageUrl }}" alt="Current image 4" style="max-width: 100px">
                                        @else
                                            <p>No avatar available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="image4" class="form-label">Image 4</label>
                                        <input type="file" name="image4" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label>Current image 5</label><br>
                                        @php
                                            $imagePath = $user->media()->where('collection_name', 'user-image5')->first();
                                            $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                                        @endphp
                                        @if ($imageUrl)
                                            <img src="{{ $imageUrl }}" alt="Current image 5" style="max-width: 100px">
                                        @else
                                            <p>No avatar available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="image5" class="form-label">Image 5</label>
                                        <input type="file" name="image5" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label>Current image 6</label><br>
                                        @php
                                            $imagePath = $user->media()->where('collection_name', 'user-image6')->first();
                                            $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                                        @endphp
                                        @if ($imageUrl)
                                            <img src="{{ $imageUrl }}" alt="Current image 6" style="max-width: 100px">
                                        @else
                                            <p>No avatar available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="image6" class="form-label">Image 6</label>
                                        <input type="file" name="image6" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label>Current image 7</label><br>
                                        @php
                                            $imagePath = $user->media()->where('collection_name', 'user-image7')->first();
                                            $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                                        @endphp
                                        @if ($imageUrl)
                                            <img src="{{ $imageUrl }}" alt="Current image 7" style="max-width: 100px">
                                        @else
                                            <p>No avatar available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="image7" class="form-label">Image 7</label>
                                        <input type="file" name="image7" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label>Current Video1</label><br>
                                        @php
                                        $videoPath = $user->media()->where('collection_name', 'user-video1')->first();
                                        $videoUrl = $videoPath ? $videoPath->getUrl() : null;
                                        @endphp
                                        @if ($videoUrl)
                                            <img src="{{ $videoUrl }}" alt="Current video1" style="max-width: 100px">
                                        @else
                                            <p>No video1 available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="video1" class="form-label">Video1</label>
                                        <input type="file" name="video1" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label>Current Video2</label><br>
                                        @php
                                        $videoPath = $user->media()->where('collection_name', 'user-video2')->first();
                                        $videoUrl = $videoPath ? $videoPath->getUrl() : null;
                                        @endphp
                                        @if ($videoUrl)
                                            <img src="{{ $videoUrl }}" alt="Current video2" style="max-width: 100px">
                                        @else
                                            <p>No video2 available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="video2" class="form-label">Video2</label>
                                        <input type="file" name="video2" value="{{ $user->video2 }}" class="form-control">
                                    </div>
                            
                                    <div class="mb-3">
                                        <label>Current Video3</label><br>
                                        @php
                                        $videoPath = $user->media()->where('collection_name', 'user-video3')->first();
                                        $videoUrl = $videoPath ? $videoPath->getUrl() : null;
                                        @endphp
                                        @if ($videoUrl)
                                            <img src="{{ $videoUrl }}" alt="Current video3" style="max-width: 100px">
                                        @else
                                            <p>No video3 available</p>
                                        @endif
                                    </div>
                                    <div class="mb-3">
                                        <label for="video3" class="form-label">Video3</label>
                                        <input type="file" name="video3" value="{{ $user->video3 }}" class="form-control">
                                    </div>
                            
                                    <div class="mb-3">
                                        <label for="gender" class="form-label">Gender</label>
                                        <select name="gender" class="form-control">
                                            <option value="male" {{ $user->gender === 'male' ? 'selected' : '' }}>Male</option>
                                            <option value="female" {{ $user->gender === 'female' ? 'selected' : '' }}>Female</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="address" class="form-label">Address</label>
                                        <input type="text" name="address" value="{{ $user->address }}" class="form-control">
                                    </div>
                                    {{-- <div class="mb-3">
                                        <label for="country" class="form-label">Country</label>
                                        <input type="text" name="country" value="{{ $user->country }}" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="state" class="form-label">State</label>
                                        <input type="text" name="state" value="{{ $user->state }}" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="city" class="form-label">City</label>
                                        <input type="text" name="city" value="{{ $user->city }}" class="form-control">
                                    </div> --}}
                                    <div class="mb-3">
                                        <label for="facebook_link" class="form-label">Facebook Link</label>
                                        <input type="text" name="facebook_link" value="{{ $user->facebook_link }}" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="instagram_link" class="form-label">Instagram Link</label>
                                        <input type="text" name="instagram_link" value="{{ $user->instagram_link }}" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="linkedIn_link" class="form-label">LinkedIn Link</label>
                                        <input type="text" name="linkedIn_link" value="{{ $user->linkedIn_link }}" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="wikipedia_link" class="form-label">Wikipedia Link</label>
                                        <input type="text" name="wikipedia_link" value="{{ $user->wikipedia_link }}" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="imdb_link" class="form-label">IMDb Link</label>
                                        <input type="text" name="imdb_link" value="{{ $user->imdb_link }}" class="form-control">
                                    </div>
                            
                                                              
                                  <div class="mb-3">
    <label for="subscription_id">Subscription Plan</label>
    <select name="subscription_id" class="form-control" id="subscriptionSelect">
        <option value="20" {{ ($user->subscription_id === 20) ? 'selected' : '' }}>
            SILVER MEMBERSHIP
        </option>
        <option value="21" {{ ($user->subscription_id === 21) ? 'selected' : '' }}>
            GOLD MEMBERSHIP
        </option>
        <option value="22" {{ ($user->subscription_id === 22) ? 'selected' : '' }}>
            PLATINUM MEMBERSHIP
        </option>
        <option value="24" {{ ($user->subscription_id === 24) ? 'selected' : '' }}>
            STARRY EYED
        </option>
    </select>
</div>


                                 <div class="mb-3">
    <label for="color_code" class="form-label">Color Code</label>
    <input type="text" name="color_code" id="colorCodeInput" value="{{ $user->color_code }}" class="form-control" readonly>
</div>
                            
                            
                                    <div class="form-group">
                                        <label for="category">Category:</label>
                                        <select id="category" name="category[]" multiple class="form-control">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" @if (in_array($category->id, $categoryIds)) selected @endif>
                                                    {{ $category->title }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                            
                            
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $("#category").select2({
        allowClear: true
    });
</script>


<script>
    const subscriptionSelect = document.getElementById('subscriptionSelect');
    const colorCodeInput = document.getElementById('colorCodeInput');
    
    subscriptionSelect.addEventListener('change', function() {
        const selectedOption = subscriptionSelect.options[subscriptionSelect.selectedIndex].value;
        switch (selectedOption) {
            case '20': // Silver Membership
                colorCodeInput.value = 'ffefefef';
                break;
            case '21': // Gold Membership
                colorCodeInput.value = 'ffd9a321';
                break;
            case '22': // Platinum Membership
                colorCodeInput.value = 'ff0d44f1';
                break;
            case '24': // Starry Eyed
                colorCodeInput.value = 'fffc3838';
                break;
            default:
                colorCodeInput.value = ''; // Clear if none selected
                break;
        }
    });
</script>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection
