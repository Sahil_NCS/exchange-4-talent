
@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
       @include('layouts.sidebar')
        
        <div class="col-md-10">
            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Create User</div>
                            <div class="card-body">
                                <form method="POST" id="custom-form" action="{{ route('starry.create') }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('post')
                                    <div class="mb-3">
                                        <label for="name" class="form-label">Name</label>
                                        <input type="text" name="name"  class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="description" class="form-label">Description</label>
                                        <textarea name="description" class="form-control" style="overflow: auto;" rows="4" cols="40"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="mobile" class="form-label">Mobile</label>
                                        <input type="text" name="mobile"  class="form-control">
                                    </div>
                            
                                    <div class="mb-3">
                                        <label for="email" class="form-label">Email</label>
                                        <input type="text" name="email"  class="form-control">
                                    </div>
                            
                                    
                                    <div class="mb-3">
                                        <label for="avatar" class="form-label">Avatar</label>
                                        <input type="file" name="avatar" class="form-control">
                                    </div>
                            
                                    <!-- Display the current avatar image -->
                            
                            
                               
                                    <div class="mb-3">
                                        <label for="image1" class="form-label">Image1</label>
                                        <input type="file" name="image1" class="form-control">
                                    </div>
                            
                            
                             
                                    <div class="mb-3">
                                        <label for="image2" class="form-label">Image2</label>
                                        <input type="file" name="image2" class="form-control">
                                    </div>
                            
                            
                                 
                                    <div class="mb-3">
                                        <label for="image3" class="form-label">Image3</label>
                                        <input type="file" name="image3"  class="form-control">
                                    </div>
                            
                            
                                 
                                    <div class="mb-3">
                                        <label for="image4" class="form-label">Image4</label>
                                        <input type="file" name="image4" class="form-control">
                                    </div>
                            
                            
                               
                                    <div class="mb-3">
                                        <label for="image5" class="form-label">Image5</label>
                                        <input type="file" name="image5" class="form-control">
                                    </div>
                            
                            
                                
                                    <div class="mb-3">
                                        <label for="image6" class="form-label">Image6</label>
                                        <input type="file" name="image6" class="form-control">
                                    </div>
                            
                            
                               
                                    <div class="mb-3">
                                        <label for="image7" class="form-label">Image7</label>
                                        <input type="file" name="image7" class="form-control">
                                    </div>
                            
                            
                             
                                    <div class="mb-3">
                                        <label for="video1" class="form-label">Video1</label>
                                        <input type="file" name="video1" class="form-control">
                                    </div>
                            
                            
                                    <div class="mb-3">
                                        <label for="video2" class="form-label">Video2</label>
                                        <input type="file" name="video2"  class="form-control">
                                    </div>
                            
                                    
                              
                                    <div class="mb-3">
                                        <label for="video3" class="form-label">Video3</label>
                                        <input type="file" name="video3"  class="form-control">
                                    </div>
                            
                                    <div class="mb-3">
                                        <label for="gender" class="form-label">Gender</label>
                                        <select name="gender" class="form-control">
                                            <option value="Male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="address" class="form-label">Address</label>
                                        <input type="text" name="address"  class="form-control">
                                    </div>
                                    {{-- <div class="mb-3">
                                        <label for="country" class="form-label">Country</label>
                                        <input type="text" name="country" }}" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="state" class="form-label">State</label>
                                        <input type="text" name="state" " class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="city" class="form-label">City</label>
                                        <input type="text" name="city"  class="form-control">
                                    </div> --}}
                                    <div class="mb-3">
                                        <label for="facebook_link" class="form-label">Facebook Link</label>
                                        <input type="text" name="facebook_link"  class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="instagram_link" class="form-label">Instagram Link</label>
                                        <input type="text" name="instagram_link"  class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="linkedIn_link" class="form-label">LinkedIn Link</label>
                                        <input type="text" name="linkedIn_link"  class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="wikipedia_link" class="form-label">Wikipedia Link</label>
                                        <input type="text" name="wikipedia_link"  class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="imdb_link" class="form-label">IMDb Link</label>
                                        <input type="text" name="imdb_link"  class="form-control" >
                                    </div>
                            
                                                             
                                                                    <div class="mb-3">
   <!--  <label for="subscription_id">Subscription Plan</label> -->
    <input type="hidden" name="subscription_id" id="colorCodeInput" value ="24" class="form-control">
</div>
      
                                                         
                                 <div class="mb-3">
    <!-- <label for="color_code" class="form-label">Color Code</label> -->
    <input type="hidden" name="color_code" id="colorCodeInput" value ="fffc3838" class="form-control">
</div>
        
                            
                                    <div class="form-group">
                                        <label for="category">{{ __('Category') }}</label>
                                        {{-- <input id="category" type="text" class="form-control @error('category') is-invalid @enderror" name="category" value="{{ old('category') }}"> --}}
                                        <label for="category">Category:</label>
                                        <select id="category" name="category[]" multiple class="form-control">
                                            <option value=1>Actors</option>
                                            <option value="2">Anchor</option>
                                            <option value="3">Animal Performer</option>
                                            <option value="4">Animators</option>
                                            <option value="5">Astrologer</option>
                                            <option value="6">Athletes</option>
                                            <option value="7">Authors</option>
                                            <option value="8">Bartenders</option>
                                            <option value="9">Bloggers</option>
                                            <option value="10">Carpenters</option>
                                            <option value="11">Carpet Designers</option>
                                            <option value="12">Casting Directors</option>
                                            <option value="13">Chefs</option>
                                            <option value="14">Child Artist</option>
                                            <option value="15">Choreographers</option>
                                            <option value="16">Comedians</option>
                                            <option value="17">Content Creators</option>
                                            <option value="18">Costume Designers</option>
                                            <option value="19">Dancers</option>
                                            <option value="20">Dermat</option>
                                            <option value="21">Designers</option>
                                            <option value="22">Dj / Rj</option>
                                            <option value="23">Dog Whisperer</option>
                                            <option value="24">Dubbing Artists</option>
                                            <option value="25">Editors</option>
                                            <option value="26">Event Planners</option>
                                            <option value="27">Fashion Designers</option>
                                            <option value="28">Film Directors</option>
                                            <option value="29">Gamer</option>
                                            <option value="30">Graphic Designers</option>
                                            <option value="31">Gym Trainers</option>
                                            <option value="32">Hairstylist</option>
                                            <option value="33">Handloom Products Maker</option>
                                            <option value="34">Healer</option>
                                            <option value="35">Influencer</option>
                                            <option value="36">Interior Designers</option>
                                            <option value="37">Intimacy Coordinator</option>
                                            <option value="38">Illustrators</option>
                                            <option value="39">Magicians</option>
                                            <option value="40">Makeup Artists</option>
                                            <option value="41">Masseuse</option>
                                            <option value="42">Mimicry Artist</option>
                                            <option value="43">Models</option>
                                            <option value="44">Motivational Speakers</option>
                                            <option value="45">Musicians</option>
                                            <option value="46">Nail Artist</option>
                                            <option value="47">Numerologist</option>
                                            <option value="48">Painters</option>
                                            <option value="49">Palm Reader</option>
                                            <option value="50">Pets Trainer</option>
                                            <option value="51">Philanthropist</option>
                                            <option value="52">Photographers</option>
                                            <option value="53">Podcasters</option>
                                            <option value="54">Potter</option>
                                            <option value="55">Psychiatrist</option>
                                            <option value="56">Public Speaker</option>
                                            <option value="57">Reiki Master</option>
                                            <option value="58">Scuba Diver</option>
                                            <option value="59">Set Designers</option>
                                            <option value="60">Sfx Artist</option>
                                            <option value="61">Singers</option>
                                            <option value="62">Snorkeling</option>
                                            <option value="63">Sports Coach</option>
                                            <option value="64">Stunt Directors</option>
                                            <option value="65">Swimmer</option>
                                            <option value="66">Tarrot Card Reader</option>
                                            <option value="67">Tattoo Artist</option>
                                            <option value="68">Teacher / Professor</option>
                                            <option value="69">Tech Guru</option>
                                            <option value="70">Vastu Experts</option>
                                            <option value="71">Voice Over Artists</option>
                                            <option value="72">Web Designer</option>
                                            <option value="73">Wrestler / Boxer</option>
                                            <option value="74">Writers</option>
                                            <option value="75">Yoga Instructors</option>
                                            
                                        </select>
                            
                                        @error('                        category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            
                            
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $("#category").select2({
        allowClear: true
    });
</script>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection
