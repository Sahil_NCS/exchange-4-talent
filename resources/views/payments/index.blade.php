
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.sidebar')
            <div class="col-md-9">
                <h2>Payments</h2>
                <table class="table table-bordered" id="payments-table" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User ID</th>
                            <th>Subscription ID</th>
                            <th>Order ID</th>
                            <th>Tracking ID</th>
                            <th>Bank Ref No</th>
                            <th>Order Status</th>
                            <th>Payment Mode</th>
                            <th>Payment Type</th>
                            <th>Card Name</th>
                            <th>Status Code</th>
                            <th>Status Message</th>
                            <th>Currency</th>
                            <th>Billing Name</th>
                            <th>Billing Address</th>
                            <th>Billing State</th>
                            <th>Billing Zip</th>
                            <th>Billing Country</th>
                            <th>Billing Tel</th>
                            <th>Billing Email</th>
                            <th>Transaction Date</th>
                            <th>Token Eligibility</th>
                            <th>Response Code</th>
                            <th>Amount</th>
                            <th>CCavenue Status</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
    <script>
        $(function () {
            $('#payments-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('payments.index') }}",
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'user_id', name: 'user_id' },
                    { data: 'subscription_id', name: 'subscription_id' },
                    { data: 'order_id', name: 'order_id' },
                    { data: 'tracking_id', name: 'tracking_id' },
                    { data: 'bank_ref_no', name: 'bank_ref_no' },
                    { data: 'order_status', name: 'order_status' },
                    { data: 'payment_mode', name: 'payment_mode' },
                    { data: 'paymentType', name: 'paymentType' },
                    { data: 'card_name', name: 'card_name' },
                    { data: 'status_code', name: 'status_code' },
                    { data: 'status_message', name: 'status_message' },
                    { data: 'currency', name: 'currency' },
                    { data: 'billing_name', name: 'billing_name' },
                    { data: 'billing_address', name: 'billing_address' },
                    { data: 'billing_state', name: 'billing_state' },
                    { data: 'billing_zip', name: 'billing_zip' },
                    { data: 'billing_country', name: 'billing_country' },
                    { data: 'billing_tel', name: 'billing_tel' },
                    { data: 'billing_email', name: 'billing_email' },
                    { data: 'trans_date', name: 'trans_date' },
                    { data: 'token_eligibility', name: 'token_eligibility' },
                    { data: 'response_code', name: 'response_code' },
                    { data: 'amount', name: 'amount' },
                    { data: 'ccavenue_status', name: 'ccavenue_status' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' }
                ]
            });
        });
    </script>
@endsection


@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection