

@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
       @include('layouts.sidebar')




        <div class="col-md-10">
            <div class="container">
                <div class="card">
                    <div class="card-header">Create Category</div>
                    <div class="card-body">
                        <form action="{{ route('categories.store') }}"  method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label>Title:</label>
                                <input type="text" name="title" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea name="description" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Image:</label>
                                <input type="file" name="image" class="form-control-file">
                            </div>
                            <div class="form-group">
                               <!--  <label>Color Code:</label> -->
                                <input type="hidden" name="color_code" class="form-control" value="#FF6EC7">
                            </div>
                            <button type="submit" class="btn btn-primary">Create</button>
                            {{-- <a href="{{ route('categories.index') }}" class="btn btn-secondary">Cancel</a> --}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



        
    </div>
</div>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection
