
    
    @extends('layouts.app')

    @section('content')
    <div class="container-fluid">
        <div class="row ">
        @include('layouts.sidebar')
        <div class="col-md-10">
            <div class="d-flex justify-content-between">
        <h2>Categories</h2>
        <div> <a href="{{ route('categories.create') }}" class="btn btn-info">Create</a></div>
    </div>
         {{-- <a href="{{ route('home') }}" class="btn btn-info">Home</a> --}}
        <table class="table table-bordered" id="categoriesTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#categoriesTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('categories.index') }}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'image', name: 'image', orderable: false, searchable: false },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
            drawCallback: function(settings) {
                var api = this.api();
                var message = '{{ session('success') }}'; // Get the success message from session

                if (message) {
                    toastr.success(message); // Show success message as a pop-up using Toastr library
                }
            }
            });
        });
    </script>

@endsection


@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection