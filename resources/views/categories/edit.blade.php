@extends('layouts.app')

@section('content')
    
  
    <div class="container-fluid">
        <div class="row">
            @include('layouts.sidebar')



            <div class="col-md-10">
                <div class="container">
                    <div class="card">
                        <div class="card-header">Edit Category</div>
                        <div class="card-body">
                            <form action="{{ route('categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                    
                                <div class="form-group">
                                    <label>Title:</label>
                                    <input type="text" name="title" class="form-control" value="{{ $category->title }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Description:</label>
                                    <textarea name="description" class="form-control">{{ $category->description }}</textarea>
                                </div>
                                <div class="mb-3">
                                    <label>Current image</label><br>
                                    @if ($category->image)
                                        <?php
                                        // $imagePath = $category->image; // Get the image path from the database
                                        $imageUrl = $category->image;
                                        ?>
                                        <img src="{{ $imageUrl }}" alt="Current image" style="max-width: 100px">
                                    @else
                                        <p>No image available</p>
                                    @endif
                                </div>
                    
                                <div class="form-group">
                                    <label>Image:</label>
                                    <input type="file" name="image" class="form-control-file">
                                </div>
                                <div class="form-group">
                                    <!-- <label>Color Code:</label> -->
                                      <input type="hidden" name="color_code" class="form-control" value="#FF6EC7">
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                      
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>











        </div>
    </div>

@endsection


@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection