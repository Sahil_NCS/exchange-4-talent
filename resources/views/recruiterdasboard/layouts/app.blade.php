<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    @include('layouts.css')
    <style>
        .bg-dark {
            background-color: #151515 !important;
        }
    </style>
    @include('layouts.js')
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])




</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow p-0">
            <div class="container-fluid pl-0">

                <a class="navbar-brand py-0">
                    <img src="https://exchange4talent.s3.ap-south-1.amazonaws.com/Logo/E4T_100.png" alt="Logo" width="150">
                    {{-- {{ config('app.name', 'Laravel') }} --}}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <li class="nav-item">
                        <span class="navbar-text ml-3 text-white" style="margin-left: 99rem!important;"> {{ Auth::guard('recruiter')->user()->name }}</span>
                    </li>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>

</html>