<div class="col-auto">
            <div class="bg-light" id="sidebar-wrapper" style="margin-top:-20px;margin-bottom:20px;margin-left:-20px;">
                 <!-- Logo -->
                {{-- <div class="text-center py-4">

                    <img src="https://www.exchange4talentglobal.com/assets/img/E4T-Logo.png" alt="Logo" width="250">

                </div> --}}

                <!-- Sidebar Links -->
                <div class="list-group list-group-flush">
                    <a href="{{ route('clients.index') }}"class="list-group-item list-group-item-action bg-light">Recruiter Post</a>
                    <a href="{{ route('clients.getAppliedUser') }}"class="list-group-item list-group-item-action bg-light">Applied Users</a>
                    <a class="dropdown-item" href="{{ route('recruiter.logout') }}"
                    onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                   </a>

                    <form id="logout-form" action="{{ route('recruiter.logout') }}" method="POST" class="d-none">
                        
                        @csrf
                    </form>
                </div>
            </div>
        </div>