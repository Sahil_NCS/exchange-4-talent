@extends('recruiterdasboard.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('recruiterdasboard.layouts.sidebar')
            <!-- /#sidebar-wrapper -->
            <div class="col-md-9">
                <div class="d-flex justify-content-between">
                    <h1>Recruiter Post List</h1>
                    <div><a href="{{ route('clients.create') }}" class=" btn btn-info">Add Post</a></div>
                </div>
                <table class="table table-bordered data-table nowrap" style="width: 100%;">
                    <thead>
                        <tr>

                            <th>Id</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Location</th>
                            <th>Post Start Date</th>
                            <th>Post End Date</th>
                            <th>Audition Date</th>
                            <th width="100px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('clients.index') }}", // Use a named route
                columns: [
                    {
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'title',
                        name: 'title'
                    },
                    {
                        data: 'description',
                        name: 'description'
                    },
                    {
                        data: 'category_title',
                        name: 'category_title'
                    },
                    {
                        data: 'location',
                        name: 'location'
                    },

                    {
                        data: 'start_date',
                        name: 'start_date',
                        render: function(data, type, row) {
                            // Convert the data (assumed to be in UTC) to a local date string
                            const date = new Date(data);
                            const options = {
                                year: 'numeric',
                                month: '2-digit',
                                day: '2-digit'
                            };
                            return date.toLocaleDateString('en-IN', options);
                        }
                    }
                    ,
                    {
                        data: 'end_date',
                        name: 'end_date',
                        render: function(data, type, row) {
                            // Convert the data (assumed to be in UTC) to a local date string
                            const date = new Date(data);
                            const options = {
                                year: 'numeric',
                                month: '2-digit',
                                day: '2-digit'
                            };
                            return date.toLocaleDateString('en-IN', options);
                        }
                    }
                    ,
                    {
                        data: 'audition_date',
                        name: 'audition_date',
                        render: function(data, type, row) {
                            // Convert the data (assumed to be in UTC) to a local date string
                            const date = new Date(data);
                            const options = {
                                year: 'numeric',
                                month: '2-digit',
                                day: '2-digit'
                            };
                            return date.toLocaleDateString('en-IN', options);
                        }
                    },
                    // Add columns for 'Edit,' 'Delete,' and 'Status' buttons
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }
                ]
            });
        });
    </script>
    
@endsection

@section('styles')
    @include('recruiterdasboard.layouts.css')
@endsection

@section('scripts')
    @include('recruiterdasboard.layouts.js')
@endsection
