
@extends('recruiterdasboard.layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
       @include('recruiterdasboard.layouts.sidebar')
        
        <div class="col-md-9">
            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Clients </div>
                            <div class="card-body">
                                <form method="POST" id="custom-form" action="{{ route('clients.store') }}">
                                    @csrf
                                    @method('post')
                                    <div class="mb-3">
                                        <label for="title" class="form-label">Title</label>
                                        <input type="text" name="title"  class="form-control" required>
                                    </div>
                                    <div class="mb-3">
                                        <label for="description" class="form-label">Description</label>
                                        <textarea name="description" class="form-control" style="overflow: auto;" rows="4" cols="40" required></textarea>
                                    </div>

                                    <div class="mb-3">
                                        <label for="location" class="form-label">Location</label>
                                        <input name="location" class="form-control"  required>
                                    </div>
                            
                                    <div class="form-group">
                                        <label for="category">Category:</label>
                                        <select id="category" name="category[]" required multiple class="form-control">
                                            <option value=1>Actors</option>
                                            <option value="2">Anchor</option>
                                            <option value="3">Animal Performer</option>
                                            <option value="4">Animators</option>
                                            <option value="5">Astrologer</option>
                                            <option value="6">Athletes</option>
                                            <option value="7">Authors</option>
                                            <option value="8">Bartenders</option>
                                            <option value="9">Bloggers</option>
                                            <option value="10">Carpenters</option>
                                            <option value="11">Carpet Designers</option>
                                            <option value="12">Casting Directors</option>
                                            <option value="13">Chefs</option>
                                            <option value="14">Child Artist</option>
                                            <option value="15">Choreographers</option>
                                            <option value="16">Comedians</option>
                                            <option value="17">Content Creators</option>
                                            <option value="18">Costume Designers</option>
                                            <option value="19">Dancers</option>
                                            <option value="20">Dermat</option>
                                            <option value="21">Designers</option>
                                            <option value="22">Dj / Rj</option>
                                            <option value="23">Dog Whisperer</option>
                                            <option value="24">Dubbing Artists</option>
                                            <option value="25">Editors</option>
                                            <option value="26">Event Planners</option>
                                            <option value="27">Fashion Designers</option>
                                            <option value="28">Film Directors</option>
                                            <option value="29">Gamer</option>
                                            <option value="30">Graphic Designers</option>
                                            <option value="31">Gym Trainers</option>
                                            <option value="32">Hairstylist</option>
                                            <option value="33">Handloom Products Maker</option>
                                            <option value="34">Healer</option>
                                            <option value="35">Influencer</option>
                                            <option value="36">Interior Designers</option>
                                            <option value="37">Intimacy Coordinator</option>
                                            <option value="38">Illustrators</option>
                                            <option value="39">Magicians</option>
                                            <option value="40">Makeup Artists</option>
                                            <option value="41">Masseuse</option>
                                            <option value="42">Mimicry Artist</option>
                                            <option value="43">Models</option>
                                            <option value="44">Motivational Speakers</option>
                                            <option value="45">Musicians</option>
                                            <option value="46">Nail Artist</option>
                                            <option value="47">Numerologist</option>
                                            <option value="48">Painters</option>
                                            <option value="49">Palm Reader</option>
                                            <option value="50">Pets Trainer</option>
                                            <option value="51">Philanthropist</option>
                                            <option value="52">Photographers</option>
                                            <option value="53">Podcasters</option>
                                            <option value="54">Potter</option>
                                            <option value="55">Psychiatrist</option>
                                            <option value="56">Public Speaker</option>
                                            <option value="57">Reiki Master</option>
                                            <option value="58">Scuba Diver</option>
                                            <option value="59">Set Designers</option>
                                            <option value="60">Sfx Artist</option>
                                            <option value="61">Singers</option>
                                            <option value="62">Snorkeling</option>
                                            <option value="63">Sports Coach</option>
                                            <option value="64">Stunt Directors</option>
                                            <option value="65">Swimmer</option>
                                            <option value="66">Tarrot Card Reader</option>
                                            <option value="67">Tattoo Artist</option>
                                            <option value="68">Teacher / Professor</option>
                                            <option value="69">Tech Guru</option>
                                            <option value="70">Vastu Experts</option>
                                            <option value="71">Voice Over Artists</option>
                                            <option value="72">Web Designer</option>
                                            <option value="73">Wrestler / Boxer</option>
                                            <option value="74">Writers</option>
                                            <option value="75">Yoga Instructors</option>
                                            
                                        </select>
                            
                                        @error('                        category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            
                                <div class="mb-3">
                                    <label for="start_date" class="form-label">Post Start Date</label>
                                    <input type="date" name="start_date" id="start_date" class="form-control" style="overflow: auto;" required>
                                </div>
                                
                                <div class="mb-3">
                                    <label for="end_date" class="form-label">Post End Date</label>
                                    <input type="date" name="end_date" id="end_date" class="form-control" style="overflow: auto;" required>
                                </div>
                            

                                <div class="mb-3">
                                    <label for="audition_date" class="form-label">Audition Date</label>
                                    <input type="date" name="audition_date" class="form-control" style="overflow: auto;" required min="<?= date('Y-m-d'); ?>">
                                </div>
                                


                                    <button type="submit" class="btn btn-dark">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
    $("#category").select2({
        allowClear: true
    });
</script>

<script>
    $(document).ready(function () {
        // Get references to the start date and end date input elements
        var startDateInput = document.getElementById('start_date');
        var endDateInput = document.getElementById('end_date');

        // Get the current date
        var currentDate = new Date();
        var currentDateString = currentDate.toISOString().split('T')[0];

        // Set the minimum value of the start date input to the current date
        startDateInput.min = currentDateString;

        // Add an event listener to the start date input                     
        startDateInput.addEventListener('change', function () {
            // Get the selected start date
            var selectedStartDate = new Date(startDateInput.value);

            // Calculate the minimum value for the end date (one day after the selected start date)
            var minEndDate = new Date(selectedStartDate);
            minEndDate.setDate(minEndDate.getDate() + 1);

            // Set the minimum value of the end date input
            endDateInput.min = minEndDate.toISOString().split('T')[0];
        });
    });
</script>


@endsection

@section('styles')
    @include('recruiterdasboard.layouts.css')
@endsection

@section('scripts')
    @include('recruiterdasboard.layouts.js')
@endsection
