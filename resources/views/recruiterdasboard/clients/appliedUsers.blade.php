@extends('recruiterdasboard.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('recruiterdasboard.layouts.sidebar')
            <!-- /#sidebar-wrapper -->
            <div class="col-md-9">
                <div class="d-flex justify-content-between">
                    <h1>Applied Users</h1>
                    @if ($errors->has('error'))
                    <div class="alert alert-danger">
                        {{ $errors->first('error') }}
                    </div>
                    @endif

                </div>
                 
                <table class="table table-bordered data-table nowrap" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Applied Post(s)</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($appliedUsers as $user)
                            @foreach ($user->posts as $post)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->description }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->mobile }}</td>
                                    <td>{{ $post->title }}</td>
                                    <td>
                                        <button class="btn btn-success shortlist-btn" data-user-id="{{ $user->id }}" data-post-id="{{ $post->id }}">Shortlist</button>
                                        <button class="btn btn-success hire-btn" data-user-id="{{ $user->id }}" data-post-id="{{ $post->id }}">Hire</button>
                                        <button class="btn btn-danger reject-btn" data-user-id="{{ $user->id }}" data-post-id="{{ $post->id }}">Reject</button>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>

            
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function () {
        $('.data-table').DataTable();
    });
    </script>

<script>
        $(document).ready(function() {

            $('.shortlist-btn').click(function() {
                var userId = $(this).data('user-id');
                var postId = $(this).data('post-id');
                updateUserStatus(userId,postId, 'shortlist');
            });

            // Handle Hire button click
            $('.hire-btn').click(function() {
                var userId = $(this).data('user-id');
                var postId = $(this).data('post-id');
                updateUserStatus(userId,postId, 'hired');
            });

            // Handle Reject button click
            $('.reject-btn').click(function() {
                var userId = $(this).data('user-id');
                var postId = $(this).data('post-id');
                updateUserStatus(userId,postId, 'rejected');
            });


            function updateUserStatus(userId,postId, status) {
                // Send an AJAX request to update the user's status
                $.ajax({
                    url: '{{ route("updateUserStatus") }}', // Define your update status route
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}' // Include the CSRF token in the headers
                    },
                    data: {
                        userId: userId,
                        postId:postId,
                        status: status
                    },
                    success: function(response) {
                        // Handle success response (if needed)
                        console.log(response);
                    },
                    error: function(error) {
                        // Handle error (if needed)
                        console.error(error);
                    }
                });
            }
        });
</script>
    
@endsection

@section('styles')
    @include('recruiterdasboard.layouts.css')
@endsection

@section('scripts')
    @include('recruiterdasboard.layouts.js')
@endsection
