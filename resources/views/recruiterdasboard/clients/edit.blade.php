
@extends('recruiterdasboard.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
         @include('recruiterdasboard.layouts.sidebar')
            <div class="col-md-10">
                <div class="container">
                    <div class="card">
                        <div class="card-header">Edit Recruiter</div>
                        <div class="card-body">
                            <form action="{{ route('recruiters.update', ['recruiter' => $recruiter->id]) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="mb-3">
                                    <label for="title" class="form-label">Title</label>
                                    <input type="text" name="title"  class="form-control" required value="{{ $recruiter->title }}">
                                </div>
                                <div class="mb-3">
                                    <label for="description" class="form-label">Description</label>
                                    <textarea name="description" class="form-control" style="overflow: auto;" rows="4" cols="40" required>{{ $recruiter->description }}"</textarea>
                                </div>

                                
                                <div class="form-group">
                                    <label for="category">Category:</label>
                                    <select id="category" name="category[]" required multiple class="form-control">
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}" @if (in_array($category->id, $categoryIds)) selected @endif>
                                                {{ $category->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="mb-3">
                                    <label for="start_date" class="form-label">Post Start Date</label>
                                    <input type="date" name="start_date" id="start_date" class="form-control" style="overflow: auto;" required value="{{ $recruiter->start_date }}">
                                </div>
                                
                                <div class="mb-3">
                                    <label for="end_date" class="form-label">Post End Date</label>
                                    <input type="date" name="end_date" id="end_date" class="form-control" style="overflow: auto;" required value="{{ $recruiter->end_date }}">
                                </div>
                            

                                <div class="mb-3">
                                    <label for="audition_date" class="form-label">Audition Date</label>
                                    <input type="date" name="audition_date" class="form-control" style="overflow: auto;" required min="<?= date('Y-m-d'); ?>" value="{{ $recruiter->audition_date }}">
                                </div>
                                


                                <button type="submit" class="btn btn-primary">Update Recruiter</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    @include('recruiterdasboard.layouts.css')
@endsection

@section('scripts')
    @include('recruiterdasboard.layouts.js')
@endsection
