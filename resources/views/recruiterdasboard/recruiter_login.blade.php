


<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Exchange4Talent</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://www.exchange4talentglobal.com/assets/sass/style.css">
    <style>
        .vh-100 {
            height: 100vh;
        }

        .card {
            border: 0;
        }

        .right-logo {
            background-image: url(https://exchange4talentglobal.com/tms/public/images/login-logo.jpg);
            background-size: cover;
            width: 100%;
            height: 100%;
            border-radius: 0 4px 4px 0;
        }

        .btn-link {
            color: #bb8639;
        }
    </style>

</head>

<body class="">


    <section class="vh-100 d-flex align-items-center" style="background-image: url(https://exchange4talentglobal.com/tms/public/images/login-bg.jpg)">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="row justify-content-center">
                                <div class="col-md-7 p-5">
                                    <h3 class="text-center mb-4">{{ __('Login') }}</h3>
                                    @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                
                                
                                
                                
                                    <form class="form-horizontal" method="POST" action="{{ route('recruiter.login.post') }}">
                                        @csrf
                                    
                                        <div class="form-group">
                                            <label for="email" class="col-md-4">E-Mail</label>
                                    
                                            <div class="col-md-12">
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label for="password" class="col-md-4">Password</label>
                                    
                                            <div class="col-md-12">
                                                <input id="password" type="password" class="form-control" name="password" required>
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-dark">
                                                    Login
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                                <!-- / col  -->
                                <div class="col-md-5 ">
                                    <div class="right-logo"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>