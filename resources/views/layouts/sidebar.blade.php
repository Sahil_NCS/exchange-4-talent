<div class="col-auto">
            <div class="bg-light" id="sidebar-wrapper" style="margin-top:-20px;margin-bottom:20px;margin-left:-20px;">
                 <!-- Logo -->
                {{-- <div class="text-center py-4">

                    <img src="https://www.exchange4talentglobal.com/assets/img/E4T-Logo.png" alt="Logo" width="250">

                </div> --}}

                <!-- Sidebar Links -->
                <div class="list-group list-group-flush">


                   
                    <a href="{{ route('users.index') }}"class="list-group-item list-group-item-action bg-light">Users</a>
                    <a href="{{ route('silver.index') }}" class="list-group-item list-group-item-action bg-light">Silver User</a>
                     <a href="{{ route('gold.index') }}" class="list-group-item list-group-item-action bg-light">Gold User</a>
                     <a href="{{ route('platinum.index') }}" class="list-group-item list-group-item-action bg-light">Platinum User</a>
                         <a href="{{ route('starry.index') }}" class="list-group-item list-group-item-action bg-light">Starry Eyed User</a>
                    <a href="{{ route('categories.index') }}"class="list-group-item list-group-item-action bg-light">Categories</a>
                    <a href="{{ route('ads.index') }}" class="list-group-item list-group-item-action bg-light">Ads</a>
                    <a href="{{ route('subscription.index') }}"class="list-group-item list-group-item-action bg-light">Subscriptions</a>
                    <a href="{{ route('recruiters.index') }}"class="list-group-item list-group-item-action bg-light">Recruiters</a>
                    <a href="{{ route('coupons.index') }}"class="list-group-item list-group-item-action bg-light">Coupon</a>
                    <a href="{{ route('payments.index') }}"class="list-group-item list-group-item-action bg-light">Payment</a>




                    <a class="dropdown-item" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </div>
        </div>