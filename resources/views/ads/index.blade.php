@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
       @include('layouts.sidebar')
        <div class="col-md-9">
            <div class="d-flex justify-content-between">
            <h1>Ads List</h1>
           <div><a href="{{ route('ads.create') }}" class="btn btn-info">Add Ads</a></div>
        </div>

                            <!-- @if(session('success'))
                                <div class="alert alert-success alert-dismissible fade show bg-red" role="alert">
                                {{session('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                </button>
                                </div>
                            @endif -->

                            @if(session('success'))
                                <div class="alert alert-success alert-dismissible fade show bg-green" role="alert" id="error-alert">
                                    {{ session('success') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <script>
                                    // Automatically fade out the error alert after 5 seconds (5000 milliseconds)
                                    setTimeout(function() {
                                        $('#error-alert').fadeOut('slow');
                                    }, 3000);
                                </script>
                            @endif

            <table class="table table-bordered data-table nowrap" id="data-table" style="width: 100%;">
                <thead>
                    <tr>
                        <th>Sn No</th>
                         <th>Id</th>
                         <th>Name</th>
                        <th>Videos</th>
                        <th>Images</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('ads.index') }}",
            columns: [
                 { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'videos', name: 'videos' , orderable: false, searchable: false },
                { data: 'images', name: 'images' , orderable: false, searchable: false },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });
    });
</script>
@endsection


@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection