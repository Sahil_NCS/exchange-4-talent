

    @extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.sidebar')

        <div class="col-md-10">
            <div class="container py-5">
                <div class="row justify-content-center">
                    <div class="col-md-8 offset-md-2">
                        <div class="card">
                            <div class="card-header">Edit Ads</div>
                    
                        <div class="card-body">
                        <form method="POST" action="{{ route('ads.update', $ad->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')


                            <div class="mb-3">
                                <label for="name" class="form-label">Name</label>
                                <input type="text" name="name" value="{{ $ad->name }}" class="form-control" required>
                           </div>

                           <div class="mb-3">
                               <label>Current Video</label><br>
                               @php
                                   $videoPath = $ad->media()->where('collection_name', 'user-ads-video')->first();
                                   $videoUrl = $videoPath ? $videoPath->getUrl() : null;
                               @endphp
                               @if ($videoUrl)
                                   <video controls width="150">
                                       <source src="{{ $videoUrl }}" type="video/mp4">
                                   </video>
                               @else
                                   <p>No Video available</p>
                               @endif
                           </div>
                           


                           <div class="form-group">
                               <label for="videos">Videos</label>
                               <input type="file" name="videos" class="form-control" accept="video/*">
                           </div>


                           <div class="mb-3">
                               <label>Current image</label><br>
                               @php
                               $imagePath = $ad->media()->where('collection_name', 'user-ads-image')->first();
                               $imageUrl = $imagePath ? $imagePath->getUrl() : null;
                               @endphp
                               @if ($imageUrl)
                               <img src="{{$imageUrl}}" alt="Image" width="50" height="50">
                               @else
                                   <p>No Video available</p>
                               @endif
                           </div>
                           <div class="form-group">
                               <label for="images">Images</label>
                               <input type="file" name="images"  class="form-control" accept="image/*">
                           </div>

                            <button type="submit" class="btn btn-primary">Update Ad</button>
                        </form>
                    </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection
