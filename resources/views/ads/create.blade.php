@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
      @include('layouts.sidebar')
        
        <div class="col-md-10">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <div class="card">
                            <div class="card-header">Create New Ad</div>

                            @if(session('errormessage'))
                                <div class="alert alert-danger alert-dismissible fade show bg-red" role="alert" id="error-alert">
                                    {{ session('errormessage') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <script>
                                    // Automatically fade out the error alert after 5 seconds (5000 milliseconds)
                                    setTimeout(function() {
                                        $('#error-alert').fadeOut('slow');
                                    }, 3000);
                                </script>
                            @endif

                            <div class="card-body">
                                <form method="POST" action="{{ route('ads.store') }}" enctype="multipart/form-data">
                                    @csrf
        
                                    <div class="form-group">
                                        <label for="user_id">User Name</label>
                                        <select name="user_id" id="user_id" class="form-control">
                                            @foreach ($data as $user)
                                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endforeach
                                        </select>
                                     </div>

                                    <div class="form-group">
                                        <label for="videos">Videos</label>
                                        <input type="file" name="videos"  class="form-control" accept="video/*">
                                    </div>
        
                                    <div class="form-group">
                                        <label for="images">Images</label>
                                        <input type="file" name="images"  class="form-control" accept="image/*">
                                    </div>
        
                                    <button type="submit" class="btn btn-primary">Create Ad</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection


