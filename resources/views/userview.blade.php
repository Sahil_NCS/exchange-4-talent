<!DOCTYPE html>
<html>

<head>
    <title>Users Records</title>
    <!-- Add Bootstrap CSS link here -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f8f9fa;
            margin: 20px;
        }

        h1 {
            text-align: center;
            margin-bottom: 30px;
        }

        table {
            border: 1px solid #dee2e6;
            border-collapse: collapse;
            width: 100%;
            margin-top: 20px;
        }

        th,
        td {
            border: 1px solid #dee2e6;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        tbody tr:hover {
            background-color: #f8f9fa;
        }

        @media only screen and (max-width: 767px) {
            table {
                font-size: 12px;
            }
        }

        @media only screen and (max-width: 576px) {
            table {
                font-size: 10px;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <h1>Users Records</h1>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Categories</th>
                        <th>Subscription</th>
                        <th>Profile Photo</th>
                        <th>Photo 1</th>
                        <th>Photo 2</th>
                        <th>Video Url</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- Replace the PHP foreach loop with your actual data -->
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->mobile }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->category_title }}</td>
                        <td>
                            @if($user->subscription_id === 20)
                                SILVER
                            @elseif($user->subscription_id === 21)
                                GOLD
                            @elseif($user->subscription_id === 22)
                                PLATINUM
                            @elseif($user->subscription_id === 24)
                                STARRY EYED
                            @else
                                <!-- Handle other cases here -->
                                Unknown
                            @endif
                        </td>
                        <td>
                            @if ($user->avatar)
                                <img src="{{ $user->avatar }}"  width="200" height="200">
                            @else
                                No image uploaded
                            @endif
                        </td>
                                               
                        {{-- <td>{{ $user->image1 }}</td> --}}
                        {{-- <td>
                            <img src="{{ $user->image1 }}" alt="no image uploded" width="200" height="200">
                        </td>  --}}

                        <td>
                            @if ($user->image1)
                                <img src="{{ $user->image1 }}"  width="200" height="200">
                            @else
                                No image uploaded
                            @endif
                        </td>

                        {{-- <td>{{ $user->image2 }}</td> --}}
                        {{-- <td>
                            <img src="{{ $user->image2 }}" alt="no image uploded" width="200" height="200">
                        </td>  --}}

                        <td>
                            @if ($user->image2)
                            <img src="{{ $user->image2 }}"  width="200" height="200">
                            @else
                                No image uploaded
                            @endif
                        </td>
                        {{-- <td>{{ $user->video1 }}</td> --}}
                        {{-- <td>
                            <a href="{{ $user->video1 }}" target="_blank">{{ $user->video1 }}</a>
                        </td> --}}

                        <td>
                            @if ($user->video1)
                            <a href="{{ $user->video1 }}" target="_blank">{{ $user->video1 }}</a>
                            @else
                                No Video URL uploaded
                            @endif
                        </td>                      

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- Add Bootstrap JS and jQuery here (for some interactive features) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.7/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
