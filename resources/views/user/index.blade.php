
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            @include('layouts.sidebar')
            <!-- /#sidebar-wrapper -->
            <div class="col-md-9">
                <div class="d-flex justify-content-between">
        <h1>User List</h1>
        <div><a href="{{ route('userregisterform') }}" class=" btn btn-info">Add Create</a></div>
    </div>
        <table class="table table-bordered data-table nowrap" style="width: 100%;">
            <thead>
                <tr>
                    <th>Sn No</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th>Avatar</th>
                    <th>Category</th>
                    <th>Subscription</th>
                    <th>Date</th>
                    <th width="100px">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
            </div>
    </div>
</div>





    <script>
        $(document).ready(function() {
            var table = $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                // scrollX: true,
                ajax: "{{ url('/users') }}", // Replace with your route name or URL
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                     {
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'mobile',
                        name: 'mobile'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'avatar',
                        name: 'avatar',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, row) {
                            if (data) {
                                return '<img src="' + data + '" alt="Avatar" width="50">';
                            } else {
                                return '<p>No Avatar</p>';
                            }
                        }
                    },
                    {
                        
                        data: 'category_title',
                        name: 'category_title',
                        render: function(data, type, row) {
                            if (data) {
                                return '<p>'+ data +'</p>';
                            } else {
                                return '<p>No Category Selected</p>';
                            }
                        }
                    },

                    {
                        data: 'subscription_id',
                        name: 'subscription_id',
                        render: function(data, type, row) {
                        if (data === 20) {
                            return 'SILVER MEMBERSHIP';
                        } else if (data === 21) {
                            return 'GOLD MEMBERSHIP';
                        } else if (data === 22) {
                            return 'PLATINUM MEMBERSHIP';
                        } else if (data === 24) {
                            return 'STARRY EYED';
                        }else {
                            return 'NO MEMBERSHIP';
                        }
                    }
                    },
                     {
                    data: 'created_at',
                    name: 'created_at',
                    render: function(data, type, row) {
                        // Convert the data (assumed to be in UTC) to a local date string
                        const date = new Date(data);
                        const options = { year: 'numeric', month: 'long', day: 'numeric' };
                        return date.toLocaleDateString('en-US', options);
                    }
                },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });
    </script>




@endsection


@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection

