
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
         @include('layouts.sidebar')

            <div class="col-md-10">
                <div class="container">
                    <div class="card">
                        <div class="card-header">Edit Recruiter</div>
                        <div class="card-body">
                            <form action="{{ route('recruiters.update', ['recruiter' => $recruiter->id]) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control" name="name" value="{{ $recruiter->name }}" required>
                                </div>

                 <!--            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" name="email" value="{{ $recruiter->email }}" required>
                            </div>

                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" class="form-control" password="password" value="{{ $recruiter->password }}" required>
                            </div> -->
                                <div class="form-group">
                                    <label for="description">Description:</label>
                                    <textarea class="form-control" name="description" rows="4" required>{{ $recruiter->description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <!-- <label for="color_code">Color Code:</label> -->
                                    <input type="hidden" class="form-control" name="color_code" rows="4" value="5ebd60">
                                </div>
                                <div class="mb-3">
                                    <label>Current logo</label><br>
                                    @if ($recruiter->logo)
                                        <?php
                                        $imagePath = $recruiter->logo; // Get the image path from the database
                                        // $imageUrl = "https://e4t-users-images.s3.ap-south-1.amazonaws.com/{$imagePath}";
                                        ?>
                                        <img src="{{ $imagePath }}" alt="Current logo" style="max-width: 100px">
                                    @else
                                        <p>No logo available</p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="logo">Logo:</label>
                                    <input type="file" class="form-control-file" name="logo" accept="image/*">
                                </div>
                                <div class="form-group">
                                    <label for="url">URL:</label>
                                    <input type="text" class="form-control" name="url" value="{{ $recruiter->url }}">
                                </div>
                                <div class="form-group">
                                    <label for="link">Link:</label>
                                    <input type="text" class="form-control" name="link" value="{{ $recruiter->link }}">
                                </div>
                                <div class="form-group">
                                    <label for="type">Type:</label>
                                    <input type="text" class="form-control" name="type" value="{{ $recruiter->type }}">
                                </div>
                                <button type="submit" class="btn btn-primary">Update Recruiter</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection
