
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
       @include('layouts.sidebar')
            <div class="col-md-9">
                <div class="d-flex justify-content-between">
                <h2>Recruiters</h2>
            <div><a href="{{ route('recruiters.create') }}" class="btn btn-info">Add Recruiter</a></div>
                </div>
                <br><br>
                <table class="table table-bordered" id="recruiters-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Logo</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>URL</th>
                            <th>Link</th>
                            <th>Type</th>
                            <th width="100px">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
    <script>
        $(document).ready(function () {
            $('#recruiters-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('recruiters.index') }}',
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                    { data: 'logo', name: 'logo', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'description', name: 'description' },
                    { data: 'url', name: 'url' },
                    { data: 'link', name: 'link' },
                    { data: 'type', name: 'type' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });
    </script>
    
    @endsection


    @section('styles')
        @include('layouts.css')
    @endsection
    
    @section('scripts')
        @include('layouts.js')
    @endsection

