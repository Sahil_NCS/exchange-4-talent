

    @extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
       @include('layouts.sidebar')

        <div class="col-md-10">
            <div class="container">
                <div class="card">
                    <div class="card-header">Add New Recruiter</div>
                    <div class="card-body">
                        <form action="{{ route('recruiters.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>

                            <!-- <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>

                            <div class="form-group">
                                <label for="password">Password:</label>
                                <input type="password" class="form-control" password="password" required>
                            </div> -->

                            <div class="form-group">
                                <label for="description">Description:</label>
                                <textarea class="form-control" name="description" rows="4" required></textarea>
                            </div>
                            <div class="form-group">
                                <!-- <label for="color_code">Color Code:</label> -->
                                 <input type="hidden" class="form-control" name="color_code" rows="4" value="5ebd60">
                            </div>
                            <div class="form-group">
                                <label for="logo">Logo:</label>
                                <input type="file" class="form-control-file" name="logo" accept="image/*" required>
                            </div>
                            <div class="form-group">
                                <label for="url">URL:</label>
                                <input type="text" class="form-control" name="url">
                            </div>
                            <div class="form-group">
                                <label for="link">Link:</label>
                                <input type="text" class="form-control" name="link">
                            </div>
                            <div class="form-group">
                                <label for="type">Type:</label>
                                <input type="text" class="form-control" name="type">
                            </div>
                            <button type="submit" class="btn btn-primary">Add Recruiter</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection
