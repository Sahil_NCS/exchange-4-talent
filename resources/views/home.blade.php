

@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        @include('layouts.sidebar')
        
        <div class="col-md-9">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 mt-4">
                        <div class="card count shadow-none w-100 h-100 m-0 p-0" style="background-color: #bb8639;">
                            <div class="card-body">
                                <h5 class="card-title text-light">Total Users</h5>
                                <p class="card-text display-4 mb-0 text-light">{{ $userCount }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 mt-4">
                        <div class="card count shadow-none w-100 h-100 m-0 p-0" style="background-color: #2971ff;">
                            <div class="card-body">
                                <h5 class="card-title text-light">Total Categories</h5>
                                <p class="card-text display-4 mb-0 text-light">{{ $categoryCount }}</p>
                            </div>
                        </div>
                    </div>

                      <div class="col-md-3 mt-4">
                        <div class="card count shadow-none w-100 h-100 m-0 p-0" style="background-color: #de0c35;">
                            <div class="card-body">
                                <h5 class="card-title text-light">Total Recruiter</h5>
                                <p class="card-text display-4 mb-0 text-light">{{ $recruiterCount }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 mt-4">
                        <div class="card count shadow-none w-100 h-100 m-0 p-0" style="background-color: #bb8639;">
                            <div class="card-body">
                                <h5 class="card-title text-light">Total Success Payment</h5>
                                <p class="card-text display-4 mb-0 text-light">{{ $successCount }}</p>
                            </div>
                        </div>
                    </div>


                    <!-- Add more columns here if needed -->
                </div>
            </div>
        </div>
    </div>
</div>




        <!--         <div class="col-md-10">
           <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">{{ __('Dashboard') }}</div>

                            <div class="card-body">
                                @if (session('status'))
<div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
@endif

                                {{ __('You are logged in!') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->


@endsection
