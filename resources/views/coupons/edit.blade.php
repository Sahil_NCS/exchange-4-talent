
    @extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
       @include('layouts.sidebar')

        <div class="col-md-10">
            <div class="container">
                <div class="card">
                    <div class="card-header">Edit Coupon</div>
                    <div class="card-body">
                        <form action="{{ route('coupons.update', ['coupon' => $coupon->id]) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="coupon_code">Coupon Code</label>
                                <input type="text" name="coupon_code" class="form-control" value="{{ $coupon->coupon_code }}">
                            </div>
                            <div class="form-group">
                                <label for="coupon_discount">Coupon Discount</label>
                                <input type="number" name="coupon_discount" class="form-control" value="{{ $coupon->coupon_discount }}">
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection
