
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            @include('layouts.sidebar')
            <div class="col-md-9">
                <div class="d-flex justify-content-between">
                    <h1>Ads Coupon</h1>
                <div><a href="{{ route('coupons.create') }}" class="btn btn-info">Add Coupon</a></div>
            </div>
                <table class="table" id="data-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Coupon Code</th>
                            <th>Coupon Discount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
    <script>
        $(function () {
            $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('coupons.index') }}",
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'coupon_code', name: 'coupon_code' },
                    { data: 'coupon_discount', name: 'coupon_discount' },
                    { data: 'action', name: 'action' }
                ]
            });
        });
    </script>
@endsection


@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection