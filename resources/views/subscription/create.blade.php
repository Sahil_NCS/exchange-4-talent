
    @extends('layouts.app')

    @section('content')
    <div class="container-fluid">
        <div class="row">
           @include('layouts.sidebar')
    
            <div class="col-md-10">
                <div class="container">
                    <h2>Create Subscription Plan</h2>
                    <div class="card">
                        <div class="card-header">Create Subscription Plan</div>
                        <div class="card-body">
                            <form action="{{ route('subscription.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Plan</label>
                                    <input type="text" name="plan" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Validity</label>
                                    <input type="number" name="validity" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Images</label>
                                    <input type="file" name="images" class="form-control-file">
                                </div>
                                <div class="form-group">
                                    <label>Images Limit</label>
                                    <input type="number" name="images_limit" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Videos Limit</label>
                                    <input type="number" name="videos_limit" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Video Duration</label>
                                    <input type="number" name="video_duration" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Color Code</label>
                                    <input type="text" name="color_code" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Videos</label>
                                    <input type="file" name="videos" class="form-control-file">
                                </div>
                                <div class="form-group">
                                    <label>Contacts</label>
                                    <input type="number" name="contacts" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>View Email</label>
                                    <input type="checkbox" name="view_email">
                                </div>
                                <div class="form-group">
                                    <label>View Mobile</label>
                                    <input type="checkbox" name="view_mobile">
                                </div>
                                <div class="form-group">
                                    <label>Image Changes</label>
                                    <input type="number" name="image_changes" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Video Changes</label>
                                    <input type="number" name="video_changes" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Max Images Size</label>
                                    <input type="number" name="max_images_size" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Max Video Size</label>
                                    <input type="number" name="max_video_size" class="form-control">
                                </div>
                        
                                <button type="submit" class="btn btn-primary">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    
    @section('styles')
        @include('layouts.css')
    @endsection
    
    @section('scripts')
        @include('layouts.js')
    @endsection
    