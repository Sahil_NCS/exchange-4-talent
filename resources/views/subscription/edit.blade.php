@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        @include('layouts.sidebar')

        <div class="col-md-10">
            <div class="container">
                <h2>Edit Subscription Plan</h2>
                <div class="card">
                    <div class="card-header">Edit Subscription Plan</div>
                    <div class="card-body">
                        <form action="{{ route('subscription.update', $subscriptionPlan->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                    
                            <div class="form-group">
                                <label for="title">Title:</label>
                                <input type="text" class="form-control" id="title" name="title" value="{{ $subscriptionPlan->title }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="description">Description:</label>
                                <textarea class="form-control" id="description" name="description">{{ $subscriptionPlan->description }}</textarea>
                            </div>
                    
                            <div class="form-group">
                                <label for="price">Price:</label>
                                <input type="text" class="form-control" id="price" name="price" value="{{ $subscriptionPlan->price }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="plan">Plan:</label>
                                <input type="text" class="form-control" id="plan" name="plan" value="{{ $subscriptionPlan->plan }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="validity">Validity:</label>
                                <input type="text" class="form-control" id="validity" name="validity" value="{{ $subscriptionPlan->validity }}">
                            </div>
                    
                            <div class="mb-3">
                                <label>Current image</label><br>
                                @if ($subscriptionPlan->images)
                                    <?php
                                    $imagePath = $subscriptionPlan->image; // Get the image path from the database
                                    // $imageUrl = "https://exchange4talent.s3.ap-south-1.amazonaws.com/{$imagePath}";
                                    ?>
                                    <img src="{{ $imagePath }}" alt="Current image" style="max-width: 100px">
                                @else
                                    <p>No image available</p>
                                @endif
                            </div>
                
                            <div class="form-group">
                                <label for="images">Images:</label>
                                <input type="file" class="form-control" id="images" name="images">
                            </div>
                    
                            <div class="form-group">
                                <label for="images_limit">Images Limit:</label>
                                <input type="text" class="form-control" id="images_limit" name="images_limit" value="{{ $subscriptionPlan->images_limit }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="videos_limit">Videos Limit:</label>
                                <input type="text" class="form-control" id="videos_limit" name="videos_limit" value="{{ $subscriptionPlan->videos_limit }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="video_duration">Video Duration:</label>
                                <input type="text" class="form-control" id="video_duration" name="video_duration" value="{{ $subscriptionPlan->video_duration }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="color_code">Color Code:</label>
                                <input type="text" class="form-control" id="color_code" name="color_code" value="{{ $subscriptionPlan->color_code }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="videos">Videos:</label>
                                <input type="text" class="form-control" id="videos" name="videos" value="{{ $subscriptionPlan->videos }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="contacts">Contacts:</label>
                                <input type="text" class="form-control" id="contacts" name="contacts" value="{{ $subscriptionPlan->contacts }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="view_email">View Email:</label>
                                <input type="text" class="form-control" id="view_email" name="view_email" value="{{ $subscriptionPlan->view_email }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="view_mobile">View Mobile:</label>
                                <input type="text" class="form-control" id="view_mobile" name="view_mobile" value="{{ $subscriptionPlan->view_mobile }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="image_changes">Image Changes:</label>
                                <input type="text" class="form-control" id="image_changes" name="image_changes" value="{{ $subscriptionPlan->image_changes }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="video_changes">Video Changes:</label>
                                <input type="text" class="form-control" id="video_changes" name="video_changes" value="{{ $subscriptionPlan->video_changes }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="max_images_size">Max Images Size:</label>
                                <input type="text" class="form-control" id="max_images_size" name="max_images_size" value="{{ $subscriptionPlan->max_images_size }}">
                            </div>
                    
                            <div class="form-group">
                                <label for="max_video_size">Max Video Size:</label>
                                <input type="text" class="form-control" id="max_video_size" name="max_video_size" value="{{ $subscriptionPlan->max_video_size }}">
                            </div>
                    
                            <!-- Add other form fields for the remaining columns -->
                    
                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection
