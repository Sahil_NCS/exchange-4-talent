


@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
           @include('layouts.sidebar')
            <div class="col-md-9">
                <h2>Subscription Plans</h2>
                {{-- <a href="{{ route('subscription-plans.create') }}" class="btn btn-success mb-2">Create Subscription Plan</a> --}}
                <table class="table table-bordered data-table nowrap" id="subscription-plans-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Plan</th>
                            <th>Validity</th>
                            <th>Images</th>
                            <th>Images Limit</th>
                            <th>Videos Limit</th>
                            <th>Video Duration</th>
                            <th>Color Code</th>
                            <th>Videos</th>
                            <th>Contacts</th>
                            <th>View Email</th>
                            <th>View Mobile</th>
                            <th>Image Changes</th>
                            <th>Video Changes</th>
                            <th>Max Images Size</th>
                            <th>Max Video Size</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
    <script>
        $(function () {
            $('#subscription-plans-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('subscription.index') }}",
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'price', name: 'price' },
                    { data: 'plan', name: 'plan' },
                    { data: 'validity', name: 'validity' },
                    { data: 'images', name: 'images' , orderable: false, searchable: false },
                    { data: 'images_limit', name: 'images_limit' },
                    { data: 'videos_limit', name: 'videos_limit' },
                    { data: 'video_duration', name: 'video_duration' },
                    { data: 'color_code', name: 'color_code' },
                    { data: 'videos', name: 'videos' },
                    { data: 'contacts', name: 'contacts' },
                    { data: 'view_email', name: 'view_email' },
                    { data: 'view_mobile', name: 'view_mobile' },
                    { data: 'image_changes', name: 'image_changes' },
                    { data: 'video_changes', name: 'video_changes' },
                    { data: 'max_images_size', name: 'max_images_size' },
                    { data: 'max_video_size', name: 'max_video_size' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    { data: 'actions', name: 'actions', orderable: false, searchable: false }
                ]
            });
        });
    </script>



@endsection


@section('styles')
    @include('layouts.css')
@endsection

@section('scripts')
    @include('layouts.js')
@endsection

