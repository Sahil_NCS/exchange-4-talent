<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exchange4Talent</title>


    <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">


   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://www.exchange4talentglobal.com/assets/sass/style.css"> 
	<link rel="stylesheet" href="https://www.exchange4talentglobal.com/assets/css/custom.css"> </head>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .glow{
                background: #13171e;
                border: 8px #d1923b solid;
                box-shadow: 0px 0px 60px 10px #bb8639;
                border-style: ridge;
                border-radius: 40px;
                padding: 30px;
        }
        .form-group{
            margin-bottom: 20px;
        }
        #phone_code{
            font-size: 1.1rem;
            font-weight: 800;
        }
        .form-control, .form-control:focus, .select2-container--default .select2-selection--multiple{
            color: #ffffff;
            background-color: #222429;
            border-color: #343434;
                }
        .select2-container--default .select2-selection--multiple .select2-selection__choice{
                    background-color: #060303;
                }
        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
            color: #fff;
            left: auto;
            top: 0;
            right: 0;
            background-color: #939393;
        }
    </style>
</head>
<body class="dark-mode">
    


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card contact__four-form glow">
                {{-- <div class="card-header">{{ __('Register') }}</div> --}}

                <div class="card-body">
                    @if(session('msg'))
                        <div class="alert alert-danger">
                            {{ session('msg') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf
                    <div class="contact__four-form-title mb-4 w-100" style="max-width: 100% !important;">
                        <div class="row align-items-center justify-content-center text-center">
                            <div class="col-4">
                                <img src="https://www.exchange4talentglobal.com/assets/img/E4T-Logo.png">
                            </div>
                            <div class="col-12 mt-3">
                                <h4>Talent Registration Form</h4>
                            </div>
                        </div>
                        <div class="form-group mt-4">
                            <label for="name">{{ __('Name') }}</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="description">{{ __('Description') }}</label>
                            <textarea id="description" class="@error('description') is-invalid @enderror" name="description" rows="4" required>{{ old('description') }}</textarea>
                            @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        {{-- <div class="form-group">
                            <label for="mobile">{{ __('Mobile') }}</label>
                            <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required>
                            @error('mobile')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> --}}

                        <div class="form-group row">
                            {{-- <label for="phone_code" class="col-md-2 col-form-label">{{ __('Phone Code') }}</label> --}}
                            <label for="mobile" class="col-md-2 col-form-label">{{ __('Mobile') }}</label>
                            <div class="col-md-2">
                                <select id="phone_code" class="form-control @error('phone_code') is-invalid @enderror" name="phone_code" required style="height: 60px">
                                    <option value="">Select PhoneCode</option>
                                    @foreach($countries as $country)
                                        <option value="{{ $country->phonecode }}">{{ $country->phonecode }} {{ $country->iso3 }}</option>
                                    @endforeach
                                </select>
                                @error('phone_code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                           
                            <div class="col-md-8">
                                <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required>
                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="email">{{ __('Email') }}</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="profile_picture">{{ __('Profile Picture') }}</label>
                            <input id="profile_picture" type="file" class="form-control-file @error('profile_picture') is-invalid @enderror" name="profile_picture" required>
                            @error('profile_picture')
                                <span class="invalid-feedback" role="alert">
                                    <strong>The profile picture must be an image and below 5Mb (jpeg, png, gif).</strong>
                                </span>
                            @enderror
                        </div>

                     <!--    <div class="form-group">
                            <label for="date_of_birth">{{ __('Date of Birth') }}</label>
                            <input id="date_of_birth" type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" required>
                            @error('date_of_birth')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> -->

                        <div class="form-group">
                            <label>{{ __('Gender') }}</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="male" value="male" required>
                                <label class="form-check-label" for="male">{{ __('Male') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="female" value="female" required>
                                <label class="form-check-label" for="female">{{ __('Female') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="gender" id="other" value="other" required>
                                <label class="form-check-label" for="other">{{ __('Other') }}</label>
                            </div>
                            @error('gender')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="city">{{ __('City') }}</label>
                            <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required>
                            @error('city')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="state">{{ __('State') }}</label>
                            <input id="state" type="text" class="form-control @error('state') is-invalid @enderror" name="state" value="{{ old('state') }}" required>
                            @error('state')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="country">{{ __('Country') }}</label>
                            <input id="country" type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}" required>
                            @error('country')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label>{{ __('Barter') }}</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="barter" id="barter_yes" value="yes" required>
                                <label class="form-check-label" for="barter_yes">{{ __('Yes') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="barter" id="barter_no" value="no" required>
                                <label class="form-check-label" for="barter_no">{{ __('No') }}</label>
                            </div>
                            @error('barter')
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="form-group" id="barter_description_group" style="display: none;">
                            <label for="barter_description">{{ __('Trading for') }}</label>
                            <textarea id="barter_description" class="@error('barter_description') is-invalid @enderror" name="barter_description" rows="4" >{{ old('barter_description') }}</textarea>
                            @error('barter_description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


<!-- 
<select class="form-control" multiple="multiple">
  <option selected="selected">orange</option>
  <option>white</option>
  <option selected="selected">purple</option>
</select>

 -->

                    <div class="form-group">
                            <label for="category">{{ __('Category') }}</label>
                            {{-- <input id="category" type="text" class="form-control @error('category') is-invalid @enderror" name="category" value="{{ old('category') }}" required> --}}
                            <label for="category">Category:</label>
                            <select id="category" name="category[]" required multiple class="form-control">
                                <option value=1>Actors</option>
                                <option value="2">Anchor</option>
                                <option value="3">Animal Performer</option>
                                <option value="4">Animators</option>
                                <option value="5">Astrologer</option>
                                <option value="6">Athletes</option>
                                <option value="7">Authors</option>
                                <option value="8">Bartenders</option>
                                <option value="9">Bloggers</option>
                                <option value="10">Carpenters</option>
                                <option value="11">Carpet Designers</option>
                                <option value="12">Casting Directors</option>
                                <option value="13">Chefs</option>
                                <option value="14">Child Artist</option>
                                <option value="15">Choreographers</option>
                                <option value="16">Comedians</option>
                                <option value="17">Content Creators</option>
                                <option value="18">Costume Designers</option>
                                <option value="19">Dancers</option>
                                <option value="20">Dermat</option>
                                <option value="21">Designers</option>
                                <option value="22">Dj / Rj</option>
                                <option value="23">Dog Whisperer</option>
                                <option value="24">Dubbing Artists</option>
                                <option value="25">Editors</option>
                                <option value="26">Event Planners</option>
                                <option value="27">Fashion Designers</option>
                                <option value="28">Film Directors</option>
                                <option value="29">Gamer</option>
                                <option value="30">Graphic Designers</option>
                                <option value="31">Gym Trainers</option>
                                <option value="32">Hairstylist</option>
                                <option value="33">Handloom Products Maker</option>
                                <option value="34">Healer</option>
                                <option value="35">Influencer</option>
                                <option value="36">Interior Designers</option>
                                <option value="37">Intimacy Coordinator</option>
                                <option value="38">Illustrators</option>
                                <option value="39">Magicians</option>
                                <option value="40">Makeup Artists</option>
                                <option value="41">Masseuse</option>
                                <option value="42">Mimicry Artist</option>
                                <option value="43">Models</option>
                                <option value="44">Motivational Speakers</option>
                                <option value="45">Musicians</option>
                                <option value="46">Nail Artist</option>
                                <option value="47">Numerologist</option>
                                <option value="48">Painters</option>
                                <option value="49">Palm Reader</option>
                                <option value="50">Pets Trainer</option>
                                <option value="51">Philanthropist</option>
                                <option value="52">Photographers</option>
                                <option value="53">Podcasters</option>
                                <option value="54">Potter</option>
                                <option value="55">Psychiatrist</option>
                                <option value="56">Public Speaker</option>
                                <option value="57">Reiki Master</option>
                                <option value="58">Scuba Diver</option>
                                <option value="59">Set Designers</option>
                                <option value="60">Sfx Artist</option>
                                <option value="61">Singers</option>
                                <option value="62">Snorkeling</option>
                                <option value="63">Sports Coach</option>
                                <option value="64">Stunt Directors</option>
                                <option value="65">Swimmer</option>
                                <option value="66">Tarrot Card Reader</option>
                                <option value="67">Tattoo Artist</option>
                                <option value="68">Teacher / Professor</option>
                                <option value="69">Tech Guru</option>
                                <option value="70">Vastu Experts</option>
                                <option value="71">Voice Over Artists</option>
                                <option value="72">Web Designer</option>
                                <option value="73">Wrestler / Boxer</option>
                                <option value="74">Writers</option>
                                <option value="75">Yoga Instructors</option>
                                
                            </select>

                            @error('                        category')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="facebook_link">{{ __('Facebook Link') }}</label>
                        <input id="facebook_link" type="text" class="form-control @error('facebook_link') is-invalid @enderror" name="facebook_link" value="{{ old('facebook_link') }}">
                        @error('facebook_link')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="instagram">{{ __('Instagram') }}</label>
                        <input id="instagram" type="text" class="form-control @error('instagram') is-invalid @enderror" name="instagram" value="{{ old('instagram') }}">
                        @error('instagram')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="wikipedia">{{ __('Wikipedia') }}</label>
                        <input id="wikipedia" type="text" class="form-control @error('wikipedia') is-invalid @enderror" name="wikipedia" value="{{ old('wikipedia') }}">
                        @error('wikipedia')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="imdb">{{ __('IMDB') }}</label>
                        <input id="imdb" type="text" class="form-control @error('imdb') is-invalid @enderror" name="imdb" value="{{ old('imdb') }}">
                        @error('imdb')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="linkedin">{{ __('LinkedIn') }}</label>
                        <input id="linkedin" type="text" class="form-control @error('linkedin') is-invalid @enderror" name="linkedin" value="{{ old('linkedin') }}">
                        @error('linkedin')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="photo_1">{{ __('Photo 1') }}</label>
                        <input id="photo_1" type="file" class="form-control-file @error('photo_1') is-invalid @enderror" name="photo_1" required>
                        @error('photo_1')
                            <span class="invalid-feedback" role="alert">
                                <strong>The profile picture must be an image and below 2Mb (jpeg, png, gif).</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="photo_2">{{ __('Photo 2') }}</label>
                        <input id="photo_2" type="file" class="form-control-file @error('photo_2') is-invalid @enderror" name="photo_2" required>
                        @error('photo_2')
                            <span class="invalid-feedback" role="alert">
                                <strong>The profile picture must be an image and below 2Mb (jpeg, png, gif).</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="video_link">{{ __('Video - Enter Youtube Link to match your profile ( Only 1 Link )') }}</label>
                        <input id="video_link" type="text" class="form-control @error('video_link') is-invalid @enderror" name="video_link" value="{{ old('video_link') }}">
                        @error('video_link')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn-one">
                                {{ __('Register') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
      $("#category").select2({
          allowClear: true
      });


      $(document).ready(function() {
        $('input[name="barter"]').change(function() {
            if ($(this).val() === 'yes') {
                $('#barter_description_group').show();
            } else {
                $('#barter_description_group').hide();
            }
        });
    });
    </script>

</body>
</html>