<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendPushNotification extends Notification
{
    use Queueable;

    protected $title;
    protected $description;
    protected $userId;
    protected $type;
    protected $clientId;
    protected $fcmTokens;

    /**
     * Create a new notification instance.
     */
    public function __construct()
    {
        $this->title = $title;
        $this->message = $description;
        $this->message = $userId;
        $this->message = $type;
        $this->message = $clientId;
        $this->fcmTokens = $fcmTokens;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['firebase'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new FirebaseMessage)
            ->withTitle($this->title)
            ->withBody($this->description)
            ->withPriority('high')->asMessage($this->fcmTokens);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
