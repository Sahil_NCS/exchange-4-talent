<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
// use App\Models\Users;
// use Illuminate\Support\Facades\Validator;
// use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Country;
use App\Models\Media;
use DateTime;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use Illuminate\Support\Str;
use DB;

class RegisterController extends Controller
{

    public function showRegistrationForm(){
        $countries = Country::orderBy('name')->get();
    
        return view("registration", ['countries' => $countries]);
    }
   

    public function register(Request $request)
    {
        // return $request;
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'mobile' => 'required|unique:users',
            'profile_picture' => 'required|image|max:10240',
            'photo_1' => 'image|max:10240',
            'photo_2' => 'image|max:10240',
            // Add validation rules for other fields here
        ]);
    
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
    
        $user = new User();
        $user->name = $request->input('name');
        $user->phone_code = $request->input('phone_code');
        $user->mobile = $request->input('mobile');
        $user->description = $request->input('description');
        $user->email = $request->input('email');
    
        // Format date of birth
        // $user->dob = $request->input('date_of_birth');
        // $date = DateTime::createFromFormat('d-m-Y', $dateInput);
        // $user->dob = $date ? $date->format('Y-m-d') : null;
    
        // Convert talent_type and barter to integers
        // $user->talent_type = $request->input('talent_type', 0) ? 1 : 0;
        $user->trade_barter = $request->input('barter', 0) ? 1 : 0;
        $user->barter_description = $request->input('barter_description');
        $user->gender = $request->input('gender');
        $user->city = $request->input('city');
        $user->state = $request->input('state');
        $user->country = $request->input('country');
        // $user->categories = $request->input('category');
        // $user->category_id = $request->input('category');


       $jsonEncoded = $user->category_ids = json_encode($request->category);
// Decode JSON to an array
$categoryIds = json_decode($jsonEncoded);

// Convert the array to comma-separated string
$commaSeparated = implode(',', $categoryIds);

// Store the comma-separated string in your object's category_ids property
$user->category_ids = $commaSeparated;






// Decode JSON to an array
$jsonEncodeds = $user->category_title = json_encode($request->category);
$categoryIds = json_decode($jsonEncodeds);

// Assuming you have a 'Category' model
$categories = Category::whereIn('id', $categoryIds)->get();

// Initialize an array to store category titles
$categoryTitles = [];

// Loop through the fetched categories to extract titles
foreach ($categories as $category) {
    $categoryTitles[] = $category->title;
}

// Convert the titles array to a comma-separated string
$commaSeparatedTitles = implode(',', $categoryTitles);

// Store the comma-separated string in your object's category_title property
$user->category_title = $commaSeparatedTitles;
        
        
        if ($request->has('facebook_link')) {
            $user->facebook_link = $request->input('facebook_link');
        }
        if ($request->has('instagram')) {
            $user->facebook_link = $request->input('instagram');
        }
        if ($request->has('linkedin')) {
            $user->linkedIn_link = $request->input('linkedin');
        }
        if ($request->has('wikipedia')) {
            $user->wikipedia_link = $request->input('wikipedia');
        }
        if ($request->has('imdb')) {
            $user->imdb_link = $request->input('imdb');
        }
    
            // $users->instagram_link = $request->input('instagram');
            // $users->linkedIn_link = $request->input('linkedin');
            // $users->wikipedia_link = $request->input('wikipedia');
            // $users->imdb_link = $request->input('imdb');
        // Set other fields similarly
        
        // Handle image uploads
        // if ($request->hasFile('profile_picture')) {
        //     $user->avatar = $request->file('profile_picture')->store('bannerimage', 'public');
        // }

        if ($request->hasFile('profile_picture')) {
            $file = $request->file('profile_picture');
            // $imagePath = "images/" . $user->name . ' ' . $file->getClientOriginalName();

          

            // Storage::disk("s3")->put($imagePath, file_get_contents($file));

            // Storage::disk('s3')->setVisibility($imagePath,'public');
            
            // $url = Storage::disk("s3")->url($imagePath);


            // $user->avatar = $url;

            $media = $user->addMedia($file)->toMediaCollection('avatar', 's3');
   
        }


    
        // if ($request->hasFile('photo_1')) {
        //     $user->image1 = $request->file('photo_1')->store('bannerimage', 'public');
        // }

        if ($request->hasFile('photo_1')) {
            $file = $request->file('photo_1');
            // $imagePath = "images/" . $user->name . ' ' . $file->getClientOriginalName();

          

            // Storage::disk("s3")->put($imagePath, file_get_contents($file));

            // Storage::disk('s3')->setVisibility($imagePath,'public');
            
            // $url = Storage::disk("s3")->url($imagePath);


            // $user->image1 = $url;

            $media = $user->addMedia($file)->toMediaCollection('user-image1', 's3');
        
            
        }


    
        // if ($request->hasFile('photo_2')) {
        //     $user->image2 = $request->file('photo_2')->store('bannerimage', 'public');
        // }


        if ($request->hasFile('photo_2')) {
            $file = $request->file('photo_2');
            // $imagePath = "images/" . $user->name . ' ' . $file->getClientOriginalName();

          

            // Storage::disk("s3")->put($imagePath, file_get_contents($file));

            // Storage::disk('s3')->setVisibility($imagePath,'public');
            
            // $url = Storage::disk("s3")->url($imagePath);


            // $user->image2 = $url;
            $media = $user->addMedia($file)->toMediaCollection('user-image2', 's3');
            
        }


    
        $user->video1 = $request->input('video_link');
    
        $saved = $user->save();
        if(!$saved){
            // App::abort(500, 'Error');

            return redirect()->route('register')->with('msg', 'Somthing went wrong. Please try again!');
        }
        $name = $request->input('name');
        return view('thank_you',compact('name'));
        // Optionally, you can redirect the user to a success page or perform other actions here.


    }
    

    public function success()
{
    return view('success');
}

public function show(){

    $users = DB::select('select * from users');
    return view('userview',['users'=>$users]);
    
    
    }



}
