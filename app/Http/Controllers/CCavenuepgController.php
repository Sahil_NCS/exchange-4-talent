<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CCavenuepgController extends Controller
{
    public function index(){
        return view("CCavenue.index");
    }



    public function ccavRequestHandler(){

        $merchant_data='';
        $working_key='AE8D9E2A16C305D2973721EB6CF6A0C8';//Shared by CCAVENUES
        $access_code='AVZP95KH82AH54PZHA';//Shared by CCAVENUES

        foreach ($_POST as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }

        $encrypted_data=$this->encrypt($merchant_data,$working_key); // Method for encrypting the data.

        // return view('CCavenue.ccavRequestHandler', compact('encrypted_data', 'access_code'));
		return view('CCavenue.ccavRequestHandler', [
			'encrypted_data' => $encrypted_data,
			'access_code' => $access_code,
		]);
} 
    


    public function ccavResponseHandler(){


	
	$workingKey='AE8D9E2A16C305D2973721EB6CF6A0C8';		//Working Key should be provided here.
	$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
	$rcvdString=$this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
	$order_status="";
	$decryptValues=explode('&', $rcvdString);
	$dataSize=sizeof($decryptValues);

	for($i = 0; $i < $dataSize; $i++) 
	{
        $information = explode ( '=', $decryptValues [$i] );
        $responseMap [$information [0]] = $information [1];  
	}

    $order_status = $responseMap ['order_status'];
    
	// if($order_status=="Success")
	// {
	// 	echo "<br>Your Payment is Successfull. We will contact you soon with your request on mail <br> <a class='btn btn-success' href='https://exchange4talentglobal.com/CCAvenue/CCAvenue/'>Back to Home</a>";
		
	// }
	// else if($order_status=="Aborted")
	// {
	// 	echo "<br> Your Payment has Been Aborted <br> <a class='btn btn-primary' href='https://exchange4talentglobal.com/CCAvenue/CCAvenue/'>Retry</a> || <a class='btn btn-success' href='https://exchange4talentglobal.com/CCAvenue/CCAvenue/'>Switch to Main Page</a>";
	
	// }
	// else if($order_status==="Failure")
	// {
	// 	echo "<br>The transaction has been declined. <br> <a class='btn btn-success' href='https://exchange4talentglobal.com/CCAvenue/CCAvenue/'>Back to Home</a>";
	// }
	// else
	// {
	// 	echo "<br>Thank you for the payment. Your transaction is successful. <br> <a class='btn btn-success' href='https://exchange4talentglobal.com/CCAvenue/CCAvenue/'>Back to Home</a>";
	
	// }

    return view('CCavenue.ccavResponseHandler', compact('order_status'));

    }



    function encrypt($plainText,$key)
	{
		function hextobin($hexString) 
		{ 
			$length = strlen($hexString); 
			$binString = "";   
			$count = 0; 
			while ($count < $length) 
			{       
				$subString = substr($hexString, $count, 2);           
				$packedString = pack("H*", $subString); 
				if ($count == 0)
				{
					$binString = $packedString;
				} 
				else 
				{
					$binString .= $packedString;
				} 
				$count += 2; 
			} 
			return $binString; 
		}
	}

	function decrypt($encryptedText,$key)
	{
		$key = hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText = hextobin($encryptedText);
		$decryptedText = openssl_decrypt($encryptedText, 'AES-128-CBC', $key, OPENSSL_RAW_DATA, $initVector);
		return $decryptedText;
	}
	//*********** Padding Function *********************

	 function pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}

	//********** Hexadecimal to Binary function for php 4.0 version ********

	function hextobin($hexString) 
   	 { 
        	$length = strlen($hexString); 
        	$binString="";   
        	$count=0; 
        	while($count<$length) 
        	{       
        	    $subString =substr($hexString,$count,2);           
        	    $packedString = pack("H*",$subString); 
        	    if ($count==0)
		    {
				$binString=$packedString;
		    } 
        	    
		    else 
		    {
				$binString.=$packedString;
		    } 
        	    
		    $count+=2; 
        	} 
  	        return $binString; 
    	  } 

}
