<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;
use DataTables;
class CouponsController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $coupons = Coupon::select(['id', 'coupon_code', 'coupon_discount']);
            return DataTables::of($coupons)
                ->addColumn('action', function($coupon) {
                    $editRoute = route('coupons.edit', ['coupon' => $coupon->id]);
                    $deleteRoute = route('coupons.destroy', ['coupon' => $coupon->id]);

                    $actionBtn = '<a href="' . $editRoute . '" class="edit btn btn-success btn-sm">Edit</a>';
                    $actionBtn .= ' <a href="' . $deleteRoute . '" class="delete btn btn-danger btn-sm">Delete</a>';

                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('coupons.index');
    }

    public function create()
    {
        return view('coupons.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'coupon_code' => 'required|unique:coupons',
            'coupon_discount' => 'required|numeric',
        ]);

        Coupon::create([
            'coupon_code' => $request->coupon_code,
            'coupon_discount' => $request->coupon_discount,
        ]);

        return redirect()->route('coupons.index')->with('success', 'Coupon created successfully');
    }

    public function edit(Coupon $coupon)
    {
        return view('coupons.edit', compact('coupon'));
    }

    public function update(Request $request, Coupon $coupon)
    {
        $this->validate($request, [
            'coupon_code' => 'required|unique:coupons,coupon_code,' . $coupon->id,
            'coupon_discount' => 'required|numeric',
        ]);

        $coupon->update([
            'coupon_code' => $request->coupon_code,
            'coupon_discount' => $request->coupon_discount,
        ]);

        return redirect()->route('coupons.index')->with('success', 'Coupon updated successfully');
    }

    public function destroy(Coupon $coupon)
    {
        $coupon->delete();

        return redirect()->route('coupons.index')->with('success', 'Coupon deleted successfully');
    }
}
