<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use Aws\S3\S3Client;
use \DateTime;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;

class CategoryController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $categories = Category::select(['id', 'title', 'description', 'image', 'color_code', 'created_at', 'updated_at']);
            
            return DataTables::of($categories)
                ->addColumn('image', function($category) {
                    // $imageUrl = asset('https://exchange4talent.s3.ap-south-1.amazonaws.com/' . $category->image); 
                    $imageUrl = $category->image; 
                    return '<img src="' . $imageUrl . '" alt="Image" width="70" height="70">';
                })
                ->addColumn('action', function($category) {
                    $editRoute = route('categories.edit', ['category' => $category->id]);
                    $deleteRoute = route('categories.destroy', ['category' => $category->id]);
    
                    $actionBtn = '<a href="' . $editRoute . '" class="edit btn btn-success btn-sm">Edit</a>';
                    $actionBtn .= ' <a href="' . $deleteRoute . '" class="delete btn btn-danger btn-sm">Delete</a>';
                    
                    return $actionBtn;
                })
                ->rawColumns(['image', 'action'])
                ->toJson();
        }
    
        return view('categories.index');
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(Request $request)
    {
        $category = new Category;
        $category->title = $request->input('title');
        $category->description = $request->input('description');
        $category->color_code = $request->input('color_code');

        // Upload image to S3
        // if ($request->hasFile('image')) {
        //     $file = $request->file('image');
        //     $imagePath = "categories/" . time() . '_' . $file->getClientOriginalName();
        //     Storage::disk('s3')->put($imagePath, file_get_contents($file));
        //     $category->image = $imagePath;
        // }

if ($request->hasFile('image')) {
    $file = $request->file('image');
    $imagePath = "categories/" . time() . '_' . $file->getClientOriginalName();
    
    // Store the file on S3
    Storage::disk('s3')->put($imagePath, file_get_contents($file));
    
    // Create the full S3 URL
    $s3Url = 'https://exchange4talent.s3.ap-south-1.amazonaws.com/' . $imagePath;
    
    $category->image = $s3Url; // Store the full S3 URL in the database
}
        $category->save();

        return redirect()->route('categories.index')->with('success', 'Category created successfully');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->title = $request->input('title');
        $category->description = $request->input('description');
        $category->color_code = $request->input('color_code');

    //    // Delete the previous avatar if it exists
    //    if ($category->image) {
    //     // Extract the filename from the image URL
    //     $filename = basename($category->image);
    //     // Delete the previous image4 image from S3 storage
    //     Storage::disk("s3")->delete('categories/' . $filename);
    // }
    
    // if ($request->hasFile('image')) {
    //     $file = $request->file('image');
    //     $imagePath = "categories/" . time() . ' ' . $file->getClientOriginalName();
    
    //    Storage::disk("s3")->put($imagePath, file_get_contents($file));
    //     Storage::disk('s3')->setVisibility($imagePath, 'public');
    
    
    //     // Update the user's image field with the new URL
    //     $category->image = $imagePath;
 
    // }


        if ($category->image) {
    // Extract the filename from the image URL
    $filename = basename($category->image);
    // Delete the previous image from S3 storage
    Storage::disk("s3")->delete('categories/' . $filename);
}

if ($request->hasFile('image')) {
    $file = $request->file('image');
    $imagePath = "categories/" . time() . '_' . $file->getClientOriginalName();

    // Store the file on S3
    Storage::disk("s3")->put($imagePath, file_get_contents($file));
    Storage::disk('s3')->setVisibility($imagePath, 'public');

    // Create the full S3 URL
    $s3Url = 'https://exchange4talent.s3.ap-south-1.amazonaws.com/' . $imagePath;

    // Update the category's image field with the new URL
    $category->image = $s3Url;
}

        $category->save();

        return redirect()->route('categories.index')->with('success', 'Category updated successfully');
    }

    public function destroy($id)
    {
        $category = Category::find($id);

        if (!$category) {
            return redirect()->route('categories.index')->with('error', 'Category not found.');
        }

        // Delete the associated S3 image if it exists
        if ($category->image) {
            Storage::disk('s3')->delete($category->image);
        }

        $category->delete();

        return redirect()->route('categories.index')->with('success', 'Category and associated image deleted successfully.');
    }
}
