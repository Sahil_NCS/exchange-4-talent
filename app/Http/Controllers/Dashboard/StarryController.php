<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use Aws\S3\S3Client;
use \DateTime;
use App\Models\Media;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;
class StarryController extends Controller
{


    public function index(Request $request)
{

    if ($request->ajax()) {
        $subscriptionId = 24; // Replace this with the subscription ID you want to retrieve

      $data = User::where('subscription_id', $subscriptionId)->get();
  

        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('avatar', function($row) {
                $avatarMedia = $row->media()
                    ->where('collection_name', 'avatar')
                    ->first();
                $avatarUrl = $avatarMedia ? $avatarMedia->getUrl() : '';
                return $avatarUrl;
            })
            ->addColumn('image1', function($row) {
                // Retrieve the image1 URL from the 'image1' field
                $image1 = $row->media()
                ->where('collection_name', 'user-image1')
                ->first();
                $image1 = $image1 ? $image1->getUrl() : '';
                return $image1;
               
            })
            ->addColumn('image2', function($row) {
                // Retrieve the image2 URL from the 'image2' field
                $image2 = $row->media()
                ->where('collection_name', 'user-image2')
                ->first();
                $image2 = $image2 ? $image2->getUrl() : '';
                return $image2;
            })
            ->addColumn('image3', function($row) {
                // Retrieve the image3 URL from the 'image3' field
                $image3 = $row->media()
                ->where('collection_name', 'user-image3')
                ->first();
                $image3 = $image3 ? $image3->getUrl() : '';
                return $image3;
            })
            ->addColumn('image4', function($row) {
                // Retrieve the image4 URL from the 'image4' field
                $image4 = $row->media()
                ->where('collection_name', 'user-image4')
                ->first();
                $image4 = $image4 ? $image4->getUrl() : '';
                return $image4;
            })
            ->addColumn('image5', function($row) {
                // Retrieve the image5 URL from the 'image5' field
                $image5 = $row->media()
                ->where('collection_name', 'user-image5')
                ->first();
                $image5 = $image5 ? $image5->getUrl() : '';
                return $image5;
            })
            ->addColumn('image6', function($row) {
                // Retrieve the image6 URL from the 'image6' field
                $image6 = $row->media()
                ->where('collection_name', 'user-image6')
                ->first();
                $image6 = $image6 ? $image6->getUrl() : '';
                return $image6;
            })
            ->addColumn('image7', function($row) {
                // Retrieve the image7 URL from the 'image7' field
                $image7 = $row->media()
                ->where('collection_name', 'user-image7')
                ->first();
                $image7 = $image7 ? $image7->getUrl() : '';
                return $image7;
            })
            ->addColumn('video1', function($row) {
            // Retrieve the video1 URL from the 'video1' field
            return $row->video1;
        })
        ->addColumn('action', function($row){
            $editRoute = route('starry.edit', ['user' => $row->id]);
            $updateRoute = route('starry.destroy', ['users' => $row->id]);
            
            $actionBtn = '<a href="' . $editRoute . '" class="edit btn btn-success btn-sm">Edit</a>';
            $actionBtn .= ' <a href="' . $updateRoute . '" class="delete btn btn-danger btn-sm">Delete</a>';
            
            return $actionBtn;
        })
            ->rawColumns(['avatar', 'image1', 'image2','image3', 'image4','image5', 'image6','image7', 'video1', 'action'])
            ->make(true);
    }


    return view('starryeyed.index');
}


public function show(){
        return view("starryeyed.create");
    }



    public function create(Request $request)
    {

        $user = new User();
        $user->name = $request->input('name');
        $user->mobile = $request->input('mobile');
        $user->description = $request->input('description');
        $user->email = $request->input('email');
        $user->color_code = $request->input('color_code');
        $user->gender = $request->input('gender');
        $user->address = $request->input('address');
        $user->facebook_link = $request->input('facebook_link');
        $user->instagram_link = $request->input('instagram_link');
        $user->linkedIn_link = $request->input('linkedIn_link');
        $user->wikipedia_link = $request->input('wikipedia_link');
        $user->imdb_link = $request->input('imdb_link');

    
        // if ($dateInput) {
        //     // Convert the date input to 'yyyy-mm-dd' format
        //     $date = DateTime::createFromFormat('d-m-Y', $dateInput);
        //     if ($date !== false) {
        //         $formattedDate = $date->format('Y-m-d');
        //         $users->dob = $formattedDate;
        //     } else {
        //         // Handle invalid date input
        //         return response()->json(['status'=>400, 'error' => 'Invalid date format']);
        //         }
        //     } 
        // }
    
        // $users->category_id = $request->input('category_id');
        $jsonEncoded = $user->category_ids = json_encode($request->category);

        // Decode JSON to an array
        $categoryIds = json_decode($jsonEncoded);
        
        // Convert the array to comma-separated string
        $commaSeparated = implode(',', $categoryIds);
        
        // Store the comma-separated string in your object's category_ids property
        $user->category_ids = $commaSeparated;
        
        
        
        
        // Decode JSON to an array
        $jsonEncodeds = $user->category_title = json_encode($request->category);
        $categoryIds = json_decode($jsonEncodeds);
        
        // Assuming you have a 'Category' model
        $categories = Category::whereIn('id', $categoryIds)->get();
        
        // Initialize an array to store category titles
        $categoryTitles = [];
        
        // Loop through the fetched categories to extract titles
        foreach ($categories as $category) {
            $categoryTitles[] = $category->title;
        }
        
        // Convert the titles array to a comma-separated string
        $commaSeparatedTitles = implode(',', $categoryTitles);
        
        // Store the comma-separated string in your object's category_title property
        $user->category_title = $commaSeparatedTitles;
    
            
        
        // $user->subcategory_id = $request->input('subcategory_id');
        // $user->trade_barter = $request->input('trade_barter');
        $user->subscription_id = $request->input('subscription_id');
        // $user->subscription_validity = $request->input('subscription_validity');
        // $user->subscribed_on = $request->input('subscribed_on');
        // $user->booking_fee = $request->input('booking_fee');
     
    


    
    if ($request->hasFile('avatar')) {
        $file = $request->file('avatar');
    
        $media = $user->addMedia($file)->toMediaCollection('avatar', 's3');
        // Update the user's avatar field with the new URL
 
    }

    
      // Delete the previous avatar if it exists

    if ($request->hasFile('image1')) {
        $file = $request->file('image1');
        $media = $user->addMedia($file)->toMediaCollection('user-image1', 's3');
 
    }
    

    
    if ($request->hasFile('image2')) {
        $file = $request->file('image2');
        $media = $user->addMedia($file)->toMediaCollection('user-image2', 's3');
    }
    
        

    
    if ($request->hasFile('image3')) {
        $file = $request->file('image3');
        $media = $user->addMedia($file)->toMediaCollection('user-image3', 's3');
    }
    

        
 
    
    if ($request->hasFile('image4')) {
        $file = $request->file('image4');
        $media = $user->addMedia($file)->toMediaCollection('user-image4', 's3');
    }


        

    if ($request->hasFile('image5')) {
        $file = $request->file('image5');
        $media = $user->addMedia($file)->toMediaCollection('user-image5', 's3');
    }


    
    if ($request->hasFile('image6')) {
        $file = $request->file('image6');
        $media = $user->addMedia($file)->toMediaCollection('user-image6', 's3');
    }


        

    
    if ($request->hasFile('image7')) {
        $file = $request->file('image7');
        $media = $user->addMedia($file)->toMediaCollection('user-image7', 's3');
    }


        

    
    if ($request->hasFile('video1')) {
        $file = $request->file('video1');
        $media = $user->addMedia($file)->toMediaCollection('user-video1', 's3');
    }


        
        if ($request->hasFile('video2')) {
            $file = $request->file('video2');
            $media = $user->addMedia($file)->toMediaCollection('user-video2', 's3');
        }



    if ($request->hasFile('video3')) {
        $file = $request->file('video3');
        $media = $user->addMedia($file)->toMediaCollection('user-video3', 's3');
    }


        $user->save();
    
        // return response()->json(['status'=>200,'user' => $users]);
          return redirect()->route('starry.index')->with('success', 'User Create successfully');
    }




    public function edit(User $user)
    {
    $categories = Category::all(); // Retrieve all available categories

     $categoryIds = explode(',',$user->category_ids);

    return view('starryeyed.edit', compact('user', 'categories','categoryIds'));
        
    }


    public function update(Request $request, User $users)
    {

// return $request->avatar;

        // $users->firebase_id = $request->input('firebase_id');
        $firebase_id = $request->input('firebase_id');
        if ($firebase_id !== null) {
           $users->firebase_id = $firebase_id;
        }
        // $users->name = $request->input('name');
        $name = $request->input('name');
        if ($name !== null) {
           $users->name = $name;
        }
    
        $description = $request->input('description');
        if ($description !== null) {
           $users->description = $description;
        }
    
        $email = $request->input('email');
         if ($email !== null) {
            $users->email = $email;
        }
        $mobile = $request->input('mobile');
        if ($mobile !== null) {
           $users->mobile = $mobile;
       }
        // $users->email = $request->input('email');
        // $users->mobile_otp = $request->input('mobile_otp');
        // $mobile_otp = $request->input('mobile_otp');
        // if ($mobile_otp !== null) {
        //    $users->mobile_otp = $mobile_otp;
        // }
        // $users->email_otp = $request->input('email_otp');
        $email_otp = $request->input('email_otp');
        if ($email_otp !== null) {
           $users->email_otp = $email_otp;
        }
        // $users->mobile_verified = $request->input('mobile_verified');
        $mobile_verified = $request->input('mobile_verified');
        if ($mobile_verified !== null) {
           $users->mobile_verified = $mobile_verified;
        }
        // $users->email_verified = $request->input('email_verified');
        $email_verified = $request->input('email_verified');
        if ($email_verified !== null) {
           $users->email_verified = $email_verified;
        }

        $color_code = $request->input('color_code');
        if ($color_code !== null) {
           $users->color_code = $color_code;
        }
    
        $dateInput = $request->input('dob');
        if ($dateInput !== null) {
            $users->dob = $dateInput;
         
        if ($dateInput) {
            // Convert the date input to 'yyyy-mm-dd' format
            $date = DateTime::createFromFormat('d-m-Y', $dateInput);
            if ($date !== false) {
                $formattedDate = $date->format('Y-m-d');
                $users->dob = $formattedDate;
            } else {
                // Handle invalid date input
                return response()->json(['status'=>400, 'error' => 'Invalid date format']);
                }
            } 
        }
    
    
    
    
        
        // $talentType = $request->input('talent_type');
        $talent_type = $request->input('talent_type');
        if ($talent_type !== null) {
            $users->talent_type = $talent_type ? 1 : 0;
        }
        // Convert the boolean value to an integer
       
    
        // $users->gender = $request->input('gender');
        $gender = $request->input('gender');
        if ($gender !== null) {
           $users->gender = $gender;
        }
        // $users->address = $request->input('address');
        $address = $request->input('address');
        if ($address !== null) {
           $users->address = $address;
        }
    
        // $country = $request->input('country');
        // if ($country !== null) {
        //    $users->country = $country;
        // }
    
        // $state = $request->input('state');
        // if ($state !== null) {
        //    $users->state = $state;
        // }
    
        // $city = $request->input('city');
        // if ($city !== null) {
        //    $users->city = $city;
        // }
        
        // $users->password = $request->input('password');
        $password = $request->input('password');
        if ($password !== null) {
           $users->password = $password;
        }
    
    
        $facebook_link = $request->input('facebook_link');
        if ($facebook_link !== null) {
           $users->facebook_link = $facebook_link;
        }
    
        $instagram_link = $request->input('instagram_link');
        if ($instagram_link !== null) {
           $users->instagram_link = $instagram_link;
        }
    
    
        $linkedIn_link = $request->input('linkedIn_link');
        if ($linkedIn_link !== null) {
           $users->linkedIn_link = $linkedIn_link;
        }
    
    
        $wikipedia_link = $request->input('wikipedia_link');
        if ($wikipedia_link !== null) {
           $users->wikipedia_link = $wikipedia_link;
        }
    
    
        $imdb_link = $request->input('imdb_link');
        if ($imdb_link !== null) {
           $users->imdb_link = $imdb_link;
        }
    
    
        // $users->remember_token = $request->input('remember_token');
        $remember_token = $request->input('remember_token');
        if ($remember_token !== null) {
           $users->remember_token = $remember_token;
        }
        // $users->category_id = $request->input('category_id');
        $jsonEncoded = $users->category_ids = json_encode($request->category);

        // Decode JSON to an array
        $categoryIds = json_decode($jsonEncoded);
        
        // Convert the array to comma-separated string
        $commaSeparated = implode(',', $categoryIds);
        
        // Store the comma-separated string in your object's category_ids property
        $users->category_ids = $commaSeparated;
        
        
        
        
        
        
        // Decode JSON to an array
        $jsonEncodeds = $users->category_title = json_encode($request->category);
        $categoryIds = json_decode($jsonEncodeds);
        
        // Assuming you have a 'Category' model
        $categories = Category::whereIn('id', $categoryIds)->get();
        
        // Initialize an array to store category titles
        $categoryTitles = [];
        
        // Loop through the fetched categories to extract titles
        foreach ($categories as $category) {
            $categoryTitles[] = $category->title;
        }
        
        // Convert the titles array to a comma-separated string
        $commaSeparatedTitles = implode(',', $categoryTitles);
        
        // Store the comma-separated string in your object's category_title property
        $users->category_title = $commaSeparatedTitles;
    
            
            
             
    
    
    
            
        // $users->subcategory_id = $request->input('subcategory_id');
        $subcategory_id = $request->input('subcategory_id');
        if ($subcategory_id !== null) {
           $users->subcategory_id =(int) $subcategory_id;
        }
    
        // $users->trade_barter = $request->input('trade_barter');
        // $talent_barter = $request->input('trade_barter');
        $trade_barter = $request->input('trade_barter');
        if ($trade_barter !== null) {
            $users->trade_barter = $trade_barter ? 1 : 0;
        }
        // Convert the boolean value to an integer
       
    
        // $users->subscription_id = $request->input('subscription_id');
        $subscription_id = $request->input('subscription_id');
        if ($subscription_id !== null) {
           $users->subscription_id =(int) $subscription_id;
        }
        // $users->subscription_validity = $request->input('subscription_validity');
        $subscription_validity = $request->input('subscription_validity');
        if ($subscription_validity !== null) {
           $users->subscription_validity = $subscription_validity;
        }
        // $users->subscribed_on = $request->input('subscribed_on');
        $subscribed_on = $request->input('subscribed_on');
        if ($subscribed_on !== null) {
           $users->subscribed_on = $subscribed_on;
        }
        // $users->booking_fee = $request->input('booking_fee');
        $booking_fee = $request->input('booking_fee');
        if ($booking_fee !== null) {
           $users->booking_fee = $booking_fee;
        }
    


        $existingAvatar = Media::where('model_id', $users->id)
        ->where('collection_name', 'avatar')
        ->first();

        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');

            $collectionName = 'avatar'; // Replace with your desired collection name
            $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

            
                // Delete the previous avatar file from S3 and database
                if ($existingAvatar) {
                    // Storage::disk('s3')->delete($existingAvatar->getPath());
                    $existingAvatar->delete(); // Delete the avatar record from the database
                }else{
                    // Add the new image file to the media collection with resizing and optimization
                    $media = $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }  
        }


    
        $existingAvatar = Media::where('model_id', $users->id)
        ->where('collection_name', 'user-image1')
        ->first();

        if ($request->hasFile('image1')) {
            $file = $request->file('image1');

            $collectionName = 'user-image1'; // Replace with your desired collection name
            $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

            
                // Delete the previous avatar file from S3 and database
                if ($existingAvatar) {
                    // Storage::disk('s3')->delete($existingAvatar->getPath());
                    $existingAvatar->delete(); // Delete the avatar record from the database
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }else{
                    // Add the new image file to the media collection with resizing and optimization
                    $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }  
        }
    
    
        
        $existingAvatar = Media::where('model_id', $users->id)
        ->where('collection_name', 'user-image2')
        ->first();

        if ($request->hasFile('image2')) {
            $file = $request->file('image2');

            $collectionName = 'user-image2'; // Replace with your desired collection name
            $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

            
                // Delete the previous avatar file from S3 and database
                if ($existingAvatar) {
                    // Storage::disk('s3')->delete($existingAvatar->getPath());
                    $existingAvatar->delete(); // Delete the avatar record from the database
                 $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }else{
                    // Add the new image file to the media collection with resizing and optimization
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }  
        }


        
      // Delete the previous avatar if it exists
      $existingAvatar = Media::where('model_id', $users->id)
        ->where('collection_name', 'user-image3')
        ->first();

        if ($request->hasFile('image3')) {
            $file = $request->file('image3');

            $collectionName = 'user-image3'; // Replace with your desired collection name
            $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

            
                // Delete the previous avatar file from S3 and database
                if ($existingAvatar) {
                    // Storage::disk('s3')->delete($existingAvatar->getPath());
                    $existingAvatar->delete(); // Delete the avatar record from the database
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }else{
                    // Add the new image file to the media collection with resizing and optimization
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }  
        }
    

        
      // Delete the previous avatar if it exists
      $existingAvatar = Media::where('model_id', $users->id)
        ->where('collection_name', 'user-image4')
        ->first();

        if ($request->hasFile('image4')) {
            $file = $request->file('image4');

            $collectionName = 'user-image4'; // Replace with your desired collection name
            $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

            
                // Delete the previous avatar file from S3 and database
                if ($existingAvatar) {
                    // Storage::disk('s3')->delete($existingAvatar->getPath());
                    $existingAvatar->delete(); // Delete the avatar record from the database
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }else{
                    // Add the new image file to the media collection with resizing and optimization
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }  
        }


        
      // Delete the previous avatar if it exists
      $existingAvatar = Media::where('model_id', $users->id)
        ->where('collection_name', 'user-image5')
        ->first();

        if ($request->hasFile('image5')) {
            $file = $request->file('image5');

            $collectionName = 'user-image5'; // Replace with your desired collection name
            $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

            
                // Delete the previous avatar file from S3 and database
                if ($existingAvatar) {
                    // Storage::disk('s3')->delete($existingAvatar->getPath());
                    $existingAvatar->delete(); // Delete the avatar record from the database
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }else{
                    // Add the new image file to the media collection with resizing and optimization
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }  
        }

        
      // Delete the previous avatar if it exists
      $existingAvatar = Media::where('model_id', $users->id)
        ->where('collection_name', 'user-image6')
        ->first();

        if ($request->hasFile('image6')) {
            $file = $request->file('image6');

            $collectionName = 'user-image7'; // Replace with your desired collection name
            $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

            
                // Delete the previous avatar file from S3 and database
                if ($existingAvatar) {
                    // Storage::disk('s3')->delete($existingAvatar->getPath());
                    $existingAvatar->delete(); // Delete the avatar record from the database
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }else{
                    // Add the new image file to the media collection with resizing and optimization
                     $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                }  
        }


        
      // Delete the previous avatar if it exists
      $existingAvatar = Media::where('model_id', $users->id)
      ->where('collection_name', 'user-image7')
      ->first();

      if ($request->hasFile('image7')) {
          $file = $request->file('image7');

          $collectionName = 'user-image7'; // Replace with your desired collection name
          $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

          
              // Delete the previous avatar file from S3 and database
              if ($existingAvatar) {
                  // Storage::disk('s3')->delete($existingAvatar->getPath());
                  $existingAvatar->delete(); // Delete the avatar record from the database
                  $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
              }else{
                  // Add the new image file to the media collection with resizing and optimization
                   $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
              }  
      }


        
      // Delete the previous avatar if it exists
      $existingAvatar = Media::where('model_id', $users->id)
      ->where('collection_name', 'user-video1')
      ->first();

      if ($request->hasFile('video1')) {
          $file = $request->file('video1');

          $collectionName = 'user-video1'; // Replace with your desired collection name
          $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

          
              // Delete the previous avatar file from S3 and database
              if ($existingAvatar) {
                  // Storage::disk('s3')->delete($existingAvatar->getPath());
                  $existingAvatar->delete(); // Delete the avatar record from the database
                  $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
              }else{
                  // Add the new image file to the media collection with resizing and optimization
                   $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
              }  
      }

          // Delete the previous avatar if it exists
          $existingAvatar = Media::where('model_id', $users->id)
      ->where('collection_name', 'user-video2')
      ->first();

      if ($request->hasFile('video2')) {
          $file = $request->file('video2');

          $collectionName = 'user-video2'; // Replace with your desired collection name
          $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket

          
              // Delete the previous avatar file from S3 and database
              if ($existingAvatar) {
                  // Storage::disk('s3')->delete($existingAvatar->getPath());
                  $existingAvatar->delete(); // Delete the avatar record from the database
                  $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
              }else{
                  // Add the new image file to the media collection with resizing and optimization
                  $media = $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
              }  
      }


              // Delete the previous avatar if it exists
              $existingAvatar = Media::where('model_id', $users->id)
              ->where('collection_name', 'user-video3')
              ->first();
        
              if ($request->hasFile('video3')) {
                  $file = $request->file('video3');
        
                  $collectionName = 'user-video3'; // Replace with your desired collection name
                  $directory = 'e4t-users-images/'; // Replace with the desired directory structure within your S3 bucket
        
                  
                      // Delete the previous avatar file from S3 and database
                      if ($existingAvatar) {
                          // Storage::disk('s3')->delete($existingAvatar->getPath());
                          $existingAvatar->delete(); // Delete the avatar record from the database
                          $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                      }else{
                          // Add the new image file to the media collection with resizing and optimization
                          $media = $users->addMedia($file)->toMediaCollection($collectionName, 's3', $directory);
                      }  
              }


        $users->save();
    
        // return response()->json(['status'=>200,'user' => $users]);
          return redirect()->route('starry.index')->with('success', 'User updated successfully');
    }




    public function destroy($id)
    {
        $user = User::find($id);

        if (!$user) {
            return redirect()->route('starry.index')->with('error', 'User not found.');
        }

        // Delete the associated S3 avatar if it exists
        if ($user->avatar) {
            Storage::disk('s3')->delete($user->avatar);
        }

        $user->delete();

        return redirect()->route('starry.index')->with('success', 'User and associated avatar deleted successfully.');
    }






}