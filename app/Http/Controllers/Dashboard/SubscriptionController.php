<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;use App\Models\User;
class SubscriptionController extends Controller
{
    public function index(Request $request)
{
    if ($request->ajax()) {
        // Fetch users with subscription_id = 20
        $usersWithSubscription20 = User::where('subscription_id', 20)->get();
        return response()->json($usersWithSubscription20);
    }

    return view('subscription.index');
}
}
  