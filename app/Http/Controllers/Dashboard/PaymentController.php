<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;
use DataTables;
class PaymentController extends Controller
{
    public function index(Request $request)
{
    if ($request->ajax()) {
        $payments = Payment::select([
            'id', 'user_id', 'subscription_id', 'order_id', 'tracking_id',
            'bank_ref_no', 'order_status', 'payment_mode','paymentType', 'card_name',
            'status_code', 'status_message', 'currency', 'billing_name',
            'billing_address', 'billing_state', 'billing_zip', 'billing_country',
            'billing_tel', 'billing_email', 'trans_date', 'token_eligibility',
            'response_code', 'amount', 'ccavenue_status', 'created_at', 'updated_at',
        ]);

        return DataTables::of($payments)
            ->toJson();
    }

    return view('payments.index');
}
    
}
