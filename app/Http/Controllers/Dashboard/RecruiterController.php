<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Recruiter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use Aws\S3\S3Client;
use \DateTime;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;

class RecruiterController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $recruiters = Recruiter::all();

            return datatables()->of($recruiters)
               ->addIndexColumn()
                ->addColumn('logo', function ($recruiter) {
                    $imageUrl = $recruiter->logo;
                    return '<img src="' . $imageUrl . '" alt="Logo" width="50">';
                })
                ->addColumn('action', function ($recruiter) {
                    $editRoute = route('recruiters.edit', ['recruiter' => $recruiter->id]);
                    $deleteRoute = route('recruiters.destroy', ['recruiter' => $recruiter->id]);

                    $actionBtn = '<a href="' . $editRoute . '" class="btn btn-success btn-sm">Edit</a>';
                    $actionBtn .= ' <a href="' . $deleteRoute . '" class="btn btn-danger btn-sm delete-btn">Delete</a>';

                    return $actionBtn;
                })
                ->rawColumns(['logo', 'action'])
                ->make(true);
        }

        return view('recruiters.index');
    }

    public function create()
    {
        return view('recruiters.create');
    }

    public function store(Request $request)
    {
        $recruiter = new Recruiter();
        $recruiter->name = $request->input('name');
        $recruiter->description = $request->input('description');
        $recruiter->color_code = $request->input('color_code');
        $recruiter->url = $request->input('url');
        $recruiter->link = $request->input('link');
        $recruiter->type = $request->input('type');
        // $recruiter->email = $request->input('email');
        // $recruiter->password = bcrypt($request->input('password'));
        // Other fields

        // if ($request->hasFile('logo')) {
        //     $file = $request->file('logo');
        //     $imagePath = "recruiter/" . time() . '_' . $file->getClientOriginalName();

        //     Storage::disk('s3')->put($imagePath, file_get_contents($file));
        //     Storage::disk('s3')->setVisibility($imagePath, 'public');

        //     $recruiter->logo = $imagePath;
        // }

        if ($request->hasFile('logo')) {
    $file = $request->file('logo');
    $imagePath = "recruiter/" . time() . '_' . $file->getClientOriginalName();

    // Store the file on S3
    Storage::disk('s3')->put($imagePath, file_get_contents($file));
    Storage::disk('s3')->setVisibility($imagePath, 'public');

    // Create the full S3 URL
    $s3Url = 'https://exchange4talent.s3.ap-south-1.amazonaws.com/' . $imagePath;

    $recruiter->logo = $s3Url; // Store the full S3 URL in the database
}

        $recruiter->save();
        return redirect()->route('recruiters.index')->with('success', 'Recruiter created successfully.');
    }

    public function edit(Recruiter $recruiter)
    {
        return view('recruiters.edit', compact('recruiter'));
    }

    public function update(Request $request, Recruiter $recruiter)
    {
        $recruiter->name = $request->input('name');
        $recruiter->description = $request->input('description');
        $recruiter->color_code = $request->input('color_code');
        $recruiter->url = $request->input('url');
        $recruiter->link = $request->input('link');
        $recruiter->type = $request->input('type');
        // $recruiter->email = $request->input('email');
        // $recruiter->password = bcrypt($request->input('password'));

         // Other fields

        // if ($request->hasFile('logo')) {
        //     // Delete previous logo from S3
        //     if ($recruiter->logo) {
        //         Storage::disk('s3')->delete($recruiter->logo);
        //     }

        //     $file = $request->file('logo');
        //     $imagePath = "recruiter/" . time() . '_' . $file->getClientOriginalName();

        //     Storage::disk('s3')->put($imagePath, file_get_contents($file));
        //     Storage::disk('s3')->setVisibility($imagePath, 'public');

        //     $recruiter->logo = $imagePath;
        // }
        if ($request->hasFile('logo')) {
    // Delete previous logo from S3
    if ($recruiter->logo) {
        $filename = basename($recruiter->logo);
        Storage::disk('s3')->delete('recruiter/' . $filename);
    }

    $file = $request->file('logo');
    $imagePath = "recruiter/" . time() . '_' . $file->getClientOriginalName();

    // Store the file on S3
    Storage::disk('s3')->put($imagePath, file_get_contents($file));
    Storage::disk('s3')->setVisibility($imagePath, 'public');

    // Create the full S3 URL
    $s3Url = 'https://exchange4talent.s3.ap-south-1.amazonaws.com/' . $imagePath;

    // Update the recruiter's logo field with the new URL
    $recruiter->logo = $s3Url;
}

        $recruiter->save();
        return redirect()->route('recruiters.index')->with('success', 'Recruiter updated successfully.');
    }

    public function destroy(Recruiter $recruiter)
    {
        // Delete logo from S3
        if ($recruiter->logo) {
            Storage::disk('s3')->delete($recruiter->logo);
        }

        $recruiter->delete();
        return redirect()->route('recruiters.index')->with('success', 'Recruiter deleted successfully.');
    }
}
