<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ads;
use App\Models\User;
use App\Models\Media;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use Aws\S3\S3Client;
use \DateTime;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;

class AdController extends Controller
{


    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::select('*') ->orderBy('created_at', 'desc') // Ordering by 'created_at' in descending order
            ->get();

            // $ads = Ads::select(['id','videos', 'images']); 
               
            return datatables()->of($data)
                    ->addIndexColumn()
                ->addColumn('videos', function ($row) {
                    $userVideoMedia = $row->media()
                    ->where('collection_name', 'user-ads-video')
                    ->first();
                    $videoUrl = $userVideoMedia ? $userVideoMedia->getUrl() : '';

                    if ($videoUrl !== '') {
                        return '<video controls width="150"><source src="' . $videoUrl . '" type="video/mp4"></video>';
                    } else {
                        return 'No Ads Video';
                    }
                })
                ->addColumn('images', function ($row) {
                    $userImageMedia = $row->media()
                    ->where('collection_name', 'user-ads-image')
                    ->first();
                    $imageUrl = $userImageMedia ? $userImageMedia->getUrl() : '';
                    
                    if($imageUrl !== ''){
                        return '<img src="' . $imageUrl . '" alt="Image" width="50" height="50">';
                    }else{
                        return 'No Ads Image';
                    }
                })
                ->addColumn('action', function ($row) {
                    $editRoute = route('ads.edits.test', ['ad' => $row->id]);
                    $deleteRoute = route('ads.destroy', ['ad' => $row->id]);
    
                    $actionBtn = '<a href="' . $editRoute . '" class="edit btn btn-success btn-sm">Edit</a>';
                    $actionBtn .= ' <a href="' . $deleteRoute . '" class="delete btn btn-danger btn-sm">Delete</a>';
    
                    return $actionBtn;
                })
                ->rawColumns(['name','videos', 'images', 'action'])
                ->toJson();
        }
    
        return view('ads.index');
    }



    
public function create()
{
    $data = User::get();
    return view('ads.create', compact('data'));
}

public function store(Request $request)
{
    $data = $request->all();

    // Assuming you are storing the user ID in the request
    $user = User::find($data['user_id']);

     

    // Handle file uploads (videos and images) to S3
    if ($request->hasFile('videos')) {

        $existingUservideo = Media::where('model_id',$user->id)->where('collection_name','user-ads-video')->first();

        if($existingUservideo){
         return redirect()->route('ads.create')->with('errormessage','Your are only upload one ADS Video');
        }

        $file = $request->file('videos');
        $user->addMedia($file)->toMediaCollection('user-ads-video', 's3');
    }


    

    if ($request->hasFile('images')) {
        $existingUserimage = Media::where('model_id',$user->id)->where('collection_name','user-ads-image')->first();

       if($existingUserimage){
        return redirect()->route('ads.create')->with('errormessage','Your are only upload one ADS Image');
       }
        $files = $request->file('images');
        foreach ($files as $file) {
            $user->addMedia($file)->toMediaCollection('user-ads-image', 's3');
        }
    }

    // Create the ad using the Ads model
     Ads::create($data);

    return redirect()->route('ads.index')->with('success', 'Ad created successfully');

}

public function store1(Request $request)
{
    $data = $request->all();

    // Assuming you are storing the user ID in the request
    $user = User::find($data['user_id']);

       $existingUservideo = Media::where('model_id',$user->id)->where('collection_name','user-ads-video')->first();

       if($existingUservideo){
        return redirect()->route('ads.create')->with('errormessage','Your are only upload one ADS Video');
       }

    // Handle file uploads (videos and images) to S3
    if ($request->hasFile('videos')) {
        $file = $request->file('videos');
        $user->addMedia($file)->toMediaCollection('user-ads-video', 's3');
    }


    $existingUserimage = Media::where('model_id',$user->id)->where('collection_name','user-ads-image')->first();

       if($existingUserimage){
        return redirect()->route('ads.create')->with('errormessage','Your are only upload one ADS Image');
       }

    if ($request->hasFile('images')) {
        $files = $request->file('images');
        foreach ($files as $file) {
            $user->addMedia($file)->toMediaCollection('user-ads-image', 's3');
        }
    }

    // Create the ad using the Ads model
     Ads::create($data);

    return redirect()->route('ads.index')->with('success', 'Ad created successfully');
}

public function edit($id)
{
    $ad = User::find($id);
    return view('ads.edit', compact('ad'));
}

public function update(Request $request, $ad)
{
    $ad = User::where('id', $ad)->first();


    $mediaItemID = Media::where('model_id', $ad->id)->where('collection_name','user-ads-video')->first();

    if ($ad) {

        if ($mediaItemID === null) {
            if ($request->hasFile('videos')){
                   $newVideo = $request->file('videos');
        
                if ($newVideo) {
            
                    // Add the new video to the collection
                    $ad->addMedia($newVideo)->toMediaCollection('user-ads-video', 's3');
                }
            }else{
                return 'something is wrong..';
            }
            
        }else{


            $mediaItem = $ad->getMedia('user-ads-video')->where('id', $mediaItemID->id)->first();

        
        if ($mediaItem) {
            $newVideo = $request->file('videos');

            // Replace the existing media item with the new video in the collection
            $ad->addMedia($newVideo)->toMediaCollection('user-ads-video', 's3');

            $mediaItem->delete(); // Delete the replaced media item
           
        } else {
            // return response()->json(['message' => 'Media item not found'], 404);
        }

     }

        
    } else {
        // return response()->json(['message' => 'Ad not found for the current user'], 404);
    }


// for user ads images 
if($request->file('images')){


    $mediaItemID = Media::where('model_id', $ad->id)->where('collection_name','user-ads-image')->first();

    if ($ad) {

        if ($mediaItemID === null) {
            if ($request->hasFile('images')){
                   $newVideo = $request->file('images');
        
                if ($newVideo) {
            
                    // Add the new video to the collection
                    $ad->addMedia($newVideo)->toMediaCollection('user-ads-image', 's3');
                }
            }else{
                return 'something is wrong in image..';
            }
            
        }else{


            $mediaItem = $ad->getMedia('user-ads-video')->where('id', $mediaItemID->id)->first();

        
        if ($mediaItem) {
            $newVideo = $request->file('images');

            // Replace the existing media item with the new video in the collection
            $ad->addMedia($newVideo)->toMediaCollection('user-ads-image', 's3');

            $mediaItem->delete(); // Delete the replaced media item
           
        } else {
            // return response()->json(['message' => 'Media item not found'], 404);
        }

     }

        
    } else {
        // return response()->json(['message' => 'Ad not found for the current user'], 404);
    }

}


    $ad->save();

    return redirect()->route('ads.index')->with('success', 'Ad updated successfully');
}




public function destroy($id)
{
    $user = User::where('id', $id)->first(); // Assuming you have a 'user_id' column in your Post model

    $collectionName = 'user-ads-video';

    //  return  $mediaItem = $user->getMedia($collectionName)->first();

     if ($user) {
        $mediaItem = $user->getMedia($collectionName)->first();

        if ($mediaItem) {
            $mediaItemUserId = $mediaItem->model_id;

            if ($user->id === $mediaItemUserId) {
                $mediaItem->delete(); // Delete the media item from both the database and S3 storage
                // return response()->json(['message' => 'Media item deleted successfully']);
            } else {
                // return response()->json(['message' => 'You do not have permission to delete this media item'], 403);
            }
        } else {
            // return response()->json(['message' => 'Media item not found'], 404);
        }
    } else {
        // return response()->json(['message' => 'User not found for the current user'], 404);
    }

    return redirect()->route('ads.index')->with('success', 'Ad deleted successfully');
}

}