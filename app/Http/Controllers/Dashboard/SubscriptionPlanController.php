<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscription;
use DataTables;
use Illuminate\Support\Facades\Storage;
class SubscriptionPlanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $subscriptionPlans = Subscription::select([
                'id', 'title', 'description', 'price', 'plan', 'validity',
                'images', 'images_limit', 'videos_limit', 'video_duration',
                'color_code', 'videos', 'contacts', 'view_email', 'view_mobile',
                'image_changes', 'video_changes', 'max_images_size', 'max_video_size',
                'created_at', 'updated_at'
            ]);
    
            return DataTables::of($subscriptionPlans)
                ->addColumn('images', function ($subscriptionPlan) {
                    $imageUrl = $subscriptionPlan->images;
                    return '<img src="' . $imageUrl . '" alt="Image" width="250" height="150">';
                })
                ->addColumn('actions', function ($subscriptionPlan) {
                    $editRoute = route('subscription.edit', ['subscriptionPlan' => $subscriptionPlan->id]);
                    $deleteRoute = route('subscription.destroy', ['subscriptionPlan' => $subscriptionPlan->id]);
    
                    $editButton = '<a href="' . $editRoute . '" class="btn btn-success btn-sm">Edit</a>';
                    $deleteButton = '<a href="' . $deleteRoute . '" class="btn btn-danger btn-sm delete" data-id="' . $subscriptionPlan->id . '">Delete</a>';
    
                    return $editButton . ' ' . $deleteButton;
                })
                ->rawColumns(['images', 'actions'])
                ->toJson();
        }
    
        return view('subscription.index');
    }
    
    
    

    public function create()
    {
        return view('subscription.create');
    }

    public function store(Request $request)
    {
        // Validate the request data
        // $validatedData = $request->validate([
        //     'title' => 'required',
        //     'description' => 'required',
        //     'price' => 'required|numeric',
        //     'plan' => 'required',
        //     'validity' => 'required|integer',
        //     'images' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048' // Adjust file types and size
        //     // Add validation rules for other columns
        // ]);

        // Handle image upload
        if ($request->hasFile('images')) {
            $imagePath = $request->file('images')->store('subscription-image/', 's3');
            $validatedData['images'] = $imagePath;
        }

        // Create a new SubscriptionPlan instance
        Subscription::create($validatedData);

        return redirect()->route('subscription.index')->with('success', 'Subscription Plan created successfully');
    }

    public function edit(Subscription $subscriptionPlan)
{
    return view('subscription.edit', compact('subscriptionPlan'));
}

    public function update(Request $request, Subscription $subscriptionPlan)
{
    // $request->validate([
    //     'title' => 'required',
    //     'description' => 'required',
    //     'price' => 'required|numeric',
    //     'plan' => 'required',
    //     'validity' => 'required',
    //     // Add validation rules for other columns here
    // ]);

    $data = $request->all();

    // Handle image upload and update if a new image is provided
    if ($request->hasFile('images')) {
        $imagePath = $request->file('images')->store('subscription-image/', 's3');
        $data['images'] = $imagePath;

        // Delete the previous image from S3 if it exists
        if ($subscriptionPlan->images) {
            Storage::disk('s3')->delete($subscriptionPlan->images);
        }
    }

    $subscriptionPlan->update($data);

    return redirect()->route('subscription.index')->with('success', 'Subscription plan updated successfully');
}


    public function destroy(Subscription $subscriptionPlan)
    {
        // Delete the associated S3 image if it exists
        if ($subscriptionPlan->images) {
            Storage::disk('s3')->delete($subscriptionPlan->images);
        }

        // Delete the subscription plan
        $subscriptionPlan->delete();

        return redirect()->route('subscription.index')->with('success', 'Subscription Plan deleted successfully');
    }

}
