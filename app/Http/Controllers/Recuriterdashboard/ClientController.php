<?php

namespace App\Http\Controllers\Recuriterdashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Client;
use App\Models\Category;
use App\Models\User;
use App\Models\UserPostMapping;
use App\Models\FcmToken;
use Auth;
use DataTables;
use Illuminate\Support\Facades\DB;
use Notification;
use App\Notifications\SendPushNotification;



class ClientController extends Controller
{

    public function index(Request $request)
{
    if ($request->ajax()) {
        $recruiterId = Auth::guard('recruiter')->id();
        $clients = Client::select(['id', 'recuriter_id', 'title', 'description', 'category_title', 'start_date', 'end_date', 'audition_date', 'location', 'status'])
            ->where('recuriter_id', $recruiterId)
            ->get(); // Fetch data from the database

        return DataTables::of($clients)
            ->addColumn('action', function ($row) {


                $statusBtn = '<form action="' . route('clients.updateStatus', ['client' => $row->id]) . '" method="POST">';
                $statusBtn .= csrf_field();
                $statusBtn .= '<button type="submit" class="btn btn-sm ' . ($row->status == 1 ? 'btn-success' : 'btn-danger') . '">';
                $statusBtn .= $row->status == 1 ? 'Active' : 'Inactive';
                $statusBtn .= '</button></form>';

           
            

                return $statusBtn;
            })

            
            ->rawColumns(['action'])
            ->make(true);
    }
    return view('recruiterdasboard.clients.index');
}

    
    






    // public function store(Request $request)
    // {
    //     request()->validate([
    //         'title' => 'required',
    //         'description' => 'required',
    //         ]);
    //     $Notification = Notification::create(["description" =>  $request->description,
    //                                     "title" =>  $request->title,
    //                                     "user_id"=>Auth::id(),]);
    //     if($Notification)
    //     {
    //         $members= User::where('type','member')->get();
    //         app('App\Http\Controllers\Controller')->sendPush($request->title,$request->description,0);
            
    //         return redirect('notification')->with('success');
    //     }
    //     else
    //         return redirect('notification')->with('failed');
    // }


    public function create()
    {
        return view('recruiterdasboard.clients.create');
    }



    public function store(Request $request)
    {
     
        // Validate the incoming data
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'location' => 'required|string',
            'category' => 'required|array',
            'start_date' => 'required|date', // Add this line to validate 'start_date'
            'end_date' => 'required|date',   // Add this line to validate 'end_date'
            'audition_date' => 'required|string', // Assuming 'audition' is a string
        ]);
    
        $client = new Client();
        $client->title = $validatedData['title'];
        $client->description = $validatedData['description'];
        $client->start_date = $validatedData['start_date'];
        $client->end_date = $validatedData['end_date'];
        $client->audition_date = $validatedData['audition_date'];
        $client->location = $validatedData['location'];
        $client->status= "1";
        $client->recuriter_id = Auth::guard('recruiter')->id(); // Assuming you want to set a default value of 1
    
          // $users->category_id = $request->input('category_id');
          $jsonEncoded = $client->category_ids = json_encode($request->category);

          // Decode JSON to an array
          $categoryIds = json_decode($jsonEncoded);
          
          // Convert the array to comma-separated string
          $commaSeparated = implode(',', $categoryIds);
          
          // Store the comma-separated string in your object's category_ids property
          $client->category_ids = $commaSeparated;
          
          // Decode JSON to an array
          $jsonEncodeds = $client->category_title = json_encode($request->category);
          $categoryIds = json_decode($jsonEncodeds);
          
          // Assuming you have a 'Category' model
          $categories = Category::whereIn('id', $categoryIds)->get();
          
          // Initialize an array to store category titles
          $categoryTitles = [];
          
          // Loop through the fetched categories to extract titles
          foreach ($categories as $category) {
              $categoryTitles[] = $category->title;
          }
          
          // Convert the titles array to a comma-separated string
          $commaSeparatedTitles = implode(',', $categoryTitles);
          
          // Store the comma-separated string in your object's category_title property
          $client->category_title = $commaSeparatedTitles;
      
          

            $categoryIds = json_decode($jsonEncoded);
          $categoryIds = array_map('intval', $categoryIds);



       $user = User::whereIn('category_ids',$categoryIds)->get();

        $userIds = $user->pluck('id');
    
        // Save the $client object to the database
        if ($client->save()) {
    
            $existingToken = FcmToken::whereIn('user_id', $userIds)->pluck('fcm_token')->toArray();

            app('App\Http\Controllers\Controller')->sendPush($request->title, $request->description, $existingToken, 'job', $client->id);
            
            return redirect()->route('clients.index')->with('success', 'Client created successfully.');
        } else {
            // Handle the case where the save operation fails
            return redirect()->back()->with('error', 'Failed to create client.');
        }
    }
    



public function updateStatus(Request $request, Client $client)
{
    // return $client;
    // Validate the request and update the client's status
    // $request->validate([
    //     'status' => 'required|in:0,1', // Valid values are 0 (inactive) and 1 (active)
    // ]);

    // Toggle the status value
    $client->status = $client->status === 1 ? 0 : 1;
    $client->save();

    return redirect()->route('clients.index')
        ->with('success', 'Client status updated successfully');
}


// public function updateStatus(Request $request, $clientId)
// {
//     $newStatus = $request->input('status');
//     $client = Client::find($clientId);

//     if ($client) {
//         $client->update(['status' => $newStatus]);

//         return response()->json(['message' => 'Status updated successfully']);
//     } else {
//         return response()->json(['error' => 'Client not found'], 404);
//     }
// }

public function getAppliedUser(Request $request){
    // Get the currently authenticated recruiter's ID
     $recruiterId = Auth::guard('recruiter')->id();

    // Retrieve the posts created by the recruiter
    $recruiterPosts = Client::where('recuriter_id', $recruiterId)->pluck('id');


    $userId = UserPostMapping::whereIn('post_id',$recruiterPosts)->pluck('user_id');

    $appliedUsers = User::whereIn('id', $userId)
    ->with('posts') 
    ->paginate(10);

    

    return view('recruiterdasboard.clients.appliedUsers')->with('appliedUsers', $appliedUsers);
}

public function updateUserStatus(Request $request) {
    $userId = $request->input('userId');
    $postId = $request->input('postId');
    $status = $request->input('status');

    $job_status = 0; 

    if ($status == 'shortlist') {
        $job_status = 2;
    } elseif ($status == 'hired') {
        $job_status = 4;
    } elseif ($status == 'rejected') {
        $job_status = 3;
    }

    // Use the where clause to update the specific record
    UserPostMapping::where('user_id', $userId)
        ->where('post_id', $postId)
        ->update(['job_status' => $job_status]);

    return response()->json(['message' => 'User status updated successfully']);
}





}