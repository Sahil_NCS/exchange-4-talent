<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Subscription;
use App\Models\UserPostMapping;
use App\Models\Rating;
use Illuminate\Support\Facades\Storage;
use App\Models\Tag;
use App\Models\Ads;
use App\Models\Imaged;
use Illuminate\Support\Facades\Schema;
// use Carbon\Carbon;
use \DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Media\Video;
use FFMpeg\Coordinate\TimeCode;
use Illuminate\Support\Facades\Validator;
use Aws\S3\S3Client;
use DataTables;
use App\Models\Client;
use App\Models\Recruiter;
use DB;
use Carbon\Carbon;



class RecruiterPostControllerAPI extends Controller
{
    public function getUsersByRecruiter(Request $request, $recruiter_id)
    {
            // Clients table se recruiter ke matching clients retrieve karein
        $clients = Client::where('recuriter_id', $recruiter_id)->get();

        // Collect all the unique category_ids from clients
        $uniqueCategoryIds = [];
        foreach ($clients as $client) {
            $clientCategoryIds = explode(',', $client->category_ids);
            $uniqueCategoryIds = array_merge($uniqueCategoryIds, $clientCategoryIds);
        }
        $uniqueCategoryIds = array_unique($uniqueCategoryIds);

        // Users table se matching category_ids ke users retrieve karein
        $matchingUsers = User::where(function ($query) use ($uniqueCategoryIds) {
            foreach ($uniqueCategoryIds as $categoryId) {
                $query->orWhereRaw("FIND_IN_SET(?, category_ids)", [$categoryId]);
            }
        })->get();

        return response()->json($matchingUsers);
    }


    public function getRecruiterPostByUserId(Request $request)
{
    $userId = $request->userId;
    $user = User::find($userId);

    if (!$user) {
        return response()->json(['status' => 404, 'message' => 'User not found']);
    }

    $userCategory = $user->category_ids;
    $usersArray = explode(',', $userCategory);

    // Convert each element of $usersArray to integers
    $usersArray = array_map('intval', $usersArray);

    // Client:: mean post table
  $posts = Client::whereIn('category_ids', $usersArray)->with('recruiter')->has('recruiter')->paginate(10);
    

          // Get the user's applied post_ids
          $appliedPostIds = UserPostMapping::where('user_id', $userId)->pluck('post_id')->toArray();

          // Add a dynamic field "applied" to each post in the $posts collection
          $posts->each(function ($post) use ($appliedPostIds) {
              $post->applied = in_array($post->id, $appliedPostIds);
          });

    return response()->json(['status' => 200, 'data' => $posts]);
}

    



public function applyJobs(Request $request) {
    $userId = $request->userId;
    $postId = $request->postId;
    
    if ($userId && $postId) {
        $postExist = UserPostMapping::where('user_id', $userId)->where('post_id', $postId)->first();
    
        if (!$postExist) {
            UserPostMapping::create([
                'user_id' => $userId,
                'post_id' => $postId
            ]);
    
            // Check if the user has applied to this post
            $userPostMappingRecord = UserPostMapping::where('user_id', $userId)
                ->where('post_id', $postId)
                ->first();
    
            $jobStatus = $userPostMappingRecord ? $userPostMappingRecord->job_status : 0;
    
            // Retrieve the single post and attach the job status
            $post = Client::where('id', $postId)
                ->with('recruiter')
                ->has('recruiter')
                ->first();
    
            // Add the dynamic field "jobStatus" to the post
            $post->jobStatus = $jobStatus;
    
            return response()->json(['status' => 200, 'message' => 'Applied Successfully', 'data' => $post]);
        } else {
            return response()->json(['status' => 400, 'message' => 'You are Already Applied']);
        }
    } else {
        return response()->json(['status' => 400, 'message' => 'Invalid User or Post']);
    }
}



public function appliedJobs1(Request $request){
    try {
        //code...
        $userId = $request->userId;

        if ($userId) {
            
            // Find the UserPostMapping records associated with the given user_id
            $userPostMapping = UserPostMapping::where('user_id', $userId)->get();
        
            // Assuming you want to return the post_id(s) associated with the user
            $postIds = $userPostMapping->pluck('post_id');

            // $posts = Client::whereIn('id',$postIds->with('recruiter'))->paginate(10);
            $posts = Client::whereIn('category_ids', $postIds)
            ->with('recruiter')
            ->whereHas('recruiter', function ($query) {
                $query->select('id')->from('recruiter');
            })
            ->paginate(10);

            $appliedPostIds = UserPostMapping::where('user_id', $userId)->pluck('post_id')->toArray();

          // Add a dynamic field "applied" to each post in the $posts collection
          $posts->each(function ($post) use ($appliedPostIds) {
              $post->applied = in_array($post->id, $appliedPostIds);
          });
        
            return response()->json(['status' => 200, 'data' => $posts]);
        } else {
            return response()->json(['status' => 200, 'message' => "User Not Found..."]);
        }
        

    } catch (\Throwable $th) {
        return response()->json(['status'=>400,'Error'=>$th]);
    }
}

public function appliedJobs(Request $request) {
    try {
        // Validate the user ID
        $userId = $request->userId;

        if (!$userId) {
            return response()->json(['status' => 400, 'message' => "User Not Found..."]);
        }

        // Query for UserPostMapping records associated with the user
        $userPostMapping = UserPostMapping::where('user_id', $userId)->get();

        if ($userPostMapping->isEmpty()) {
            return response()->json(['status' => 400, 'message' => "No UserPostMapping records found for this user."]);
        }

        // Get the post_ids associated with the user
        $postIds = $userPostMapping->pluck('post_id')->toArray();

        // Query for posts that have matching post_ids
        $posts = Client::whereIn('id', $postIds)
            ->with(['recruiter'])
            ->has('recruiter')
            ->paginate(10);

        // Add a dynamic field "jobStatus" to each post in the $posts collection
        $posts->each(function ($post) use ($userPostMapping) {
            $userPostMappingRecord = $userPostMapping->where('post_id', $post->id)->first();
            $post->jobStatus = $userPostMappingRecord ? $userPostMappingRecord->job_status : 0;
        });

        return response()->json(['status' => 200, 'data' => $posts]);
    } catch (\Throwable $th) {
        return response()->json(['status' => 400, 'Error' => $th]);
    }
}


public function getRecruiterSinglePost(Request $request) {
    $userId = Auth::id();
    $postId = $request->post_id;

    $post = Client::where('id', $postId)
        ->with('recruiter')
        ->has('recruiter')
        ->first();

    if (!$post) {
        return response()->json(['status' => 400, 'message' => 'Post Not Found']);
    }

    // Check if the user has applied to this post
    $userPostMappingRecord = UserPostMapping::where('user_id', $userId)
        ->where('post_id', $postId)
        ->first();

    $jobStatus = $userPostMappingRecord ? $userPostMappingRecord->job_status : 0;

    // Add the dynamic field "jobStatus" to the post
    $post->jobStatus = $jobStatus;

    return response()->json(['status' => 200, 'data' => $post]);
}


public function getRecruitersWithPostCount(){
    // Retrieve paginated recruiter data
    $recruiterData = Recruiter::paginate(10);

    // Get the IDs of the recruiters
    $recruiter_ids = $recruiterData->pluck('id');

    // Query the clients table to count clients for each recruiter
    $recruiterData->each(function ($recruiter) use ($recruiter_ids) {
        $clientCount = Client::whereIn('recuriter_id', $recruiter_ids)->where('recuriter_id', $recruiter->id)->count();
        $recruiter->Post_count = $clientCount;
    });

    // Sort the recruiter data by client_count in descending order
    // $recruiterData = $recruiterData->sortByDesc('Post_count');

    return response()->json(['status'=>200,'data'=>$recruiterData]);

}

public function getRecruitersWithPostCount1(){
    // Retrieve paginated recruiter data
    $recruiterData = Recruiter::get();

    // Get the IDs of the recruiters
    $recruiter_ids = $recruiterData->pluck('id');

    // Query the clients table to count clients for each recruiter
    $recruiterData->each(function ($recruiter) use ($recruiter_ids) {
        $clientCount = Client::whereIn('recuriter_id', $recruiter_ids)->where('recuriter_id', $recruiter->id)->count();
        $recruiter->Post_count = $clientCount;
    });

    return  $recruiterData->pluck('Post_count');
    // Sort the recruiter data by client_count in descending order
    // $recruiterData = $recruiterData->sortByDesc('Post_count');
     


    return response()->json(['status'=>200,'data'=>$recruiterData]);

}

public function getPostsOfRecruiter(Request $request) {
    $userId = Auth::id();
    $recruiter_id = $request->recruiterId;

    // Retrieve posts of the specified recruiter
    $posts = Client::where('recuriter_id', $recruiter_id)->paginate(10);

    if ($posts->isEmpty()) {
        return response()->json(['status' => 400, 'message' => 'Posts not Found']);
    }

    // Iterate through each post and add the dynamic field "jobStatus"
    $posts->each(function ($post) use ($userId) {
        $userPostMappingRecord = UserPostMapping::where('user_id', $userId)
            ->where('post_id', $post->id)
            ->first();

        $post->jobStatus = $userPostMappingRecord ? $userPostMappingRecord->job_status : null;
    });

    return response()->json(['status' => 200, 'data' => $posts]);
}


public function getActivePostsOfRecruiter(Request $request) {
    $userId = Auth::id();
    $recruiterId = $request->recruiterId;

    $activePosts = Client::where('recuriter_id', $recruiterId)
        ->where('status', '1')
        ->orderBy('audition_date', 'ASC')
        ->paginate(10);

    // if ($activePosts->isEmpty()) {
    //     return response()->json(['status' => 400, 'message' => 'There are no active posts']);
    // }

    $activePosts->each(function ($post) use ($userId) {
        $userPostMappingRecord = UserPostMapping::where('user_id', $userId)
            ->where('post_id', $post->id)
            ->first();

        $post->jobStatus = $userPostMappingRecord ? $userPostMappingRecord->job_status : 0;
    });

    return response()->json(['status' => 200, 'data' => $activePosts]);
}



public function getInActivePostsOfRecruiter(Request $request){

    $userId = Auth::id();
    $recruiterId = $request->recruiterId;

    $activePosts = Client::where('recuriter_id',$recruiterId)->where('status','=','0')->orderBy('audition_date','ASC')->paginate(10);

    $activePosts->each(function ($post) use ($userId) {
        $userPostMappingRecord = UserPostMapping::where('user_id', $userId)
            ->where('post_id', $post->id)
            ->first();

        $post->jobStatus = $userPostMappingRecord ? $userPostMappingRecord->job_status : 0;
    });

    if($activePosts){
        return response()->json(['status'=>200,'data'=>$activePosts]);
    }else{
        return response()->json(['status'=>400,'message'=>'There is No any Inactive Post']);
    }
}


public function removePost(Request $request) {
    $userId = Auth::id();
    $postId = $request->postId;


    // Check if the user has applied to this post
    $userPostMappingRecord = UserPostMapping::where('user_id', $userId)
        ->where('post_id', $postId)
        ->first();

        $jobStatus = 0;

        $post = Client::where('id', $postId)
        ->with('recruiter')
        ->has('recruiter')
        ->first();

    
        if(!$post){
            return response()->json(['status' => 400, 'message' => 'Post Not Found']);
        }

        $post->jobStatus = $jobStatus;

    if ($userPostMappingRecord) {
        $userPostMappingRecord->delete(); // Delete the record if it exists
        return response()->json(['status' => 200, 'data' => $post, 'message' => 'Post deleted successfully']);
    } else {
        return response()->json(['status' => 400, 'message' => 'You have not applied to this post']);
    }
}


}
