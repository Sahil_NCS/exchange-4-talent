<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Helpers\OtpHelper;

use Illuminate\Http\Request;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Auth;
use App\Models\FcmToken;
// use Mail;
use Illuminate\Support\Str;

class AuthControllerAPI extends Controller
{
    use ApiResponser;


    public function login(Request $request)
    {
        $attr = $request->validate([
            'PhoneNo' => 'required'
        ]);
        $mobile_otp = mt_rand(1000, 9999);
        $user = User::where('mobile', $request->PhoneNo)->first();
        
        if (!$user) {
            // Generate OTP
            
            $check = $this->otpCurl($mobile_otp,$request->PhoneNo);

            // Return success response with generated OTP
            return response()->json([
                'status' => 200,
                'message' => 'Unauthenticated user',
                'data' => ['otp' => $mobile_otp]
            ]);
        }
        
        if ($request->PhoneNo == 9876543211) {
            $mobile_otp = 9876;
        } else {
         
            $this->otpCurl($mobile_otp,$request->PhoneNo);
        } 

        
        $user->mobile_otp = $mobile_otp;
        $user->save();
        $mobile_otp = $user->mobile_otp;
        $data = array('name' => $user->name, 'mobile_otp' => $mobile_otp);
    
        return $this->success('Authenticated user');
    }

    public function login1(Request $request)
    {
        $attr = $request->validate([
            'PhoneNo' => 'required'
        ]);
        $mobile_otp = mt_rand(1000, 9999);
       return $user = User::where('mobile', $request->PhoneNo)->first();
        
        if (!$user) {
            // Generate OTP
            
            $check = $this->otpCurl($mobile_otp,$request->PhoneNo);
            $emailcheck = $this->emailOtpCurl($mobile_otp,$request->userEmail);
            // Return success response with generated OTP
            return response()->json([
                'status' => 200,
                'message' => 'Unauthenticated user',
                'data' => ['otp' => $mobile_otp]
            ]);
        }
        
        if ($request->PhoneNo == 9876543211) {
            $mobile_otp = 9876;
        } else {
         
            $this->otpCurl($mobile_otp,$request->PhoneNo);
        } 

        
        $user->mobile_otp = $mobile_otp;
        $user->save();
        $mobile_otp = $user->mobile_otp;
        $data = array('name' => $user->name, 'mobile_otp' => $mobile_otp);
    
        return $this->success('Authenticated user');
    }

    public function login2(Request $request)
    {
        $attr = $request->validate([
            'PhoneNo' => 'required'
        ]);
        $mobile_otp = mt_rand(1000, 9999);
        $user = User::where('mobile', $request->PhoneNo)->where('phone_code',$request->phone_code)->first();
        
        if (!$user) {
            // Generate OTP
            
            $check = $this->otpCurl($mobile_otp,$request->PhoneNo);
            // $emailcheck = $this->emailOtpCurl($mobile_otp,$request->userEmail);
            // Return success response with generated OTP
            return response()->json([
                'status' => 200,
                'message' => 'Unauthenticated user',
                'data' => ['otp' => $mobile_otp]
            ]);
        }
        
        if ($request->PhoneNo == 9876543211) {
            $mobile_otp = 9876;
        } else {
         
            $this->otpCurl($mobile_otp,$request->PhoneNo);
        } 

        
        $user->mobile_otp = $mobile_otp;
        $user->phone_code = $request->phone_code;
        $user->save();
        $mobile_otp = $user->mobile_otp;
        $data = array('name' => $user->name, 'mobile_otp' => $mobile_otp);
    
        return $this->success('Authenticated user');
    }
    
    
    public function logout(Request $request){

        $userId = Auth::id();
        $fmcToken = FcmToken::where('user_id',$userId)->first();
    
        if($fmcToken){
            $fmcToken->delete();
        }
    
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
    
    
        return response()->json(['status'=>200,'message'=>'User LogOut Successfully']);
    }



    public function mobile_otp_check(Request $request)
    {
        $user = User::where('mobile', $request->PhoneNo)->first();
    
        if (!$user) {
            // Create a new user
            $newUser = new User();
            $newUser->mobile = $request->PhoneNo;
            $newUser->save();

            $randomString = Str::random(5);
            $user = User::where('id',$newUser->id)->first();
            if($user->name == null){
                $user->name = $randomString;
            }
    
            $newUserToken = $newUser->createToken('API Token')->plainTextToken;
            $newUserId = $newUser->id;
            $newUserCategory = $newUser->category_id;
            $newUserSubscription = $newUser->subscription_id;
    
            $response = [
                'status' => 200,
                'message' => 'User not found. New user created.',
                'data' => [
                    'token' => $newUserToken,
                    'userId' => $newUserId,
                    'category' => $newUserCategory,
                    'subscription' => $newUserSubscription,
                    'User_data' => $user,
                ]
                
            ];

    
        } else {
            if ($user->mobile_otp == $request->mobile_otp) {
                $user->mobile_otp = 0;
                $user->save();
    
                $token = $user->createToken('API Token')->plainTextToken;
                $userId = $user->id;
                $category = $user->category_id;
                $subscription = $user->subscription_id;
    
                $response = [
                    'status' => 200,
                    'message' => 'mobile_otp matched',
                    'data' => [
                        'token' => $token,
                        'userId' => $userId,
                        'category' => $category,
                        'subscription' => $subscription,
                        'User_data' => $user
                    ]
                ];
            } else {
                $response = [
                    'status' => 500,
                    'message' => 'mobile_otp not matched',
                    'data' => null
                ];
            }
        }
    
        return response()->json($response);
    }


    
    
    public function mobile_otp_check1(Request $request)
    {
        $user = User::where('mobile', $request->PhoneNo)->first();
    
        if (!$user) {
            // Create a new user
            $randomString = Str::random(5);

            $newUser = new User();
            $newUser->name = $randomString;
            $newUser->mobile = $request->PhoneNo;
            $newUser->save();

            
            $user = User::where('id',$newUser->id)->first();
      
    
            $newUserToken = $newUser->createToken('API Token')->plainTextToken;
            $newUserId = $newUser->id;
            
            if($newUser->category_id == null){
                $newUser->category_id = 0;
            }
            $newUserCategory = $newUser->category_id;
        
            if($newUser->subscription_id == null){
                $newUser->subscription_id = 0;
            }
            $newUserSubscription = $newUser->subscription_id;
            // $this->otpCurl($otp,$request->PhoneNo);
            $response = [
                'status' => 200,
                'message' => 'User not found. New user created.',
                'data' => [
                    'token' => $newUserToken,
                    'userId' => $newUserId,
                    'category' => $newUserCategory,
                    'subscription' => $newUserSubscription,
                    'User_data' => $user,
                ]
                
            ];

    
        } else {
            if ($user->mobile_otp == $request->mobile_otp) {
                $user->mobile_otp = 0;
                $user->save();
    
                $token = $user->createToken('API Token')->plainTextToken;
                $userId = $user->id;
                $category = $user->category_id;
                $subscription = $user->subscription_id;
    
                $response = [
                    'status' => 200,
                    'message' => 'mobile_otp matched',
                    'data' => [
                        'token' => $token,
                        'userId' => $userId,
                        'category' => $category,
                        'subscription' => $subscription,
                        'User_data' => $user
                    ]
                ];
            } else {
                $response = [
                    'status' => 500,
                    'message' => 'mobile_otp not matched',
                    'data' => null
                ];
            }
        }
    
        return response()->json($response);
    }


    

    public function mobile_otp_check2(Request $request)
    {
        $user = User::where('mobile', $request->PhoneNo)->where('phone_code',$request->phone_code)->first();
    
        if (!$user) {
            // Create a new user
            $randomString = Str::random(5);

            $newUser = new User();
            $newUser->name = $randomString;
            $newUser->phone_code = $request->phone_code;
            $newUser->mobile = $request->PhoneNo;
            $newUser->save();

            
            $user = User::where('id',$newUser->id)->first();
      
    
            $newUserToken = $newUser->createToken('API Token')->plainTextToken;
            $newUserId = $newUser->id;
            
            if($newUser->category_id == null){
                $newUser->category_id = 0;
            }
            $newUserCategory = $newUser->category_id;
        
            if($newUser->subscription_id == null){
                $newUser->subscription_id = 0;
            }
            $newUserSubscription = $newUser->subscription_id;
            // $this->otpCurl($otp,$request->PhoneNo);
            $response = [
                'status' => 200,
                'message' => 'User not found. New user created.',
                'data' => [
                    'token' => $newUserToken,
                    'userId' => $newUserId,
                    'category' => $newUserCategory,
                    'subscription' => $newUserSubscription,
                    'User_data' => $user,
                ]
                
            ];

    
        } else {
            if ($user->mobile_otp == $request->mobile_otp) {
                $user->mobile_otp = 0;
                $user->save();
    
                $token = $user->createToken('API Token')->plainTextToken;
                $userId = $user->id;
                $category = $user->category_id;
                $subscription = $user->subscription_id;
    
                $response = [
                    'status' => 200,
                    'message' => 'mobile_otp matched',
                    'data' => [
                        'token' => $token,
                        'userId' => $userId,
                        'category' => $category,
                        'subscription' => $subscription,
                        'User_data' => $user
                    ]
                ];
            } else {
                $response = [
                    'status' => 500,
                    'message' => 'mobile_otp not matched',
                    'data' => null
                ];
            }
        }
    
        return response()->json($response);
    }





public function otpCurl($otp,$PhoneNo){
    $message = "Dear user, OTP for your mobile registration is ".$otp." Regards, EXCHANGE4TALENT";

    $mobile = $PhoneNo;
    $curl = curl_init();

    curl_setopt_array($curl, [
    CURLOPT_URL => "https://api.pinnacle.in/index.php/sms/urlsms?sender=EXFRTL&numbers=".urlencode($mobile)."&messagetype=TXT&message=".urlencode($message)."&response=Y&apikey=7712fa-18edf0-c0f4ad-942334-60d4f5",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => [
        "Accept: /",
        "User-Agent: Thunder Client (https://www.thunderclient.com)"
    ],
    ]);

    $response = curl_exec($curl);
    $err = curl_error($curl);
    info("otp curl response: ".$response);
    curl_close($curl);
    return "Dear user, OTP for your mobile registration is ".$otp." Regards, EXCHANGE4TALENT";
}




// public function internationOtpCurl($otp,$PhoneNo,$PhoneCode){

    
//         "sender":"EXFRTL",
//         "message":[
//         {
//         "number":$PhoneCode . $PhoneNo,
//         "text":"Dear user, OTP for your mobile registration is ".$otp." Regards, EXCHANGE4TALENT",
//         "clientuid":"ClientUID",
//         "extrares":"Extra Reserve Parameter"
//         }
//         ],
//         "messagetype":"MESSAGETYPE",
//         "scheduledate":"ScheduleDate",
//         "dltentityid":"1107169115473284768",
//         "dlttempid":"1101668300000071621",
//         "dltheaderid":"0",
//         "dlttagid":"0",
//         "tmid":"Telemarketer ID"
        
        
// }


// public function emailOtpCurl($emailotp,$userEmail){

    
//         $curl = curl_init();

//         $payload = array(
//         "userName" => "latish@exchange4talentglobal.com",
//         "password" => "Exchange@1234",
//         "privateToken" => "558e26be-4456-4670-beea-8761a730fb51",
//         "validity" => "Default"
//         );

//         curl_setopt_array($curl, [
//         CURLOPT_HTTPHEADER => [
//             "Content-Type: application/json-patch+json"
//         ],
//         CURLOPT_POSTFIELDS => json_encode($payload),
//         CURLOPT_URL => "https://authenticate.pinnacle.in/token",
//         CURLOPT_RETURNTRANSFER => true,
//         CURLOPT_CUSTOMREQUEST => "POST",
//         ]);

//        return $response = curl_exec($curl);
//         $error = curl_error($curl);

//         curl_close($curl);


//         if ($error) {
//             echo "cURL Error #:" . $error;
//         } else {

//             // Send Email with OTP
//             $email = $userEmail;
//             Mail::send('email.otp', ['otp' => $emailotp], function ($message) use ($email) {
//                 $message->to($email)
//                     ->subject('Your OTP for Exchange4Talent Verification');
//             });

//             echo 'Email sent with OTP.';

//         }

//         if ($error) {
//         echo "cURL Error #:" . $error;
//         } else {
//         echo $response;
//         }

// }








    // public function mobile_otp_check(Request $request)
    // {
    //         $user=User::where('mobile',$request->PhoneNo)->first();
    //         if ($user->mobile_otp == $request->mobile_otp) {
    //             $user->mobile_otp == 0;
    //             $user->save();
                
    //             return $this->success('mobile_otp matched',200,[
    //                 'token' => $user->createToken('API Token')->plainTextToken,
    //                 'userId' => $user->id,
    //                 'category' => $user->category_id,
    //                 'subscription' => $user->subscription_id

    //             ]);
    //         }
    //         else{
    //             return $this->error('mobile_otp not matched', 500);
    //         }
    // }


    // public function login(Request $request)
    // {
    //     $attr = $request->validate([
    //         'PhoneNo' => 'required'
    //     ]);
        
    //     $user=User::where('mobile',$request->PhoneNo)->first();
    //     if (!$user) {
    //         $mobile_otp = mt_rand(1000,9999);
            
    //         return $this->error('Credentials not match', 401);
    //     }
    //     if($request->PhoneNo == 9876543211)
    //     {
    //         $mobile_otp = 9876;
    //     }
    //     else{
    //         $mobile_otp = mt_rand(1000,9999);
    //     }
    //     $user->mobile_otp= $mobile_otp;
    //     $user->save();

	// 	$data = array('name'=>$user->name, 'mobile_otp'=>$mobile_otp);

    //      Mail::send('mail', $data, function($message)  use ($user){
    //       $message->to($user->email, $user->name)->subject
    //          ('mobile_otp Verification');
    //       $message->from('mobile_otp@rotary3141.org','Rotary Alpha 3141');
    //    });

    //      $message = $mobile_otp." is SECRET mobile_otp to login to the Rotary Alpha 3141 App. This mobile_otp is confidential. NCSWEB requests you to keep your mobile_otp safe. Do NOT share it with anyone.";

    //      $curl = curl_init();

    //      curl_setopt_array($curl, array(
    //        CURLOPT_URL => 'http://smsjust.com/sms/user/urlsms.php?username=nimayate&pass=s!79-QkX&senderid=NCSWEB&message='.urlencode($message).'&dest_mobileno='.urlencode($user->mobile).'&msgtype=TXT&response=Y&dlttempid=1307164483657805392',
    //        CURLOPT_RETURNTRANSFER => true,
    //        CURLOPT_ENCODING => '',
    //        CURLOPT_MAXREDIRS => 10,
    //        CURLOPT_TIMEOUT => 0,
    //        CURLOPT_FOLLOWLOCATION => true,
    //        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //        CURLOPT_CUSTOMREQUEST => 'GET',
    //      ));

    //      $response = curl_exec($curl);

    //      curl_close($curl); 
    //     return $this->success('Authenticated user');
    // }

}