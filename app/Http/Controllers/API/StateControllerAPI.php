<?php 

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class StateControllerAPI extends Controller
{
    public function getStatesByCountry($countryId)
    {
        $country = Country::findOrFail($countryId);
        $states = $country->states;
        return response()->json(['states' => $states]);
    }

    public function updateSelectedState(Request $request)
{
    $userId = Auth::id();
    // $userId = $request->input('users_id');
    $selectedState = $request->input('selected_state');
    $country = State::where('id', $selectedState)->pluck('name')->first();
    // Perform validation if needed, e.g., check if the state is valid.

    // Update the selected state in the 'users' table
    DB::table('users')->where('id', $userId)->update(['state' => $country]);

    return response()->json(['status' => 200, 'message' => 'Selected state updated successfully']);
}
}