<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CategoryControllerAPI;
use App\Http\Controllers\API\AttributeControllerAPI;
use App\Http\Controllers\API\SubcategoryControllerAPI;
use App\Http\Controllers\API\SubscriptionControllerAPI;
use App\Http\Controllers\API\AttributeUserControllerAPI;
use App\Http\Controllers\API\UsersControllerAPI;
use App\Http\Controllers\API\BookingControllerAPI;
use App\Http\Controllers\API\FavouriteControllerAPI;
use App\Http\Controllers\API\HomeControllerAPI;
use App\Http\Controllers\API\SearchControllerAPI;
use App\Http\Controllers\API\BannerControllerAPI;
use App\Http\Controllers\API\AuthControllerAPI;
use App\Http\Controllers\API\RatingControllerAPI;
use App\Http\Controllers\API\TagsControllerAPI;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('/login', [AuthControllerAPI::class, 'login']);
Route::post('/otp', [AuthControllerAPI::class, 'mobile_otp_check']);


Route::middleware('auth:sanctum')->group(function () {
// AttributesUsers API routes
Route::get('/attribute_user', [AttributeUserControllerAPI::class, 'index']);
Route::post('/attribute_users', [AttributeUserControllerAPI::class, 'store']);
Route::put('/attribute_user/{attribute_user}', [AttributeUserControllerAPI::class, 'update']);
Route::delete('/attribute_users/{attribute_user}', [AttributeUserControllerAPI::class, 'destroy']);

// Attributes API routes
Route::get('/attributes', [AttributeControllerAPI::class, 'index']);
Route::post('/attributes', [AttributeControllerAPI::class, 'store']);
Route::put('/attributes/{attribute}', [AttributeControllerAPI::class, 'update']);
Route::delete('/attributes/{attribute}', [AttributeControllerAPI::class, 'destroy']);


// Users API routes
Route::get('/users', [UsersControllerAPI::class, 'index']);
Route::post('/users', [UsersControllerAPI::class, 'store']);
Route::patch('/users/{users}/update', [UsersControllerAPI::class, 'update']);
Route::delete('/users/{users}/destroy', [UsersControllerAPI::class, 'destroy']);

// get single User
Route::get('users/{userId}', [UsersControllerAPI::class, 'getSingleUser']);
// Route::get('subcategories/{category}', 'SubcategoryController@getSubcategoriesByCategory');
// Route::patch('/users/{users}/update', [UsersControllerAPI::class, 'update']);



// Bookings API routes
Route::get('/bookings', [BookingControllerAPI::class, 'index']);
Route::post('/bookings', [BookingControllerAPI::class, 'store']);
Route::put('/bookings/{bookings}', [BookingControllerAPI::class, 'update']);
Route::delete('/bookings/{bookings}', [BookingControllerAPI::class, 'destroy']);


// Bookings API routes
Route::get('/favourites', [FavouriteControllerAPI::class, 'index']);
Route::post('/favourites', [FavouriteControllerAPI::class, 'store']);
// Route::put('/favourites/{favourites}', [FavouriteControllerAPI::class, 'update']);
Route::delete('/favourites/{favourites}/destroy', [FavouriteControllerAPI::class, 'destroy']);

// Subcategories API routes
Route::get('/subcategories', [SubcategoryControllerAPI::class, 'index']);
Route::post('/subcategories', [SubcategoryControllerAPI::class, 'store']);
Route::put('/subcategories/{subcategory}/update', [SubcategoryControllerAPI::class, 'update']);
Route::delete('/subcategories/{subcategory}/destroy', [SubcategoryControllerAPI::class, 'destroy']);


// Subscription API routes
Route::get('/subscriptions', [SubscriptionControllerAPI::class, 'index']);
Route::post('/subscriptions', [SubscriptionControllerAPI::class, 'store']);
Route::put('/subscriptions/{subscription}/update', [SubscriptionControllerAPI::class, 'update']);
Route::delete('/subscriptions/{subscription}/destroy', [SubscriptionControllerAPI::class, 'destroy']);


// Categories API routes
Route::get('/categories', [CategoryControllerAPI::class, 'index']);
Route::post('/categories', [CategoryControllerAPI::class, 'store']);
Route::put('/categories/{category}/update', [CategoryControllerAPI::class, 'update']);
Route::delete('/categories/{category}/destroy', [CategoryControllerAPI::class, 'destroy']);

Route::get('/categoryTitle',[CategoryControllerAPI::class,'categorytitle']);
Route::get('/subcategoryBycategories/{categoryId}',[CategoryControllerAPI::class, 'getSubcategoriesByCategory']);



//Home Page API routes
Route::get('/home', [HomeControllerAPI::class, 'index']);

//Search Page API routes
Route::get('/search/categories', [SearchControllerAPI::class, 'index']);
Route::get('/search', [SearchControllerAPI::class, 'search']);


//Banner Page API routes
Route::get('/banners', [BannerControllerAPI::class, 'index']);
Route::post('/banners', [BannerControllerAPI::class, 'store']);
Route::put('/banners/{banner}/update', [BannerControllerAPI::class, 'update']);
Route::delete('/banners/{banner}/destroy', [BannerControllerAPI::class, 'destroy']);


//Rating Page API routes
Route::post('/ratings', [RatingControllerAPI::class, 'store']);


//Banner Page API routes
Route::get('/tags', [TagsControllerAPI::class, 'index']);
Route::post('/tags', [TagsControllerAPI::class, 'store']);
Route::put('/tags/{tags}/update', [TagsControllerAPI::class, 'update']);
Route::delete('/tags/{tags}/destroy', [TagsControllerAPI::class, 'destroy']);

});