<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RatingControllerAPI extends Controller
{

    public function index()
    {
        $ratings = Rating::whereNotNull('review')->with('user')->get();
        return response()->json(['reviews' => $ratings]);
    }
    
    

    public function store(Request $request)
    {
        $request->validate([
            'rating' => 'required|numeric|between:1,5',
            // 'review' => 'required',
            'talent_id' => 'required|exists:users,id',
        ]);
    
        $rating = new Rating();
        (float)$rating->rating = $request->rating;
        $rating->review = $request->review;
        $rating->user_id = Auth::id();
        $rating->talent_id = $request->talent_id;
        $rating->save();
    
        $rate = (float)$request->rating;

        $ratingsData = Rating::where('talent_id', $request->talent_id)->get();
        $ratings = $ratingsData->pluck('rating')->toArray();
    
        // Calculate the sum of ratings
        $sum = array_sum($ratings);
    
        // Count the number of ratings
        $count = count($ratings);
    
        // Calculate the average
        (float)$average = $sum / $count;
    
        // Cast the average to a float with one decimal place
        // $average = floatval(number_format($average, 1));
    
        // Update the 'average_rating' field in the talent table
        $user = User::where('id', $request->talent_id)->first();
        if ($user) {
            $user->update(['rating' => $average]);
        }
    
        return response()->json([
            'status' => 200,
            'rating' =>number_format($rate, 2),
            'average_rating' => number_format($average,2),
            'message' => 'Rating Added successfully'
        ]);
    }
    
    



    public function allreview(Request $request)
    {
        $userId = Auth::id();
        $talent_id = $request->input("talent_id");
    
        // Retrieve review data from the Rating model for the specified talent
        $reviews = Rating::where('talent_id', $talent_id)->get();
    
        // Initialize an array to store review details
        $reviewData = [];
    
        foreach ($reviews as $review) {
            $comment = $review->review;
            (float)$rating = $review->rating;
            $created_at = $review->created_at;

            // Get the user ID who provided the review
            $userID = $review->user_id;
    
            // Get the username from the User model
            $user = User::select('name','avatar')->find($userID);
            $username = $user ? $user->name : null;
            $avatar = $user ? $user->avatar : null;
    
            // Add the review data to the array
            $reviewData[] = [
                'user_id' => $userID,
                'username' => $username,
                'avatar' => $avatar,
                'comment' => $comment,
                'rating' => number_format($rating),
                'created_at' => $created_at
            ];
        }
    
        // Read comments from the review data and store them in an array

    
        return response()->json([
            'status' => 200,
            'message' => "Review Data",
            'reviews' => $reviewData,
          
        ]);
    }




    public function update(Request $request, Rating $ratings){

        $ratings->rating = $request->input('rating');
        $ratings->review = $request->input('review');
        $ratings->save();
    
        
        return response()->json(['status'=>200, 'message'=>'rating and review updaded successfully..', 'ratings' => $ratings]);
    }
    
}
