<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BannerControllerAPI extends Controller
{
    public function index()
    {
        // Retrieve all banners
        $banners = Banner::all();

        // Return the banners data as JSON response
        return response()->json($banners);
    }




    public function store(Request $request)
{
    $Banner = new Banner();
    $Banner->title = $request->input('title');
    $Banner->description = $request->input('description');
    $Banner->types = $request->input('types');
    
    // Handle image upload
    if ($request->hasFile('image')) {
        $file = $request->file('image');
        $path = $file->store('bannerimage', 'public');
        $fullImagePath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $path;
        $Banner->image = $fullImagePath;
    }
    
    $Banner->save();

    return response()->json(['status'=>200,'Banner' => $Banner]);
}


public function store1(Request $request)
{
    $Banner = new Banner();
    $Banner->title = $request->input('title');
    $Banner->description = $request->input('description');
    $Banner->types = $request->input('types');
    
 
    // if ($request->hasFile('video')) {
    //     $video = $request->file('video');
    //     $videoPath = $video->store('bannerimage', 'public');
    //     $fullVideoPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $videoPath;
    //     $Banner->video = $fullVideoPath;
    // }

    if ($request->hasFile('video')) {
        $file = $request->file('video');
        $imagePath = "banner-videos/" . time() . ' ' . $file->getClientOriginalName();
        
        Storage::disk("s3")->put($imagePath, file_get_contents($file));
        Storage::disk('s3')->setVisibility($imagePath, 'public');
        $url = Storage::disk("s3")->url($imagePath);
        
        $Banner->video = $url;
    }
    
    $Banner->save();

    return response()->json(['status' => 200, 'Banner' => $Banner]);
}
    



public function update(Request $request, Banner $banner)
{
    // Validate the request data
    $validatedData = $request->validate([
        // 'title' => 'required',
        // 'description' => 'required',
        // 'types' => 'required',
        'video' => 'nullable|file|mimes:mp4|max:2048000' // Adjust max size as needed
    ]);
    
    if ($request->hasFile('video')) {
        // Load the banner record (assuming you have the banner ID)
        $banner = Banner::findOrFail($bannerId);
    
        // Delete previous video if it exists
        if ($banner->video) {
            $filename = basename($banner->video);
            Storage::disk("s3")->delete('banner-videos/' . $filename);
        }
    
        $file = $request->file('video');
        $videoPath = "banner-videos/" . time() . ' ' . $file->getClientOriginalName();
        
        Storage::disk("s3")->put($videoPath, file_get_contents($file));
        Storage::disk('s3')->setVisibility($videoPath, 'public');
        $url = Storage::disk("s3")->url($videoPath);
        
        $banner->video = $url;
    }
    
    $banner->update();
    
    return response()->json(['status' => 200, 'banner' => $banner]);
    
}
    

    public function destroy(Banner $banner)
    {
        // Delete the banner
        if ($banner->video) {
            $filename = basename($banner->video);
            Storage::disk("s3")->delete('banner-videos/' . $filename);
        }
    

        $banner->delete();

        // Return success message as JSON response
        return response()->json(['message' => 'Banner deleted successfully']);
    }
}
