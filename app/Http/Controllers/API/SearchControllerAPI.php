<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subcategory;
use App\Models\Category;
use Laravel\Scout\Builder;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

use Illuminate\View\View;

class SearchControllerAPI extends Controller
{
    public function index()
    {
    
        $search_categories = Category::inRandomOrder()->take(5)->get();
        return response()->json(['home_categories ' => $search_categories]);
    }


    // public function Search(Request $request)
    // {
    //     // return $request; 
    //     if ($request->filled('search')) {
    //         $search_categories = Category::search($request->search)->get();
    //     } else {
    //         $search_categories = Category::get();
    //     }
    
    //     return response()->json($search_categories);
    // }

    // public function Search(Request $request)
    // {
    //     $searchQuery = $request->input('search');
        
    //     // Search for users
    //     $users = User::where('name', 'LIKE', '%' . $searchQuery . '%')
    //         ->get();
        
    //     // Search for subcategories
    //     $subcategories = Subcategory::where('title', 'LIKE', '%' . $searchQuery . '%')
    //         ->get();
        
    //     // Search for categories
    //     $categories = Category::where('title', 'LIKE', '%' . $searchQuery . '%')
    //         ->get();
        
    //     $responseData = [];
        
    //     if (!$users->isEmpty()) {
    //         $responseData['users'] = $users;
    //     }
        
    //     if (!$subcategories->isEmpty()) {
    //         $responseData['subcategories'] = $subcategories;
    //     }
        
    //     if (!$categories->isEmpty()) {
    //         $responseData['categories'] = $categories;
    //     }
        
    //     return response()->json($responseData);
    // }
    

    
    public function search(Request $request)
    {
        $query = $request->input('search');
    
        $searchResults = [];
    
        if ($request->filled('search')) {
            $searchResults = User::whereHas('category', function ($categoryQuery) use ($query) {
                    $categoryQuery->where('title', 'LIKE', '%' . $query . '%');
                })
                ->orWhereHas('subcategory', function ($subcategoryQuery) use ($query) {
                    $subcategoryQuery->where('title', 'LIKE', '%' . $query . '%');
                })
                ->orWhere('name', 'LIKE', '%' . $query . '%')
                ->get();
        }
    
        return response()->json(['status' => 200,
        'message' => 'user favourite data.' ,'data'=>$searchResults]);
    }
    


    public function search1(Request $request)
    {
        $query = $request->input('search'); // Use input() method to get request input
        
        $searchResults = [];
        
        if (!empty($query)) {
            $searchResults = User::where(function ($userQuery) use ($query) {
                $userQuery->whereHas('category', function ($categoryQuery) use ($query) {
                    $categoryQuery->where('title', 'LIKE', '%' . $query . '%');
                })->orWhere('name', 'LIKE', '%' . $query . '%');
            })->whereNotIn('subscription_id', [0])->paginate(10);
        }
        // else {
        //     $searchResults = User::get();
        // }
        
        return response()->json([
            'status' => 200,
            'message' => 'user favourite data.',
            'data' => $searchResults
        ]);
    }
    


    public function search2(Request $request)
    {
        $query = $request->input('search'); // Use input() method to get request input
        
        $searchResults = [];
        
        // if (!empty($query)) {
        //     $searchResults = User::whereHas('category', function ($categoryQuery) use ($query) {
        //             $categoryQuery->where('title', 'LIKE', '%' . $query . '%')->whereNotIn('subscription_id', [0]);
        //         })->orWhere('name', 'LIKE', '%' . $query . '%')->whereNotIn('subscription_id', [0])->paginate(10);
        // }
        if (!empty($query)) {
            $searchResults = User::where(function ($userQuery) use ($query) {
                $userQuery->whereHas('category', function ($categoryQuery) use ($query) {
                    $categoryQuery->where('title', 'LIKE', '%' . $query . '%');
                })->orWhere('name', 'LIKE', '%' . $query . '%');
            })->whereNotIn('subscription_id', [0])->paginate(10);
        }
        // else {
        //     $searchResults = User::get();
        // }
        
        return response()->json([
            'status' => 200,
            'message' => 'user favourite data.',
            'data' => $searchResults
        ]);
    }



}