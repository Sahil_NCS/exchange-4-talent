<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\user;
use App\Models\Banner;
use App\Models\Recruiter;
use App\Models\Subscription;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;

class HomeControllerAPI extends Controller
{
    public function index()
    {
        // $home_categories = Category::get();
         $home_categories = Category::orderBy('title', 'asc')->take(15)->get();

        $home_subCategories = Subcategory::inRandomOrder()->take(2)->get();
        $banners = Banner::inRandomOrder()->take(5)->get();
        $random_users = User::inRandomOrder()->take(5)->get();
        
        return response()->json([
            'status' => 200,
            'categories' => $home_categories,
            'banners' => $banners,
            'subCategories' => $home_subCategories,
            'random_users' => $random_users
        ]);
    }
    





    public function index1()
    {
        // $home_categories = Category::get();
         $home_categories = Category::orderBy('title', 'asc')->take(30)->get();

        // $home_subCategories = Subcategory::inRandomOrder()->take(2)->get();
        $banners = Banner::inRandomOrder()->take(5)->get();
        $random_users = User::whereNotIn('subscription_id', [0,24])->inRandomOrder()->take(5)->get();

        $recruiters = Recruiter::all();

        $subscriptions = Subscription::all();

        $formattedSubscriptions = $subscriptions->map(function ($subscription) {

            $description = $subscription->description;
            $description = str_replace(['[', ']', '\''], '', $description);
            $descriptionArray = explode(',', $description);
            $descriptionArray = array_map('trim', $descriptionArray);
        
            return [
                'id' => $subscription->id,
                'title' => $subscription->title,
                'color_code' => $subscription->color_code,
                'subscriptionData' => [
                    'price' => $subscription->price,
                    'plan' => $subscription->plan,
                    'validity' => $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months',
                   
                        'description' => $descriptionArray,
                        'images' => $subscription->images,
                        'videos' => $subscription->videos,
                        'image_changes' => $subscription->image_changes,
                        'video_changes' => $subscription->video_changes,
                        'max_images_size' => $subscription->max_images_size,
                        'max_video_size' => $subscription->max_video_size,
                   
                ],
               
                'created_at' => $subscription->created_at,
                'updated_at' => $subscription->updated_at,
            ];
        });

                // Split the formatted subscriptions into two parts
           // Step 1: Sort the subscriptions data in descending order based on their IDs
            $sortedSubscriptions = $formattedSubscriptions->sortByDesc('id');

            // Step 2: Take the first two subscriptions for horizontal display
            $verticalSubscriptions = $sortedSubscriptions->take(2)->values();

            // Step 3: Take the next two subscriptions for vertical display
            $horizontalSubscriptions = $sortedSubscriptions->slice(2, 2)->values();
            // $color_Code =$subscription->color_code;

        
        return response()->json([
            'status' => 200,
            'categories' => $home_categories,
            // 'color_code'=>color_Code,
            'verticalSubscriptions'=>$verticalSubscriptions,
            'horizontalSubscriptions'=>$horizontalSubscriptions,
            'banners_videos' => $banners,
            'recruiters' => $recruiters,
            'random_users' => $random_users
           
        ]);
    }

    public function index2()
    {
        // $home_categories = Category::get();
         $home_categories = Category::orderBy('title', 'asc')->take(30)->get();

        // $home_subCategories = Subcategory::inRandomOrder()->take(2)->get();
        $banners = Banner::inRandomOrder()->take(5)->get();
        $random_users = User::whereNotIn('subscription_id', [0,24])->inRandomOrder()->take(5)->get();

        $recruiters = Recruiter::all();

        $subscriptions = Subscription::all();

        $formattedSubscriptions = $subscriptions->map(function ($subscription) {

            $description = $subscription->description;
            $description = str_replace(['[', ']', '\''], '', $description);
            $descriptionArray = explode(',', $description);
            $descriptionArray = array_map('trim', $descriptionArray);
        
            return [
                'id' => $subscription->id,
                'title' => $subscription->title,
                'color_code' => $subscription->color_code,
                'subscriptionData' => [
                    'price' => $subscription->price,
                    'plan' => $subscription->plan,
                    'validity' => $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months',
                   
                        'description' => $descriptionArray,
                        'images' => $subscription->images,
                        'videos' => $subscription->videos,
                        'image_changes' => $subscription->image_changes,
                        'video_changes' => $subscription->video_changes,
                        'max_images_size' => $subscription->max_images_size,
                        'max_video_size' => $subscription->max_video_size,
                   
                ],
               
                'created_at' => $subscription->created_at,
                'updated_at' => $subscription->updated_at,
            ];
        });

                // Split the formatted subscriptions into two parts
           // Step 1: Sort the subscriptions data in descending order based on their IDs
            $sortedSubscriptions = $formattedSubscriptions->sortByDesc('id');

            // Step 2: Take the first two subscriptions for horizontal display
            $verticalSubscriptions = $sortedSubscriptions->take(2)->values();

            // Step 3: Take the next two subscriptions for vertical display
            $horizontalSubscriptions = $sortedSubscriptions->slice(2, 2)->values();
            // $color_Code =$subscription->color_code;

        
        return response()->json([
            'status' => 200,
            'categories' => $home_categories,
            // 'color_code'=>color_Code,
            'verticalSubscriptions'=>$verticalSubscriptions,
            'horizontalSubscriptions'=>$horizontalSubscriptions,
            'banners_videos' => $banners,
            'recruiters' => $recruiters,
            'random_users' => $random_users
           
        ]);
    }

    

}