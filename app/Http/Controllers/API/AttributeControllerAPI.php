<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attribute;
// use Illuminate\Support\Facades\Storage;

class AttributeControllerAPI extends Controller
{
    public function index()
    {
        $attributes = Attribute::with('subcategory')->get();
        return response()->json(['attributes' => $attributes]);
    }

    public function store(Request $request)
    {
        $attribute = new Attribute();
        $attribute->title = $request->input('title');
        $attribute->data_type = $request->input('data_type');
        $attribute->subcategory_id = $request->input('subcategory_id');
        $attribute->save();

        return response()->json(['attribute' => $attribute], 201);
    }

    public function update(Request $request, Attribute $attribute)
    {
        $attribute->title = $request->input('title');
        $attribute->data_type = $request->input('data_type');
        $attribute->subcategory_id = $request->input('subcategory_id');
        $attribute->save();

        return response()->json(['attribute' => $attribute]);
    }

    public function destroy(Attribute $attribute)
    {
        $attribute->delete();

        return response()->json(['message' => 'Attribute deleted successfully']);
    }
}
