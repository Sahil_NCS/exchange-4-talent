<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AttributeUser;
// use Illuminate\Support\Facades\Storage;

class AttributeUserControllerAPI extends Controller
{
    public function index()
    {
        $attribute_user = AttributeUser::all();
        return response()->json(['attribute_user' => $attribute_user]);
        // return $attribute_user;
    }

    public function store(Request $request)
    {
        $attribute_user = new AttributeUser();
        $attribute_user->attribute_id = $request->input('attribute_id');
        $attribute_user->user_id = $request->input('user_id');
        $attribute_user->value = $request->input('value');
        $attribute_user->save();

        return response()->json(['attribute_user' => $attribute_user], 201);
    }

    public function update(Request $request, AttributeUser $attribute_user)
    {
        $attribute_user->attribute_id = $request->input('attribute_id');
        $attribute_user->user_id = $request->input('user_id');
        $attribute_user->value = $request->input('value');
        $attribute_user->save();

        return response()->json(['attribute_user' => $attribute_user]);
    }

    public function destroy(AttributeUser $attribute_user)
    {
        $attribute_user->delete();

        return response()->json(['message' => 'Attribute deleted successfully']);
    }
}
