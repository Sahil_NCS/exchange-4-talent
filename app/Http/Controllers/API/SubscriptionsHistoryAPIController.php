<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\SubscriptionsHistory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class SubscriptionsHistoryAPIController extends Controller
{
    public function index()
    {
        $subscriptionsHistory = SubscriptionsHistory::all();
        return response()->json($subscriptionsHistory);
    }




    public function userSubscriptionsHistory(Request $request)
    {
        $userId = Auth::id();
        $subscriptionHistories = SubscriptionsHistory::where('user_id', $userId)->paginate(10);
    
        if ($subscriptionHistories->isEmpty()) {
            return response()->json([
                'status' => 200,
                'user_id' => $userId,
                'subscription_histories' => []
            ]);
        }

        
        $subscriptionsData = [];
    
        foreach ($subscriptionHistories as $subscriptionHistory) {
            $subscriptionId = $subscriptionHistory->subscription_id;
            $price = $subscriptionHistory->price;
            $buy_date = $subscriptionHistory->buy_date;
            $expire_date = $subscriptionHistory->expire_date;
    
            // Compare the expire_date with the current date
            $currentDate = Carbon::now(); 
            $expireDate = Carbon::parse($expire_date);
    
            // Check if the subscription is active or expired
            $isActive = $expireDate >= $currentDate ? 1 : 0;

             // Update the isActive value in the database
            $subscriptionHistory->isActive = $isActive;
            $subscriptionHistory->save();
    
            // Get the title, validity, and images from the subscriptions table
            $subscription = Subscription::select('title', 'validity', 'images')->find($subscriptionId);
    
            if ($subscription) {
                $title = $subscription->title;
                $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months';
                $images = $subscription->images;
    
                $subscriptionsData[] = [
                    'subscription_id' => $subscriptionId,
                    'title' => $title,
                    'price' => $price,
                    'buy_date' => $buy_date,
                    'expire_date' => $expire_date,
                    'isActive' => $isActive,
                    'validity' => $validityMonths,
                    'images' => $images
                ];
            }
        }
    
        return response()->json([
            'status' => 200,
            'user_id' =>  $userId,
            'subscription_histories' => $subscriptionsData
        ]);
    }
    

    public function userSubscriptionsHistory1(Request $request)
    {
        $userId = Auth::id();
        $subscriptionHistories = SubscriptionsHistory::where('user_id', $userId)->paginate(10);
    
        if ($subscriptionHistories->isEmpty()) {
            return response()->json([
                'status' => 200,
                'user_id' => $userId,
                'subscription_histories' => []
            ]);
        }

        
        $subscriptionsData = [];
    
        foreach ($subscriptionHistories as $subscriptionHistory) {
            $subscriptionId = $subscriptionHistory->subscription_id;
            $price = $subscriptionHistory->price;
            $buy_date = $subscriptionHistory->buy_date;
            $expire_date = $subscriptionHistory->expire_date;
    
            // Compare the expire_date with the current date
            $currentDate = Carbon::now(); 
            $expireDate = Carbon::parse($expire_date);
    
            // Check if the subscription is active or expired
            $isActive = $expireDate >= $currentDate ? 1 : 0;

             // Update the isActive value in the database
            $subscriptionHistory->isActive = $isActive;
            $subscriptionHistory->save();
    
            // Get the title, validity, and images from the subscriptions table
            $subscription = Subscription::select('title', 'validity', 'images')->find($subscriptionId);
    
            if ($subscription) {
                $title = $subscription->title;
                $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months';
                $images = $subscription->images;
    
                $subscriptionsData[] = [
                    'subscription_id' => $subscriptionId,
                    'title' => $title,
                    'price' => $price,
                    'buy_date' => $buy_date,
                    'expire_date' => $expire_date,
                    'isActive' => $isActive,
                    'validity' => $validityMonths,
                    'images' => $images
                ];
            }
        }
    
        return response()->json([
            'status' => 200,
            'user_id' =>  $userId,
            'subscription_histories' => $subscriptionsData,
            'history' => $subscriptionHistories           
        ]);
    }
    

    // public function store(Request $request)
    // {
    //     $validatedData = $request->validate([
    //         'user_id' => 'required|exists:users,id',
    //         'talent_id' => 'nullable|exists:talents,id',
    //         'subscription_id' => 'nullable|exists:subscriptions,id',
    //         'start_date' => 'required|date',
    //     ]);

    //     $subscriptionHistory = SubscriptionsHistory::create($validatedData);

    //     return response()->json($subscriptionHistory, Response::HTTP_CREATED);
    // }

    // public function show($id)
    // {
    //     $subscriptionHistory = SubscriptionsHistory::findOrFail($id);
    //     return response()->json($subscriptionHistory);
    // }

    // public function update(Request $request, $id)
    // {
    //     $subscriptionHistory = SubscriptionsHistory::findOrFail($id);

    //     $validatedData = $request->validate([
    //         'user_id' => 'required|exists:users,id',
    //         'talent_id' => 'nullable|exists:talents,id',
    //         'subscription_id' => 'nullable|exists:subscriptions,id',
    //         'start_date' => 'required|date',
    //     ]);

    //     $subscriptionHistory->update($validatedData);

    //     return response()->json($subscriptionHistory);
    // }

    public function destroy($id)
    {
        $subscriptionHistory = SubscriptionsHistory::findOrFail($id);
        $subscriptionHistory->delete();

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
