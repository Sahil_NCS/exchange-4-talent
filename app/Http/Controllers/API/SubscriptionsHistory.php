<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionsHistory extends Model
{
    protected $table = 'subscriptions_history';

    // protected $fillable = [
    //     'user_id',
    //     'talent_id',
    //     'subscription_id',
    //     'start_date',
    // ];

    protected $fillable = ['user_id', 'subscription_id','price', 'buy_date', 'expire_date'];

    // Define the relationships if needed
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



    public function subscription()
    {
        return $this->belongsTo(Subscription::class, 'subscription_id');
    }
}
