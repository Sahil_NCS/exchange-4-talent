<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Feedback;
use Illuminate\Support\Facades\Auth;

class FeedBackFormControllerAPI extends Controller{


    public function feedbackform(Request $request){

        $user_id = Auth::id();
        Feedback::create([
            'user_id' => $user_id,
            'feedback' => $request->feedback
        ]);
    
         return response()->json(['status'=>200,'message'=>'feedback submitted']);
    }
    
    
    
    public function getfeedback(Request $request){
    
        $feedback = Feedback::where('user_id',$request->user_id)->get();
    
        return response()->json(['status'=>200,'message'=>'feedback submitted','data'=>$feedback]);
    }


}