<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Subscription;
use App\Models\Rating;
use Illuminate\Support\Facades\Storage;
use App\Models\Tag;
use App\Models\Image;
use App\Models\Video;
use App\Models\FcmToken;
use Illuminate\Support\Facades\Schema;
// use Carbon\Carbon;
use \DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Aws\S3\S3Client;
use Illuminate\Support\Facades\Session;


class UsersControllerAPI extends Controller
{
    public function index()
    {
        // $users = User::with('subcategory')->get();
        $users = User::whereNotIn('subscription_id', [0])->with('subcategory', 'category', 'subscription')->get();

        $users = $users->map(function ($user) {
            $user['category_title'] = $user->category ? $user->category->title : null;
            $user['subcategory_title'] = $user->subcategory ? $user->subcategory->title : null;
            $user['subscription_title'] = $user->subcategory ? $user->subcategory->title : null;
            unset($user['category']);
            unset($user['subcategory']);
            unset($user['subscription']);
            return $user;
        });


        // $categoryIds = explode(',',$users->category_ids);

        // $category = Category::whereIn('id',$categoryIds)->get();

        $category = Category::pluck('title');
      
        $subcategory = Subcategory::pluck('title');

        return response()->json(['status'=>200,'users' => $users , 'category' => $category , 'subcategory' => $subcategory , ]);
    }

    public function index1()
    {
        $perPage = 10;
        $users = User::whereNotIn('subscription_id', [0])
            ->with('subcategory', 'category', 'subscription')
            ->paginate($perPage);
    
        $users->getCollection()->transform(function ($user) {
            $user->category_title = $user->category ? $user->category->title : null;
            $user->subcategory_title = $user->subcategory ? $user->subcategory->title : null;
            $user->subscription_title = $user->subscription ? $user->subscription->title : null;
            unset($user->category);
            unset($user->subcategory);
            unset($user->subscription);
            return $user;
        });
    
        $category = Category::pluck('title');
        $subcategory = Subcategory::pluck('title');
    
        return response()->json([
            'status' => 200,
            'data' => $users,
            'category' => $category,
            'subcategory' => $subcategory,
        ]);
    }
    


    public function getSingleUser(Request $request)
    {

        $userId = $request->input("user_id");
        $user = User::find($userId);
        
        if (!$user) {
            return response()->json(['status' => 404, 'error' => 'User not found']);
        }
    
        $categoryIds = $user->category->pluck('id');
    
        $randomUsers = User::whereHas('category', function ($query) use ($categoryIds) {
            $query->whereIn('id', $categoryIds);
        })->inRandomOrder()->take(5)->get();
    
        $ratings = Rating::where('talent_id', $userId)->get();
    
        $totalRatings = $ratings->count();
        (float)$sumRatings = $ratings->sum('rating');
        
        // Calculate the average rating with two decimal places
        $averageRating = $totalRatings > 0 ? round($sumRatings / $totalRatings, 2) : 0;
    
        // Filter stars ratings and calculate their average
        $fiveStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 5;
        });
        $fourStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 4;
        });
        $threeStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 3;
        });
        $twoStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 2;
        });
        $oneStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 1;
        });

    
        $totalFiveStarRatings = $fiveStarRatings->map(fn($rating) => round($rating->rating))->count();
       
        (float)$averageFiveStarRating = $totalFiveStarRatings > 0 ? round( $totalFiveStarRatings / $totalRatings, 2) : 0;
        // round($fiveStarRatings->sum('rating')
        $totalFourStarRatings = $fourStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageFourStarRating = $totalFourStarRatings > 0 ? round( $totalFourStarRatings / $totalRatings, 2) : 0;
    
        $totalThreeStarRatings = $threeStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageThreeStarRating = $totalThreeStarRatings > 0 ? round($totalThreeStarRatings / $totalRatings, 2) : 0;
    
        $totalTwoStarRatings = $twoStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageTwoStarRating = $totalTwoStarRatings > 0 ? round($totalTwoStarRatings / $totalRatings, 2) : 0;
    
        $totalOneStarRatings = $oneStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageOneStarRating = $totalOneStarRatings > 0 ? round($totalOneStarRatings / $totalRatings, 2) : 0;
    
        $ratings = [
           [ 'total' => $totalFiveStarRatings,
            'average' => number_format($averageFiveStarRating,2),
            'star'=>5
        ],
           [ 'total' => $totalFourStarRatings,
            'average' => number_format($averageFourStarRating,2),
            'star'=>4
        ],
            ['total' => $totalThreeStarRatings,
            'average' => number_format($averageThreeStarRating,2),
            'star'=>3
        ],
            ['total' => $totalTwoStarRatings,
            'average' => number_format($averageTwoStarRating,2),
            'star'=>2
        ],
            ['total' => $totalOneStarRatings,
            'average' => number_format($averageOneStarRating,2),
            'star'=>1
            ]
        ];

        if ($user->image1 !== null) {
            $banner['url'] = $user->image1;
            $banner['thumbnail'] = $user->image1;
        }
        if ($user->image2 !== null) {
            $banner['url'] = $user->image2;
            $banner['thumbnail'] = $user->image2;
        }
        if ($user->image3 !== null) {
            $banner['url'] = $user->image3;
            $banner['thumbnail'] = $user->image3;
        }
        if ($user->image4 !== null) {
            $banner['url'] = $user->image4;
            $banner['thumbnail'] = $user->image4;
        }
        if ($user->image5 !== null) {
            $banner['url'] = $user->image5;
            $banner['thumbnail'] = $user->image5;
        }
        if ($user->image6 !== null) {
            $banner['url'] = $user->image6;
            $banner['thumbnail'] = $user->image6;
        }
        if ($user->image7 !== null) {
            $banner['url'] = $user->image7;
            $banner['thumbnail'] = $user->image7;
        }



        $banner = [
            [
                'url' => $user->video1,
                'thumbnail' => $user->avatar,
                'type' =>"video"
            ],
            [
                'url' => $user->avatar,
                'thumbnail' => $user->avatar,
                'type' =>"Image"
            ],
            [
                'url' => $user->image1,
                'thumbnail' => $user->image1,
                'type' =>"Image"
            ],
            [
                'url' => $user->image2,
                'thumbnail' => $user->image2,
                'type' =>"Image"
            ],
            [
                'url' => $user->image3,
                'thumbnail' => $user->image3,
                'type' =>"Image"
            ],
            [
                'url' => $user->image4,
                'thumbnail' => $user->image4,
                'type' =>"Image"
            ],
            [
                'url' => $user->image5,
                'thumbnail' => $user->image5,
                'type' =>"Image"
            ],
            [
                'url' => $user->image6,
                'thumbnail' => $user->image6,
                'type' =>"Image"
            ],
            [
                'url' => $user->image7,
                'thumbnail' => $user->image7,
                'type' =>"Image"
            ],

        ];
    
        return response()->json([
            'status' => 200,
            'message' => 'Single user Data',
            'user' => $user,
            'total_ratings' => $totalRatings,
            'average_rating' => number_format($averageRating,2),
            'ratings' => $ratings,
            'banner'=> $banner,
            'random_users' => $randomUsers,
        ]);
    }
    


    public function getSingleUser1(Request $request)
    {

        $userId = $request->input("user_id");
        $user = User::find($userId);
        
        if (!$user) {
            return response()->json(['status' => 404, 'error' => 'User not found']);
        }

        $subcription_id = $user->subscription_id;
    
        $categoryIds = $user->category_id;

        $video_duration = Subscription::where('id', $subcription_id)->value("video_duration");

    
        if ($categoryIds === 0) {
            $randomUsers = User::whereNotIn('subscription_id', [0])->inRandomOrder()->take(5)->get();
        } else {
            $categoryIds = is_array($categoryIds) ? $categoryIds : [$categoryIds]; // Ensure $categoryIds is an array

            $randomUsers = User::where('id', '!=', $userId)
                ->where(function ($query) use ($categoryIds) {
                    $query->where('subscription_id', '!=', 0)
                        ->orWhereHas('category', function ($query) use ($categoryIds) {
                            $query->whereIn('id', $categoryIds);
                        });
                })
                ->inRandomOrder()
                ->take(5)
                ->get();

        
        }
        

    
        $ratings = Rating::where('talent_id', $userId)->get();
    
        $totalRatings = $ratings->count();
        (float)$sumRatings = $ratings->sum('rating');
        
        // Calculate the average rating with two decimal places
        $averageRating = $totalRatings > 0 ? round($sumRatings / $totalRatings, 2) : 0;
    
        // Filter stars ratings and calculate their average
        $fiveStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 5;
        });
        $fourStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 4;
        });
        $threeStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 3;
        });
        $twoStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 2;
        });
        $oneStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 1;
        });

    
        $totalFiveStarRatings = $fiveStarRatings->map(fn($rating) => round($rating->rating))->count();
       
        (float)$averageFiveStarRating = $totalFiveStarRatings > 0 ? round( $totalFiveStarRatings / $totalRatings, 2) : 0;
        // round($fiveStarRatings->sum('rating')
        $totalFourStarRatings = $fourStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageFourStarRating = $totalFourStarRatings > 0 ? round( $totalFourStarRatings / $totalRatings, 2) : 0;
    
        $totalThreeStarRatings = $threeStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageThreeStarRating = $totalThreeStarRatings > 0 ? round($totalThreeStarRatings / $totalRatings, 2) : 0;
    
        $totalTwoStarRatings = $twoStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageTwoStarRating = $totalTwoStarRatings > 0 ? round($totalTwoStarRatings / $totalRatings, 2) : 0;
    
        $totalOneStarRatings = $oneStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageOneStarRating = $totalOneStarRatings > 0 ? round($totalOneStarRatings / $totalRatings, 2) : 0;
    
        $ratings = [
           [ 'total' => $totalFiveStarRatings,
            'average' => number_format($averageFiveStarRating,2),
            'star'=>5
        ],
           [ 'total' => $totalFourStarRatings,
            'average' => number_format($averageFourStarRating,2),
            'star'=>4
        ],
            ['total' => $totalThreeStarRatings,
            'average' => number_format($averageThreeStarRating,2),
            'star'=>3
        ],
            ['total' => $totalTwoStarRatings,
            'average' => number_format($averageTwoStarRating,2),
            'star'=>2
        ],
            ['total' => $totalOneStarRatings,
            'average' => number_format($averageOneStarRating,2),
            'star'=>1
            ]
        ];

        // if ($user->image1 !== null) {
        //     $banner['url'] = $user->image1;
        //     $banner['thumbnail'] = $user->image1;
        // }
        // if ($user->image2 !== null) {
        //     $banner['url'] = $user->image2;
        //     $banner['thumbnail'] = $user->image2;
        // }
        // if ($user->image3 !== null) {
        //     $banner['url'] = $user->image3;
        //     $banner['thumbnail'] = $user->image3;
        // }
        // if ($user->image4 !== null) {
        //     $banner['url'] = $user->image4;
        //     $banner['thumbnail'] = $user->image4;
        // }
        // if ($user->image5 !== null) {
        //     $banner['url'] = $user->image5;
        //     $banner['thumbnail'] = $user->image5;
        // }
        // if ($user->image6 !== null) {
        //     $banner['url'] = $user->image6;
        //     $banner['thumbnail'] = $user->image6;
        // }
        // if ($user->image7 !== null) {
        //     $banner['url'] = $user->image7;
        //     $banner['thumbnail'] = $user->image7;
        // }



       
        $banner = [];
        $thumbnailUrl = 'https://demo.nimayate.com/e4t_new/storage/app/public/avatarsimage/512X512.jpg';
        
        // Video URLs
        $videoUrls = $user->videos->pluck('video_path')->toArray();
        foreach ($videoUrls as $videoUrl) {
            $banner[] = [
                'url' => $videoUrl,
                'thumbnail' => $thumbnailUrl,
                'type' => "video",
                'host' => 'self'
            ];
        }
        if(!empty($user->video1)){
            $banner[] = [
                'url' => $user->video1,
                'thumbnail' => $thumbnailUrl,
                'type' => "video",
                'host' => 'youtube'
            ];
        }
        
        // Image URLs
        $imageUrls = $user->images->pluck('image_path')->toArray();
       
            foreach ($imageUrls as $imageUrl) {
                $banner[] = [
                    'url' => $imageUrl,
                    'thumbnail' => $imageUrl,
                    'type' => "image"
                ];
            }

            // Add individual images to the $banner array
        $individualImages = ['avatar','image1', 'image2', 'image3', 'image4', 'image5', 'image6', 'image7'];
        foreach ($individualImages as $imageProperty) {
            if (!empty($user->$imageProperty)) {
                $banner[] = [
                    'url' => $user->$imageProperty,
                    'thumbnail' => $user->$imageProperty,
                    'type' => "image"
                ];
            }
        }
        
    
        return response()->json([
            'status' => 200,
            'message' => 'Single user Data',
            'user' => $user,
            'video_duration' => $video_duration,
            'total_ratings' => $totalRatings,
            'average_rating' => number_format($averageRating,2),
            'ratings' => $ratings,
            'banner'=> $banner,
            'random_users' => $randomUsers
           
        ]);
    }



    
    public function getSingleUser2(Request $request)
    {

        $userId = $request->input("user_id");
        $user = User::find($userId);
        
        if (!$user) {
            return response()->json(['status' => 404, 'error' => 'User not found']);
        }

        $subcription_id = $user->subscription_id;
    
        $categoryIds = $user->category_id;

        $video_duration = Subscription::where('id', $subcription_id)->value("video_duration");

    
        if ($categoryIds === 0) {
            $randomUsers = User::whereNotIn('subscription_id', [0])->inRandomOrder()->take(5)->get();
        } else {
            $categoryIds = is_array($categoryIds) ? $categoryIds : [$categoryIds]; // Ensure $categoryIds is an array

        $randomUsers = User::where('id', '!=', $userId)
            ->where(function ($query) use ($categoryIds) {
                $query->where(function ($query) {
                    $query->whereNotIn('subscription_id', [0, 24]);
                })->orWhereHas('category', function ($query) use ($categoryIds) {
                    $query->whereIn('id', $categoryIds);
                });
            })
            ->inRandomOrder()
            ->take(5)
            ->get();
            
                }
                
        

    
        $ratings = Rating::where('talent_id', $userId)->get();
    
        $totalRatings = $ratings->count();
        (float)$sumRatings = $ratings->sum('rating');
        
        // Calculate the average rating with two decimal places
        $averageRating = $totalRatings > 0 ? round($sumRatings / $totalRatings, 2) : 0;
        $user->rating = $averageRating;
        // Filter stars ratings and calculate their average
        $fiveStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 5;
        });
        $fourStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 4;
        });
        $threeStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 3;
        });
        $twoStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 2;
        });
        $oneStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 1;
        });

    
        $totalFiveStarRatings = $fiveStarRatings->map(fn($rating) => round($rating->rating))->count();
       
        (float)$averageFiveStarRating = $totalFiveStarRatings > 0 ? round( $totalFiveStarRatings / $totalRatings, 2) : 0;
        // round($fiveStarRatings->sum('rating')
        $totalFourStarRatings = $fourStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageFourStarRating = $totalFourStarRatings > 0 ? round( $totalFourStarRatings / $totalRatings, 2) : 0;
    
        $totalThreeStarRatings = $threeStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageThreeStarRating = $totalThreeStarRatings > 0 ? round($totalThreeStarRatings / $totalRatings, 2) : 0;
    
        $totalTwoStarRatings = $twoStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageTwoStarRating = $totalTwoStarRatings > 0 ? round($totalTwoStarRatings / $totalRatings, 2) : 0;
    
        $totalOneStarRatings = $oneStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageOneStarRating = $totalOneStarRatings > 0 ? round($totalOneStarRatings / $totalRatings, 2) : 0;
    
        $ratings = [
           [ 'total' => $totalFiveStarRatings,
            'average' => number_format($averageFiveStarRating,2),
            'star'=>5
        ],
           [ 'total' => $totalFourStarRatings,
            'average' => number_format($averageFourStarRating,2),
            'star'=>4
        ],
            ['total' => $totalThreeStarRatings,
            'average' => number_format($averageThreeStarRating,2),
            'star'=>3
        ],
            ['total' => $totalTwoStarRatings,
            'average' => number_format($averageTwoStarRating,2),
            'star'=>2
        ],
            ['total' => $totalOneStarRatings,
            'average' => number_format($averageOneStarRating,2),
            'star'=>1
            ]
        ];



       



       
        $banner = [];
        $thumbnailUrl = 'https://demo.nimayate.com/e4t_new/storage/app/public/avatarsimage/512X512.jpg';
        
        // Video URLs
        $videoUrls = $user->videos->pluck('video_path')->toArray();
        foreach ($videoUrls as $videoUrl) {
            $banner[] = [
                'url' => $videoUrl,
                'thumbnail' => $thumbnailUrl,
                'type' => "video"
            ];
        }
        if(!empty($user->video1)){
            $banner[] = [
                'url' => $user->video1,
                'thumbnail' => $thumbnailUrl,
                'type' => "video"
            ];
        }
        
        // Image URLs
        $imageUrls = $user->images->pluck('image_path')->toArray();
       
            foreach ($imageUrls as $imageUrl) {
                $banner[] = [
                    'url' => $imageUrl,
                    'thumbnail' => $imageUrl,
                    'type' => "image"
                ];
            }

            // Add individual images to the $banner array
        $individualImages = ['avatar','image1', 'image2', 'image3', 'image4', 'image5', 'image6', 'image7'];
        foreach ($individualImages as $imageProperty) {
            if (!empty($user->$imageProperty)) {
                $banner[] = [
                    'url' => $user->$imageProperty,
                    'thumbnail' => $user->$imageProperty,
                    'type' => "image"
                ];
            }
        }
        
    
        return response()->json([
            'status' => 200,
            'message' => 'Single user Data',
            'user' => $user,
            'video_duration' => $video_duration,
            'total_ratings' => $totalRatings,
            'average_rating' => number_format($averageRating,2),
            'ratings' => $ratings,
            'banner'=> $banner,
            'random_users' => $randomUsers
           
        ]);
    }


    public function getSingleUser3(Request $request)
    {

        $userId = $request->input("user_id");
        $user = User::find($userId);
        
        if (!$user) {
            return response()->json(['status' => 404, 'error' => 'User not found']);
        }

        $subcription_id = $user->subscription_id;
    
        $categoryIds = $user->category_id;

        $video_duration = Subscription::where('id', $subcription_id)->value("video_duration");

    
        if ($categoryIds === 0) {
            $randomUsers = User::whereNotIn('subscription_id', [0])->inRandomOrder()->take(5)->get();
        } else {
            $categoryIds = is_array($categoryIds) ? $categoryIds : [$categoryIds]; // Ensure $categoryIds is an array
            $randomUsers = User::where('id', '!=', $userId)
            ->where(function ($query) use ($categoryIds) {
                if ($categoryIds === 0) {
                    $query->whereNotIn('subscription_id', [0]);
                } else {
                    $query->whereHas('category', function ($query) use ($categoryIds) {
                        $query->whereIn('id', is_array($categoryIds) ? $categoryIds : [$categoryIds]);
                    });
                }
            })
            ->inRandomOrder()
            ->take(5)
            ->get();
        
        }
        

    
        $ratings = Rating::where('talent_id', $userId)->get();
    
        $totalRatings = $ratings->count();
        (float)$sumRatings = $ratings->sum('rating');
        
        // Calculate the average rating with two decimal places
        $averageRating = $totalRatings > 0 ? round($sumRatings / $totalRatings, 2) : 0;
    
        // Filter stars ratings and calculate their average
        $fiveStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 5;
        });
        $fourStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 4;
        });
        $threeStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 3;
        });
        $twoStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 2;
        });
        $oneStarRatings = $ratings->filter(function ($rating) {
            return ceil($rating->rating) == 1;
        });

    
        $totalFiveStarRatings = $fiveStarRatings->map(fn($rating) => round($rating->rating))->count();
       
        (float)$averageFiveStarRating = $totalFiveStarRatings > 0 ? round( $totalFiveStarRatings / $totalRatings, 2) : 0;
        // round($fiveStarRatings->sum('rating')
        $totalFourStarRatings = $fourStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageFourStarRating = $totalFourStarRatings > 0 ? round( $totalFourStarRatings / $totalRatings, 2) : 0;
    
        $totalThreeStarRatings = $threeStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageThreeStarRating = $totalThreeStarRatings > 0 ? round($totalThreeStarRatings / $totalRatings, 2) : 0;
    
        $totalTwoStarRatings = $twoStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageTwoStarRating = $totalTwoStarRatings > 0 ? round($totalTwoStarRatings / $totalRatings, 2) : 0;
    
        $totalOneStarRatings = $oneStarRatings->map(fn($rating) => round($rating->rating))->count();
        (float)$averageOneStarRating = $totalOneStarRatings > 0 ? round($totalOneStarRatings / $totalRatings, 2) : 0;
    
        $ratings = [
           [ 'total' => $totalFiveStarRatings,
            'average' => number_format($averageFiveStarRating,2),
            'star'=>5
        ],
           [ 'total' => $totalFourStarRatings,
            'average' => number_format($averageFourStarRating,2),
            'star'=>4
        ],
            ['total' => $totalThreeStarRatings,
            'average' => number_format($averageThreeStarRating,2),
            'star'=>3
        ],
            ['total' => $totalTwoStarRatings,
            'average' => number_format($averageTwoStarRating,2),
            'star'=>2
        ],
            ['total' => $totalOneStarRatings,
            'average' => number_format($averageOneStarRating,2),
            'star'=>1
            ]
        ];

        
       
        $banner = [];
        $thumbnailUrl = 'https://demo.nimayate.com/e4t_new/storage/app/public/avatarsimage/512X512.jpg';
        
        // Video URLs
        // $videoUrls = $user->videos->pluck('video_path')->toArray();
        // $videoUrls = Media::where('model_id',$userId)->where('collection_name','subscription-videos')->get();
        // foreach ($videoUrls as $videoUrl) {
        //     $banner[] = [
        //         'url' => $videoUrl,
        //         'thumbnail' => $thumbnailUrl,
        //         'type' => "video"
        //     ];
        // }

        $videoCollections = ['user-video1','user-video2','user-video3','subscription-videos'];

        // $videoUrls = $user->getMedia('subscription-videos');
        // foreach ($videoUrls as $videoUrl) {
        //       $banner[] = [
        //         'url' => $videoUrl->original_url,
        //         'thumbnail' => $videoUrl->getUrl('thumbnail'),
        //         'type' => "video"
        //     ];
        // }
        // Regenerate thumbnails first
        // Regenerate thumbnails manually
        // Regenerate thumbnails manually
        // Regenerate thumbnails manually
        // foreach ($videoCollections as $videoCollection) {
        //     $mediaItems = $user->getMedia($videoCollection);

        //     foreach ($mediaItems as $mediaItem) {
        //         // Define the 'thumbnail' conversion (if not already defined in your model)
        //         $mediaItem->addMediaConversion('thumb')
        //             ->width(200)
        //             ->height(150)
        //             ->format('jpg')
        //             ->fit('crop');

        //         // Generate the 'thumbnail' conversion
        //         $mediaItem->generateConversions();
        //     }
        // }
        //         // Retrieve video URLs and thumbnails after regeneration
        foreach ($videoCollections as $videoCollection) {
            $videoUrls = $user->getMedia($videoCollection);

            foreach ($videoUrls as $videoUrl) {
                $banner[] = [
                    'url' => $videoUrl->original_url,
                    'thumbnail' => $videoUrl->getUrl('thumbnail'),
                    'type' => 'video'
                ];
            }
        }
    
     
        
        
        
        
        
        
        
        // Image URLs
        // $imageUrls = $user->images->pluck('image_path')->toArray();
        // if (empty($banner) && empty($imageUrls)) {
        //     $banner[] = [
        //         'url' => $user->avatar,
        //         'thumbnail' => $user->avatar,
        //         'type' => "image"
        //     ];
        // } else {
        //     foreach ($imageUrls as $imageUrl) {
        //         $banner[] = [
        //             'url' => $imageUrl,
        //             'thumbnail' => $imageUrl,
        //             'type' => "image"
        //         ];
        //     }
        // }

        $collectionNames = ['avatar','user-image1','user-image2','user-image3','user-image4','user-image5','user-image6','user-image7','subscription-images'];
        
        // foreach ($collectionNames as $key => $collectionName) {

        //     $imageUrls = $user->getMedia($collectionName);

        // }

       
        // foreach ($imageUrls as $imageUrl) {
        //     // return $imageUrl->file_name->getUrl('thumb');
        //     // return $imageUrl->full_name->getPath('thumb');
        //     $banner[] = [
        //         'url' => $imageUrl->original_url,
        //         'thumbnail' => $imageUrl->getUrl('thumb'),
        //         'type' => "image"
        //     ];
        // }

        foreach ($collectionNames as $key => $collectionName) {
            $imageUrls = $user->getMedia($collectionName);
        
            foreach ($imageUrls as $imageUrl) {
                $banner[] = [
                    'url' => $imageUrl->original_url,
                    'thumbnail' => $imageUrl->getUrl('thumb'),
                    'type' => "image"
                ];
            }
        }

        
        
        $videolink = User::where('id',$userId)->select('video1')->get();
    
        return response()->json([
            'status' => 200,
            'message' => 'Single user Data',
            'user' => $user,
            'video_duration' => $video_duration,
            'video_link' => $videolink,
            'total_ratings' => $totalRatings,
            'average_rating' => number_format($averageRating,2),
            'ratings' => $ratings,
            'banner'=> $banner,
            'random_users' => $randomUsers
           
        ]);
    }
    
    


public function hireTalent(Request $request)
{
    $userId = Auth::id();
    $talentId = $request->input("talent_id");

    $talent = User::where('id', $talentId)->first();

    // Check if the talent exists
    if (!$talent) {
        return response()->json(['status' => 400, 'message' => 'Talent not found']);
    }

    // Get the talent's mobile number
    $talentMobile = $talent->mobile;

    // Get the user's email
    $userEmail = Auth::user()->email;

    // Compose the email content
    $emailContent = "You have hired the talent with mobile number: " . $talentMobile;

    // Send the email to the user
    Mail::raw($emailContent, function ($message) use ($userEmail) {
        $message->to($userEmail)->subject('Hire Talent Mobile Number');
    });

    return response()->json(['status' => 200, 'message' => 'Email sent successfully']);
}

public function hireTalentBySubscriptionUsers(Request $request)
{
    $user_id = Auth::id();
    $talent_id = $request->input('talent_id');

    $users = User::whereIn('id', [$user_id, $talent_id])->get();

    $user_subscription_id = $users->where('id', $user_id)->pluck('subscription_id')->first();
    $talent_subscription_id = $users->where('id', $talent_id)->pluck('subscription_id')->first();

    $talent = $users->where('id', $talent_id)->first();

    if($user_subscription_id !== 0){

        if($talent->trade_barter == true){

            if ($user_subscription_id >= $talent_subscription_id) {
                $talentMobile = $talent->mobile;
                $talentEmail = $talent->email;
                return response()->json([
                    'status' => 200,
                    'message' => 'Talent Hired',
                    'talent_data' => ['mobile' => $talentMobile, 'email' => $talentEmail]
                ]);
             } else {
    
                // $subscriptions = Subscription::where('id', '>', $user_subscription_id)->orderByDesc('id')
                // ->skip(1) // Skip the last subscription (most recent one)
                // ->take(3) // Take the next three subscriptions
                // ->get();
    
                // $formattedSubscriptions = $subscriptions->map(function ($subscription) {
    
                //     $description = $subscription->description;
                //     $description = str_replace(['[', ']', '\''], '', $description);
                //     $descriptionArray = explode(',', $description);
                //     $descriptionArray = array_map('trim', $descriptionArray);
                
                //     return [
                //         'id' => $subscription->id,
                //         'title' => $subscription->title,
                //         'subscriptionData' => [
                //             'price' => $subscription->price,
                //             'plan' => $subscription->plan,
                //             'validity' => $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months',
                           
                //                 'description' => $descriptionArray,
                //                 'images' => $subscription->images,
                //                 'videos' => $subscription->videos,
                //                 'image_changes' => $subscription->image_changes, 
                //                 'video_changes' => $subscription->video_changes,
                //                 'max_images_size' => $subscription->max_images_size,
                //                 'max_video_size' => $subscription->max_video_size,
                           
                //         ],
                //         'created_at' => $subscription->created_at,
                //         'updated_at' => $subscription->updated_at,
                //     ];
                // });
    
                return response()->json([
                    'status' => 204,
                    'message' => 'Upgrade Subscription',
                    'subcriptions_id' => $user_subscription_id
                ]);
    
    
                }
                }else{
                    return response()->json([
                        'status' => 205,
                        'message' => 'This ' . $talent->name . ' Not Corrently Available'
                    ]);
      }

    }else{

        // $subscriptions = Subscription::where('id', '>', $user_subscription_id)->orderByDesc('id')
        // ->skip(1) // Skip the last subscription (most recent one)
        // ->take(3) // Take the next three subscriptions
        // ->get();

        // $formattedSubscriptions = $subscriptions->map(function ($subscription) {

        //     $description = $subscription->description;
        //     $description = str_replace(['[', ']', '\''], '', $description);
        //     $descriptionArray = explode(',', $description);
        //     $descriptionArray = array_map('trim', $descriptionArray);
        
        //     return [
        //         'id' => $subscription->id,
        //         'title' => $subscription->title,
        //         'subscriptionData' => [
        //             'price' => $subscription->price,
        //             'plan' => $subscription->plan,
        //             'validity' => $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months',
                   
        //                 'description' => $descriptionArray,
        //                 'images' => $subscription->images,
        //                 'videos' => $subscription->videos,
        //                 'image_changes' => $subscription->image_changes, 
        //                 'video_changes' => $subscription->video_changes,
        //                 'max_images_size' => $subscription->max_images_size,
        //                 'max_video_size' => $subscription->max_video_size,
                   
        //         ],
        //         'created_at' => $subscription->created_at,
        //         'updated_at' => $subscription->updated_at,
        //     ];
        // });

        return response()->json([
            'status' => 204,
            'message' => 'Upgrade Subscription',
            'subcriptions_id' => $user_subscription_id
        ]);

    }
}



public function hireTalentBySubscriptionUsers1(Request $request)
{
    $user_id = Auth::id();
    $talent_id = $request->input('talent_id');

    $users = User::whereIn('id', [$user_id, $talent_id])->get();

    $user_subscription_id = $users->where('id', $user_id)->pluck('subscription_id')->first();
    $talent_subscription_id = $users->where('id', $talent_id)->pluck('subscription_id')->first();

    $talent = $users->where('id', $talent_id)->first();

    if($user_subscription_id !== 0){

        if($talent->trade_barter == true){

            if ($user_subscription_id >= $talent_subscription_id) {

                $talentMobile = $talent->mobile;
                $talentEmail = $talent->email;

                if ($talent->flag == 1) {
                    // Both mobile and email are included
                } elseif ($talent->flag == 2) {
                    // Only mobile is included, no need to change anything
                    $talentEmail = null; // Don't include email for flag 2
                } else {
                    // Only email is included
                    $talentMobile = null; // Don't include mobile for other flags
                }

                return response()->json([
                    'status' => 200,
                    'message' => 'Talent Hired',
                    'talent_data' => ['mobile' => $talentMobile, 'email' => $talentEmail]
                ]);


             } else {
    
                // $subscriptions = Subscription::where('id', '>', $user_subscription_id)->orderByDesc('id')
                // ->skip(1) // Skip the last subscription (most recent one)
                // ->take(3) // Take the next three subscriptions
                // ->get();
    
                // $formattedSubscriptions = $subscriptions->map(function ($subscription) {
    
                //     $description = $subscription->description;
                //     $description = str_replace(['[', ']', '\''], '', $description);
                //     $descriptionArray = explode(',', $description);
                //     $descriptionArray = array_map('trim', $descriptionArray);
                
                //     return [
                //         'id' => $subscription->id,
                //         'title' => $subscription->title,
                //         'subscriptionData' => [
                //             'price' => $subscription->price,
                //             'plan' => $subscription->plan,
                //             'validity' => $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months',
                           
                //                 'description' => $descriptionArray,
                //                 'images' => $subscription->images,
                //                 'videos' => $subscription->videos,
                //                 'image_changes' => $subscription->image_changes, 
                //                 'video_changes' => $subscription->video_changes,
                //                 'max_images_size' => $subscription->max_images_size,
                //                 'max_video_size' => $subscription->max_video_size,
                           
                //         ],
                //         'created_at' => $subscription->created_at,
                //         'updated_at' => $subscription->updated_at,
                //     ];
                // });
    
                return response()->json([
                    'status' => 204,
                    'message' => 'Upgrade Subscription',
                    'subcriptions_id' => $user_subscription_id
                ]);
    
    
                }
                }else{
                    return response()->json([
                        'status' => 205,
                        'message' => 'This ' . $talent->name . ' Not Corrently Available'
                    ]);
      }

    }else{

        // $subscriptions = Subscription::where('id', '>', $user_subscription_id)->orderByDesc('id')
        // ->skip(1) // Skip the last subscription (most recent one)
        // ->take(3) // Take the next three subscriptions
        // ->get();

        // $formattedSubscriptions = $subscriptions->map(function ($subscription) {

        //     $description = $subscription->description;
        //     $description = str_replace(['[', ']', '\''], '', $description);
        //     $descriptionArray = explode(',', $description);
        //     $descriptionArray = array_map('trim', $descriptionArray);
        
        //     return [
        //         'id' => $subscription->id,
        //         'title' => $subscription->title,
        //         'subscriptionData' => [
        //             'price' => $subscription->price,
        //             'plan' => $subscription->plan,
        //             'validity' => $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months',
                   
        //                 'description' => $descriptionArray,
        //                 'images' => $subscription->images,
        //                 'videos' => $subscription->videos,
        //                 'image_changes' => $subscription->image_changes, 
        //                 'video_changes' => $subscription->video_changes,
        //                 'max_images_size' => $subscription->max_images_size,
        //                 'max_video_size' => $subscription->max_video_size,
                   
        //         ],
        //         'created_at' => $subscription->created_at,
        //         'updated_at' => $subscription->updated_at,
        //     ];
        // });

        return response()->json([
            'status' => 204,
            'message' => 'Upgrade Subscription',
            'subcriptions_id' => $user_subscription_id
        ]);

    }
}


public function addImage(Request $request)
{
    // Validate the request
    $request->validate([
        'avatar' => 'required|image|mimes:jpeg,png,jpg|max:2048',
    ]);
    
    // Get the authenticated user
    $user = $request->user();
    
    // Delete the previous avatar if it exists
    if ($user->avatar) {
        // Extract the filename from the avatar URL
        $filename = basename($user->avatar);
        // Delete the previous avatar image from S3 storage
        Storage::disk("s3")->delete('users-avatar/' . $filename);
    }
    
    if ($request->hasFile('avatar')) {
        $file = $request->file('avatar');
        $imagePath = "users-avatar/" . time() . ' ' . $file->getClientOriginalName();
    
        Storage::disk("s3")->put($imagePath, file_get_contents($file));
        Storage::disk('s3')->setVisibility($imagePath, 'public');
        $url = Storage::disk("s3")->url($imagePath);
    
        // Update the user's avatar field with the new URL
        $user->avatar = $url;
        $user->save();
    }
    
    // Return a success response
    return response()->json(['status' => 200, 'message' => 'Image added successfully', 'path' => $url]);

}




// public function uploadImages(Request $request)
// {
//     $user = Auth::user()->load('images');
//     $subscriptionPlan = $user->subscription;

//     if (!$subscriptionPlan) {
//         return response()->json(['status' => 400, 'message' => 'No subscription found']);
//     }

//     // Step 2: Get the images_limit from the subscription plan
//     $imagesLimit = $subscriptionPlan->images_limit;

//     // Step 3: Check if the user has already reached the image upload limit
//     $currentImageCount = $user->images->count();
//     if ($currentImageCount >= $imagesLimit) {
//         return response()->json(['status' => 400, 'message' => 'Image upload limit has been reached']);
//     }

//     // Step 4: Upload and save the image
//     $image = $request->file('image');
//     if (!$image || !$image->isValid()) {
//         return response()->json(['status' => 400, 'message' => 'Invalid image file']);
//     }

//     // Save the image to the disk in the 'user_folders' directory, under a subdirectory with the user's name
//     $imagePath = $image->store('avatarsimage/' . $user->name, 'public');
//     $fullImagePath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $imagePath;
    
//     // Save the image path to the user's images relationship
//     $user->images()->create(['image_path' => $fullImagePath]);

//     return response()->json(['status' => 200, 'message' => 'Image uploaded successfully']);
// }


public function uploadImages(Request $request)
{
        $user = Auth::user()->load('images');
    $subscriptionPlan = $user->subscription;

    if (!$subscriptionPlan) {
        return response()->json(['status' => 204, 'message' => 'No subscription found']);
    }

    // Get the images_limit from the subscription plan
    $imagesLimit = $subscriptionPlan->images_limit;

    // Check the user's current image count and calculate the allowed additional images
    $currentImageCount = $user->images->count();
    $allowedAdditionalImages = $imagesLimit - $currentImageCount;

    if ($allowedAdditionalImages <= 0) {
        return response()->json(['status' => 204, 'message' => 'Image upload limit has been reached']);
    }

    // Upload and save the image
    $image = $request->file('image');
    if (!$image || !$image->isValid()) {
        return response()->json(['status' => 400, 'message' => 'Invalid image file']);
    }

    if ($request->hasFile('image')) {
        $file = $request->file('image');
        $imagePath = "users-image/" . time() . ' ' . $file->getClientOriginalName();
        
        Storage::disk("s3")->put($imagePath, file_get_contents($file));
        Storage::disk('s3')->setVisibility($imagePath, 'public');
        $url = Storage::disk("s3")->url($imagePath);
        
        // Save the image path to the user's images relationship
        $user->images()->create(['image_path' => $url]);
    }

    return response()->json(['status' => 200, 'message' => 'Image uploaded successfully']);
}



 
public function updateImage(Request $request, $imageId)
{
    $user = Auth::user();
    $image = $user->images()->find($imageId);

    if (!$image) {
        return response()->json(['status' => 404, 'message' => 'Image not found']);
    }

    // Step 1: Validate the new image file if provided
    $newVideo = $request->file('new_image');
    if ($newVideo && !$newVideo->isValid()) {
        return response()->json(['status' => 400, 'message' => 'Invalid image file']);
    }

    // Step 2: If a new image is provided, upload it and update the image path in the database
    if ($newVideo) {
        $imagePath = $newVideo->store('avatarsimage/' . $user->name, 'public');
        $fullImagePath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $imagePath;

        // Delete the previous image from storage
        $previousImagePath = str_replace('https://demo.nimayate.com/e4t_new/storage/app/public/', '', $image->image_path);
        if (Storage::disk('public')->exists($previousImagePath)) {
            Storage::disk('public')->delete($previousImagePath);
        }

        // Update the image path in the database
        $image->update(['image_path' => $fullImagePath]);
    }

    return response()->json(['status' => 200, 'message' => 'Image updated successfully']);
}


public function deleteImage(Request $request, $imageId)
{
    $user = Auth::user();
    $image = $user->images()->find($imageId);

    if (!$image) {
        return response()->json(['status' => 404, 'message' => 'Image not found']);
    }

    // Step 1: Delete the image file from storage
    $imagePath = str_replace('https://demo.nimayate.com/e4t_new/storage/app/public/', '', $image->image_path);
    if (Storage::disk('public')->exists($imagePath)) {
        Storage::disk('public')->delete($imagePath);
    }

    // Step 2: Delete the image record from the database
    $image->delete();

    return response()->json(['status' => 200, 'message' => 'Image deleted successfully']);
}



// public function uploadVideos(Request $request){

//     $user = Auth::user()->load('videos');
//     $subscriptionPlan = $user->subscription;

//     if (!$subscriptionPlan) {
//         return response()->json(['status' => 204, 'subscription_id' =>$subscriptionPlan->id, 'message' => 'No subscription found']);
//     }

//     // Get the images_limit from the subscription plan
//     $videosLimit = $subscriptionPlan->videos_limit;

//     // Check if the user has already reached the image upload limit
//     $currentVideoCount = $user->videos->count();
//      $allowedAdditionalImages = $videosLimit - $currentVideoCount;

//      if ($allowedAdditionalImages <= 0) {
//         return response()->json(['status' => 204, 'message' => 'Video upload limit has been reached']);
//     }

//     //  Upload and save the image
//     $video = $request->file('video');
//     if (!$video || !$video->isValid()) {
//         return response()->json(['status' => 400, 'message' => 'Invalid video file']);
//     }


//     // $file = $request->file('photo_1');
//             $videoPath = "users-video/" . $user->name . ' ' . $video->getClientOriginalName();

          

//             Storage::disk("s3")->put($videoPath, file_get_contents($video));

//             Storage::disk('s3')->setVisibility($videoPath,'public');
            
//             $url = Storage::disk("s3")->url($videoPath);


//             // $user->image1 = $url;

    
//     // Save the image path to the user's images relationship
//     $user->videos()->create(['video_path' => $url]);

//     return response()->json(['status' => 200, 'message' => 'Video uploaded successfully']);
    
// }

public function uploadVideos(Request $request)
{
    $validator = Validator::make($request->all(), [
        'video' => 'required|mimetypes:video/mp4,video/x-msvideo,video/mpeg|max:102400', // Adjust max file size as needed
    ]);

    if ($validator->fails()) {
        return response()->json(['status' => 400, 'message' => 'Invalid video file', 'errors' => $validator->errors()]);
    }

    $user = Auth::user()->load('videos');
    $subscriptionPlan = $user->subscription;

    if (!$subscriptionPlan) {
        return response()->json(['status' => 204, 'subscription_id' => $subscriptionPlan->id, 'message' => 'No subscription found']);
    }

    $videosLimit = $subscriptionPlan->videos_limit;

    $currentVideoCount = $user->videos->count();
    $allowedAdditionalVideos = $videosLimit - $currentVideoCount;

    if ($allowedAdditionalVideos <= 0) {
        return response()->json(['status' => 204, 'message' => 'Video upload limit has been reached']);
    }

    // $video = $request->file('video');
    // if (!$video || !$video->isValid()) {
    //     return response()->json(['status' => 400, 'message' => 'Invalid video file']);
    // }

    // $videoPath = "users-video/" . $user->name . ' ' . $video->getClientOriginalName();

    // Storage::disk("s3")->put($videoPath, file_get_contents($video));

    // Storage::disk('s3')->setVisibility($videoPath, 'public');

    // $url = Storage::disk("s3")->url($videoPath);

    if ($request->hasFile('video')) {
        $file = $request->file('video');
        $imagePath = "users-video/" . $user->name . '_' . time() . '_' . $file->getClientOriginalName();
        
        try {
            $videoData = file_get_contents($file);
            Storage::disk("s3")->put($imagePath, $videoData, ['Content-Type' => 'video/mp4']);

            Storage::disk('s3')->setVisibility($imagePath, 'public');
            $url = Storage::disk("s3")->url($imagePath);
            $user->videos()->create(['video_path' => $url]);
            
            return response()->json(['status' => 200, 'message' => 'Video uploaded successfully']);
        } catch (Exception $e) {
            return response()->json(['status' => 500, 'message' => 'Failed to upload video', 'error' => $e->getMessage()]);
        }
    }

    return response()->json(['status' => 200, 'message' => 'Video uploaded successfully']);
}


// public function uploadVideos1(Request $request)
// {
//     $user = Auth::user()->load('videos');
//     $subscriptionPlan = $user->subscription;

//     if (!$subscriptionPlan) {
//         return response()->json(['status' => 204, 'subscription_id' => $subscriptionPlan->id, 'message' => 'No subscription found']);
//     }

//     // Get the videos_limit from the subscription plan
//     $videosLimit = $subscriptionPlan->videos_limit;
//     $allowedVideoDuration = $subscriptionPlan->video_duration; // Duration in seconds

//     // Check if the user has already reached the video upload limit
//     $currentVideoCount = $user->videos->count();
//     $allowedAdditionalVideos = $videosLimit - $currentVideoCount;

//     if ($allowedAdditionalVideos <= 0) {
//         return response()->json(['status' => 204, 'message' => 'Video upload limit has been reached']);
//     }

//     // Upload and save the video
//     $video = $request->file('video');
//     if (!$video || !$video->isValid()) {
//         return response()->json(['status' => 400, 'message' => 'Invalid video file']);
//     }

//     // Check the video duration
//     $videoDuration = $this->getVideoDuration($video);
//     if ($videoDuration > $allowedVideoDuration) {
//         return response()->json(['status' => 400, 'message' => 'Video duration exceeds the allowed limit']);
//     }

//     $videoPath = $video->store('bannervideo/' . $user->name, 'public');
//     $fullVideoPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $videoPath;

//     // Save the video path to the user's videos relationship
//     $user->videos()->create(['video_path' => $fullVideoPath]);

//     return response()->json(['status' => 200, 'message' => 'Video uploaded successfully']);
// }



public function updateVideo(Request $request, $videoId)
{
    $user = Auth::user();
    $video = $user->videos()->find($videoId);

    if (!$video) {
        return response()->json(['status' => 404, 'message' => 'Video not found']);
    }

    // Step 1: Validate the new video file if provided
    $newVideo = $request->file('new_video');
    if ($newVideo && !$newVideo->isValid()) {
        return response()->json(['status' => 400, 'message' => 'Invalid video file']);
    }

    // Step 2: If a new video is provided, upload it and update the video path in the database
    if ($newVideo) {
        $videoPath = $newVideo->store('avatarsimage/' . $user->name, 'public');
        $fullVideoPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $videoPath;

        // Delete the previous video from storage
        $previousVideoPath = str_replace('https://demo.nimayate.com/e4t_new/storage/app/public/', '', $video->video_path);
        if (Storage::disk('public')->exists($previousVideoPath)) {
            Storage::disk('public')->delete($previousVideoPath);
        }

        // Update the video path in the database
        $video->update(['video_path' => $fullVideoPath]);
    }

    return response()->json(['status' => 200, 'message' => 'Video updated successfully']);
}


public function deleteVideo(Request $request, $videoId)
{
    $user = Auth::user();
    $video = $user->videos()->find($videoId);

    if (!$video) {
        return response()->json(['status' => 404, 'message' => 'Video not found']);
    }

    // Step 1: Delete the video file from storage
    $videoPath = str_replace('https://demo.nimayate.com/e4t_new/storage/app/public/', '', $video->video_path);
    if (Storage::disk('public')->exists($videoPath)) {
        Storage::disk('public')->delete($videoPath);
    }

    // Step 2: Delete the video record from the database
    $video->delete();

    return response()->json(['status' => 200, 'message' => 'Video deleted successfully']);
}



public function availableBarterUsers(Request $request) {
    // Assuming you have the 'use' statements properly defined at the beginning of your controller
    
    // Retrieve barter users with the specified attribute set to 1 and paginate the results
    // $barterUsers = User::where('trade_barter', 1)
    // ->whereNotNull('barter_description')
    // ->paginate(10);

    $barterUsers = User::where('trade_barter', 1)
    ->whereNotNull('barter_description')
    ->where('barter_description', '!=', '') // Exclude users with empty descriptions
    ->paginate(10);


    
    // Check if any barter users were found
    if ($barterUsers->isEmpty()) {
        return response()->json(['status' => 404, 'message' => 'No barter users found.']);
    }

    // Return the list of barter users as a JSON response
    return response()->json(['status' => 200, 'data' => $barterUsers]);
}




    
    
    
    

    public function store(Request $request)
    {
        $users = new User();
        $users->firebase_id = $request->input('firebase_id');
        $users->name = $request->input('name');
        $users->mobile = $request->input('mobile');
        $users->description = $request->input('description');
        $users->email = $request->input('email');
        $users->mobile_otp = $request->input('mobile_otp');
        $users->email_otp = $request->input('email_otp');
        $users->mobile_verified = $request->input('mobile_verified');
        $users->email_verified = $request->input('email_verified');
    
        // $users->dob = $request->input('dob');
        // Retrieve the date input from the request
        $dateInput = $request->input('dob');
    
        // Convert the date input to 'yyyy-mm-dd' format
        $date = DateTime::createFromFormat('d-m-Y', $dateInput);
        $formattedDate = $date->format('Y-m-d');
    
        $users->dob = $formattedDate;
    
        $talentType = $request->input('talent_type');
    
        // Convert the boolean value to an integer
        $users->talent_type = $talentType ? 1 : 0;
    
        $users->gender = $request->input('gender');
        $users->address = $request->input('address');
        $users->facebook_link = $request->input('facebook_link');
        $users->instagram_link = $request->input('instagram_link');
        $users->linkedIn_link = $request->input('linkedIn_link');
        $users->wikipedia_link = $request->input('wikipedia_link');
        $users->imdb_link = $request->input('imdb_link');
        
        $users->password = $request->input('password');
        $users->remember_token = $request->input('remember_token');
        $users->category_id = (int) $request->input('category_id');

        $category = Category::find($users->category_id);



        $users->category_ids = $request->category_ids;
        
         $categoryIds = explode(',',$users->category_ids);

        $category = Category::whereIn('id',$categoryIds)->get();

        // If a matching category is found, set the category_title attribute on the $users object

        if ($category) {
            $temp ='';
            foreach($category as $i=> $categories){
                if($i == 0){
                    $temp =  $categories->title;
                }else{
                    $temp = $temp . ',' . $categories->title;
                }
              
            }
            $users->category_title = $temp;
        } else {
            $users->category_title = null;
        }



        // // If a matching category is found, set the category_title attribute on the $users object
        // if ($category) {
        //     $users->category_title = $category->title;
        // } else {
        //     $users->category_title = null;
        // }

        $users->subcategory_id = (int) $request->input('subcategory_id');
    
        $talent_barter = $request->input('trade_barter');
        $barter_description = $request->input('barter_description');
    
        // Convert the boolean value to an integer
        $users->trade_barter = $talent_barter ? 1 : 0;
    
        // $users->trade_barter = $request->input('trade_barter');
    
        $users->subscription_id = (int) $request->input('subscription_id');
        $users->subscription_validity = $request->input('subscription_validity');
        $users->subscribed_on = $request->input('subscribed_on');
        $users->booking_fee = $request->input('booking_fee');
    
        // Handle avatar/image upload
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $avatarPath = $avatar->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->avatar = $fullAvatarPath;
        }



        if ($request->hasFile('image1')) {
            $image1 = $request->file('image1');
            $avatarPath = $image1->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image1 = $fullAvatarPath;
        }

        if ($request->hasFile('image2')) {
            $image2 = $request->file('image2');
            $avatarPath = $image2->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image2 = $fullAvatarPath;
        }

        if ($request->hasFile('image3')) {
            $image3 = $request->file('image3');
            $avatarPath = $image3->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image3 = $fullAvatarPath;
        }

        if ($request->hasFile('image4')) {
            $image4 = $request->file('image4');
            $avatarPath = $image4->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image4 = $fullAvatarPath;
        }

        if ($request->hasFile('image5')) {
            $image5 = $request->file('image5');
            $avatarPath = $image5->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image5 = $fullAvatarPath;
        }

        if ($request->hasFile('image6')) {
            $image6 = $request->file('image6');
            $avatarPath = $image6->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image6 = $fullAvatarPath;
        }
        if ($request->hasFile('image7')) {
            $image7 = $request->file('image7');
            $avatarPath = $image7->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image7 = $fullAvatarPath;
        }



        if ($request->hasFile('videos')) {
            $videos = $request->file('videos');
            $videoPath = $videos->store('usersvideo', 'public');
            $fullVideoPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $videoPath;
            $users->videos = $fullVideoPath;
        }
    
        $tags = $request->input('tags');
    
        if (Schema::hasTable('tags')) {
            $existingTag = Tag::where('tags_name', $tags)->first();
            if ($existingTag) {
                $users->tags = $existingTag->tags_name;
            } else {
                $tagModel = new Tag();
                $tagModel->tags_name = $tags;
                $tagModel->save();
                $users->tags = $tagModel->tags_name;
            }
        } else {
            $users->tags = $tags;
        }
    
        $users->save();
    
        return response()->json(['status'=>200, 'users' => $users]);
    }



    public function store2(Request $request)
    {
        $users = new User();
        $users->firebase_id = $request->input('firebase_id');
        $users->name = $request->input('name');
        $users->mobile = $request->input('mobile');
        $users->description = $request->input('description');
        $users->email = $request->input('email');
        $users->mobile_otp = $request->input('mobile_otp');
        $users->email_otp = $request->input('email_otp');
        $users->mobile_verified = $request->input('mobile_verified');
        $users->email_verified = $request->input('email_verified');
    
        // $users->dob = $request->input('dob');
        // Retrieve the date input from the request
        $dateInput = $request->input('dob');
    
        // Convert the date input to 'yyyy-mm-dd' format
        $date = DateTime::createFromFormat('d-m-Y', $dateInput);
        $formattedDate = $date->format('Y-m-d');
    
        $users->dob = $formattedDate;
    
        $talentType = $request->input('talent_type');
    
        // Convert the boolean value to an integer
        $users->talent_type = $talentType ? 1 : 0;
    
        $users->gender = $request->input('gender');
        $users->address = $request->input('address');
        $users->facebook_link = $request->input('facebook_link');
        $users->instagram_link = $request->input('instagram_link');
        $users->linkedIn_link = $request->input('linkedIn_link');
        $users->wikipedia_link = $request->input('wikipedia_link');
        $users->imdb_link = $request->input('imdb_link');
        
        $users->password = $request->input('password');
        $users->remember_token = $request->input('remember_token');
        $users->category_id = (int) $request->input('category_id');

        $category = Category::find($users->category_id);



        $users->category_ids = $request->category_ids;
        
         $categoryIds = explode(',',$users->category_ids);

        $category = Category::whereIn('id',$categoryIds)->get();

        // If a matching category is found, set the category_title attribute on the $users object

        if ($category) {
            $temp ='';
            foreach($category as $i=> $categories){
                if($i == 0){
                    $temp =  $categories->title;
                }else{
                    $temp = $temp . ',' . $categories->title;
                }
              
            }
            $users->category_title = $temp;
        } else {
            $users->category_title = null;
        }



        // // If a matching category is found, set the category_title attribute on the $users object
        // if ($category) {
        //     $users->category_title = $category->title;
        // } else {
        //     $users->category_title = null;
        // }

        $users->subcategory_id = (int) $request->input('subcategory_id');
    
        $talent_barter = $request->input('trade_barter');
        $barter_description = $request->input('barter_description');
    
        // Convert the boolean value to an integer
        $users->trade_barter = $talent_barter ? 1 : 0;
    
        // $users->trade_barter = $request->input('trade_barter');
    
        $users->subscription_id = (int) $request->input('subscription_id');
        $users->subscription_validity = $request->input('subscription_validity');
        $users->subscribed_on = $request->input('subscribed_on');
        $users->booking_fee = $request->input('booking_fee');
    
        // Handle avatar/image upload
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $avatarPath = $avatar->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->avatar = $fullAvatarPath;
        }



        if ($request->hasFile('image1')) {
            $image1 = $request->file('image1');
            $avatarPath = $image1->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image1 = $fullAvatarPath;
        }

        if ($request->hasFile('image2')) {
            $image2 = $request->file('image2');
            $avatarPath = $image2->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image2 = $fullAvatarPath;
        }

        if ($request->hasFile('image3')) {
            $image3 = $request->file('image3');
            $avatarPath = $image3->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image3 = $fullAvatarPath;
        }

        if ($request->hasFile('image4')) {
            $image4 = $request->file('image4');
            $avatarPath = $image4->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image4 = $fullAvatarPath;
        }

        if ($request->hasFile('image5')) {
            $image5 = $request->file('image5');
            $avatarPath = $image5->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image5 = $fullAvatarPath;
        }

        if ($request->hasFile('image6')) {
            $image6 = $request->file('image6');
            $avatarPath = $image6->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image6 = $fullAvatarPath;
        }
        if ($request->hasFile('image7')) {
            $image7 = $request->file('image7');
            $avatarPath = $image7->store('avatarsimage', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->image7 = $fullAvatarPath;
        }



        if ($request->hasFile('videos')) {
            $videos = $request->file('videos');
            $videoPath = $videos->store('usersvideo', 'public');
            $fullVideoPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $videoPath;
            $users->videos = $fullVideoPath;
        }
    
        $tags = $request->input('tags');
    
        if (Schema::hasTable('tags')) {
            $existingTag = Tag::where('tags_name', $tags)->first();
            if ($existingTag) {
                $users->tags = $existingTag->tags_name;
            } else {
                $tagModel = new Tag();
                $tagModel->tags_name = $tags;
                $tagModel->save();
                $users->tags = $tagModel->tags_name;
            }
        } else {
            $users->tags = $tags;
        }
    
        $users->save();
    
        return response()->json(['status'=>200, 'users' => $users]);
    }

 

public function update(Request $request, User $users)
{
    // $users->firebase_id = $request->input('firebase_id');
    $firebase_id = $request->input('firebase_id');
    if ($firebase_id !== null) {
       $users->firebase_id = $firebase_id;
    }
    // $users->name = $request->input('name');
    $name = $request->input('name');
    if ($name !== null) {
       $users->name = $name;
    }

    $description = $request->input('description');
    if ($description !== null) {
       $users->description = $description;
    }

    $email = $request->input('email');
     if ($email !== null) {
        $users->email = $email;
    }
    // $users->email = $request->input('email');
    // $users->mobile_otp = $request->input('mobile_otp');
    // $mobile_otp = $request->input('mobile_otp');
    // if ($mobile_otp !== null) {
    //    $users->mobile_otp = $mobile_otp;
    // }
    // $users->email_otp = $request->input('email_otp');
    $email_otp = $request->input('email_otp');
    if ($email_otp !== null) {
       $users->email_otp = $email_otp;
    }
    // $users->mobile_verified = $request->input('mobile_verified');
    $mobile_verified = $request->input('mobile_verified');
    if ($mobile_verified !== null) {
       $users->mobile_verified = $mobile_verified;
    }
    // $users->email_verified = $request->input('email_verified');
    $email_verified = $request->input('email_verified');
    if ($email_verified !== null) {
       $users->email_verified = $email_verified;
    }

    $dateInput = $request->input('dob');
    if ($dateInput !== null) {
        $users->dob = $dateInput;
     
    if ($dateInput) {
        // Convert the date input to 'yyyy-mm-dd' format
        $date = DateTime::createFromFormat('d-m-Y', $dateInput);
        if ($date !== false) {
            $formattedDate = $date->format('Y-m-d');
            $users->dob = $formattedDate;
        } else {
            // Handle invalid date input
            return response()->json(['status'=>400, 'error' => 'Invalid date format']);
            }
        } 
    }




    
    // $talentType = $request->input('talent_type');
    $talent_type = $request->input('talent_type');
    if ($talent_type !== null) {
        $users->talent_type = $talent_type ? 1 : 0;
    }
    // Convert the boolean value to an integer
   

    // $users->gender = $request->input('gender');
    $gender = $request->input('gender');
    if ($gender !== null) {
       $users->gender = $gender;
    }
    // $users->address = $request->input('address');
    $address = $request->input('address');
    if ($address !== null) {
       $users->address = $address;
    }

    $country = $request->input('country');
    if ($country !== null) {
       $users->country = $country;
    }

    $state = $request->input('state');
    if ($state !== null) {
       $users->state = $state;
    }

    $city = $request->input('city');
    if ($city !== null) {
       $users->city = $city;
    }
    
    // $users->password = $request->input('password');
    $password = $request->input('password');
    if ($password !== null) {
       $users->password = $password;
    }


    $facebook_link = $request->input('facebook_link');
    if ($facebook_link !== null) {
       $users->facebook_link = $facebook_link;
    }

    $instagram_link = $request->input('instagram_link');
    if ($instagram_link !== null) {
       $users->instagram_link = $instagram_link;
    }


    $linkedIn_link = $request->input('linkedIn_link');
    if ($linkedIn_link !== null) {
       $users->linkedIn_link = $linkedIn_link;
    }


    $wikipedia_link = $request->input('wikipedia_link');
    if ($wikipedia_link !== null) {
       $users->wikipedia_link = $wikipedia_link;
    }


    $imdb_link = $request->input('imdb_link');
    if ($imdb_link !== null) {
       $users->imdb_link = $imdb_link;
    }


    // $users->remember_token = $request->input('remember_token');
    $remember_token = $request->input('remember_token');
    if ($remember_token !== null) {
       $users->remember_token = $remember_token;
    }
    // $users->category_id = $request->input('category_id');
    $category_id = $request->input('category_id');
    if ($category_id !== null) {
        $users->category_id = (int) $category_id;
    }

    $category = Category::find($category_id);

        // If a matching category is found, set the category_title attribute on the $users object
        if ($category) {
            $users->category_title = $category->title;
        } else {
            $users->category_title = null;
        }

        
    // $users->subcategory_id = $request->input('subcategory_id');
    $subcategory_id = $request->input('subcategory_id');
    if ($subcategory_id !== null) {
       $users->subcategory_id =(int) $subcategory_id;
    }

    // $users->trade_barter = $request->input('trade_barter');
    // $talent_barter = $request->input('trade_barter');
    $trade_barter = $request->input('trade_barter');
    if ($trade_barter !== null) {
        $users->trade_barter = $trade_barter ? 1 : 0;
    }
    // Convert the boolean value to an integer
   

    // $users->subscription_id = $request->input('subscription_id');
    $subscription_id = $request->input('subscription_id');
    if ($subscription_id !== null) {
       $users->subscription_id =(int) $subscription_id;
    }
    // $users->subscription_validity = $request->input('subscription_validity');
    $subscription_validity = $request->input('subscription_validity');
    if ($subscription_validity !== null) {
       $users->subscription_validity = $subscription_validity;
    }
    // $users->subscribed_on = $request->input('subscribed_on');
    $subscribed_on = $request->input('subscribed_on');
    if ($subscribed_on !== null) {
       $users->subscribed_on = $subscribed_on;
    }
    // $users->booking_fee = $request->input('booking_fee');
    $booking_fee = $request->input('booking_fee');
    if ($booking_fee !== null) {
       $users->booking_fee = $booking_fee;
    }

     // Handle avatar/image upload
     if ($request->hasFile('avatar')) {
        // Delete the previous avatar if it exists
        if ($users->avatar) {
            Storage::disk('public')->delete($users->avatar);
        }

        $avatar = $request->file('avatar');
        $avatarPath = $avatar->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->avatar = $fullAvatarPath;
    }

    if ($request->hasFile('image1')) {
         // Delete the previous avatar if it exists
         if ($users->image1) {
            Storage::disk('public')->delete($users->image1);
        }
        $image1 = $request->file('image1');
        $avatarPath = $image1->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image1 = $fullAvatarPath;
    }

    if ($request->hasFile('image2')) {
         // Delete the previous avatar if it exists
         if ($users->image2) {
            Storage::disk('public')->delete($users->image2);
        }

        $image2 = $request->file('image2');
        $avatarPath = $image2->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image2 = $fullAvatarPath;
    }

    if ($request->hasFile('image3')) {
         // Delete the previous avatar if it exists
         if ($users->image3) {
            Storage::disk('public')->delete($users->image3);
        }

        $image3 = $request->file('image3');
        $avatarPath = $image3->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image3 = $fullAvatarPath;
    }

    if ($request->hasFile('image4')) {
         // Delete the previous avatar if it exists
         if ($users->image4) {
            Storage::disk('public')->delete($users->image4);
        }

        $image4 = $request->file('image4');
        $avatarPath = $image4->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image4 = $fullAvatarPath;
    }

    if ($request->hasFile('image5')) {
        // Delete the previous avatar if it exists
        if ($users->image5) {
            Storage::disk('public')->delete($users->image5);
        }

        $image5 = $request->file('image5');
        $avatarPath = $image5->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image5 = $fullAvatarPath;
    }

    if ($request->hasFile('image6')) {
        // Delete the previous avatar if it exists
        if ($users->image6) {
            Storage::disk('public')->delete($users->image6);
        }

        $image6 = $request->file('image6');
        $avatarPath = $image6->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image6 = $fullAvatarPath;
    }
    if ($request->hasFile('image7')) {
        // Delete the previous avatar if it exists
        if ($users->image7) {
            Storage::disk('public')->delete($users->image7);
        }
        $image7 = $request->file('image7');
        $avatarPath = $image7->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image7 = $fullAvatarPath;
    }


         // Handle avatar/image upload
         if ($request->hasFile('videos')) {
            // Delete the previous avatar if it exists
            if ($users->videos) {
                Storage::disk('public')->delete($users->videos);
            }
    
            $avatar = $request->file('videos');
            $avatarPath = $avatar->store('usersvideo', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->videos = $fullAvatarPath;
        }




    $users->save();

    return response()->json(['status'=>200,'user' => $users]);
}



public function update1(Request $request, User $users)
{
    // $users->firebase_id = $request->input('firebase_id');
    $firebase_id = $request->input('firebase_id');
    if ($firebase_id !== null) {
       $users->firebase_id = $firebase_id;
    }
    // $users->name = $request->input('name');
    $name = $request->input('name');
    if ($name !== null) {
       $users->name = $name;
    }

    $description = $request->input('description');
    if ($description !== null) {
       $users->description = $description;
    }

    $email = $request->input('email');
     if ($email !== null) {
        $users->email = $email;
    }
    // $users->email = $request->input('email');
    // $users->mobile_otp = $request->input('mobile_otp');
    // $mobile_otp = $request->input('mobile_otp');
    // if ($mobile_otp !== null) {
    //    $users->mobile_otp = $mobile_otp;
    // }
    // $users->email_otp = $request->input('email_otp');
    $email_otp = $request->input('email_otp');
    if ($email_otp !== null) {
       $users->email_otp = $email_otp;
    }
    // $users->mobile_verified = $request->input('mobile_verified');
    $mobile_verified = $request->input('mobile_verified');
    if ($mobile_verified !== null) {
       $users->mobile_verified = $mobile_verified;
    }
    // $users->email_verified = $request->input('email_verified');
    $email_verified = $request->input('email_verified');
    if ($email_verified !== null) {
       $users->email_verified = $email_verified;
    }

    $dateInput = $request->input('dob');
    if ($dateInput !== null) {
        $users->dob = $dateInput;
     
    if ($dateInput) {
        // Convert the date input to 'yyyy-mm-dd' format
        $date = DateTime::createFromFormat('d-m-Y', $dateInput);
        if ($date !== false) {
            $formattedDate = $date->format('Y-m-d');
            $users->dob = $formattedDate;
        } else {
            // Handle invalid date input
            return response()->json(['status'=>400, 'error' => 'Invalid date format']);
            }
        } 
    }




    
    // $talentType = $request->input('talent_type');
    $talent_type = $request->input('talent_type');
    if ($talent_type !== null) {
        $users->talent_type = $talent_type ? 1 : 0;
    }
    // Convert the boolean value to an integer
   

    // $users->gender = $request->input('gender');
    $gender = $request->input('gender');
    if ($gender !== null) {
       $users->gender = $gender;
    }
    // $users->address = $request->input('address');
    $address = $request->input('address');
    if ($address !== null) {
       $users->address = $address;
    }

    $country = $request->input('country');
    if ($country !== null) {
       $users->country = $country;
    }

    $state = $request->input('state');
    if ($state !== null) {
       $users->state = $state;
    }

    $city = $request->input('city');
    if ($city !== null) {
       $users->city = $city;
    }
    
    // $users->password = $request->input('password');
    $password = $request->input('password');
    if ($password !== null) {
       $users->password = $password;
    }


    $facebook_link = $request->input('facebook_link');
    if ($facebook_link !== null) {
       $users->facebook_link = $facebook_link;
    }

    $instagram_link = $request->input('instagram_link');
    if ($instagram_link !== null) {
       $users->instagram_link = $instagram_link;
    }


    $linkedIn_link = $request->input('linkedIn_link');
    if ($linkedIn_link !== null) {
       $users->linkedIn_link = $linkedIn_link;
    }


    $wikipedia_link = $request->input('wikipedia_link');
    if ($wikipedia_link !== null) {
       $users->wikipedia_link = $wikipedia_link;
    }


    $imdb_link = $request->input('imdb_link');
    if ($imdb_link !== null) {
       $users->imdb_link = $imdb_link;
    }


    // $users->remember_token = $request->input('remember_token');
    $remember_token = $request->input('remember_token');
    if ($remember_token !== null) {
       $users->remember_token = $remember_token;
    }
    // $users->category_id = $request->input('category_id');
    $category_id = $request->input('category_id');
    if ($category_id !== null) {
        $users->category_id = (int) $category_id;
    }

    $category = Category::find($category_id);

        // // If a matching category is found, set the category_title attribute on the $users object
        // if ($category) {
        //     $users->category_title = $category->title;
        // } else {
        //     $users->category_title = null;
        // }

        $users->category_ids = $request->category_ids;
        if ($users->category_ids !== null) {

            $categoryIds = explode(',',$users->category_ids);

            $category = Category::whereIn('id',$categoryIds)->get();
    
            // If a matching category is found, set the category_title attribute on the $users object
    
            if ($category) {
                $temp ='';
                foreach($category as $i=> $categories){
                    if($i == 0){
                        $temp =  $categories->title;
                    }else{
                        $temp = $temp . ',' . $categories->title;
                    }
                  
                }
                $users->category_title = $temp;
            } else {
                $users->category_title = null;
            }

        }


        
        
         



        
    // $users->subcategory_id = $request->input('subcategory_id');
    $subcategory_id = $request->input('subcategory_id');
    if ($subcategory_id !== null) {
       $users->subcategory_id =(int) $subcategory_id;
    }

    // $users->trade_barter = $request->input('trade_barter');
    // $talent_barter = $request->input('trade_barter');
    $trade_barter = $request->input('trade_barter');
    if ($trade_barter !== null) {
        $users->trade_barter = $trade_barter ? 1 : 0;
    }
    // Convert the boolean value to an integer
   

    // $users->subscription_id = $request->input('subscription_id');
    $subscription_id = $request->input('subscription_id');
    if ($subscription_id !== null) {
       $users->subscription_id =(int) $subscription_id;
    }
    // $users->subscription_validity = $request->input('subscription_validity');
    $subscription_validity = $request->input('subscription_validity');
    if ($subscription_validity !== null) {
       $users->subscription_validity = $subscription_validity;
    }
    // $users->subscribed_on = $request->input('subscribed_on');
    $subscribed_on = $request->input('subscribed_on');
    if ($subscribed_on !== null) {
       $users->subscribed_on = $subscribed_on;
    }
    // $users->booking_fee = $request->input('booking_fee');
    $booking_fee = $request->input('booking_fee');
    if ($booking_fee !== null) {
       $users->booking_fee = $booking_fee;
    }

     // Handle avatar/image upload
     if ($request->hasFile('avatar')) {
        // Delete the previous avatar if it exists
        if ($users->avatar) {
            Storage::disk('public')->delete($users->avatar);
        }

        $avatar = $request->file('avatar');
        $avatarPath = $avatar->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->avatar = $fullAvatarPath;
    }

    if ($request->hasFile('image1')) {
         // Delete the previous avatar if it exists
         if ($users->image1) {
            Storage::disk('public')->delete($users->image1);
        }
        $image1 = $request->file('image1');
        $avatarPath = $image1->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image1 = $fullAvatarPath;
    }

    if ($request->hasFile('image2')) {
         // Delete the previous avatar if it exists
         if ($users->image2) {
            Storage::disk('public')->delete($users->image2);
        }

        $image2 = $request->file('image2');
        $avatarPath = $image2->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image2 = $fullAvatarPath;
    }

    if ($request->hasFile('image3')) {
         // Delete the previous avatar if it exists
         if ($users->image3) {
            Storage::disk('public')->delete($users->image3);
        }

        $image3 = $request->file('image3');
        $avatarPath = $image3->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image3 = $fullAvatarPath;
    }

    if ($request->hasFile('image4')) {
         // Delete the previous avatar if it exists
         if ($users->image4) {
            Storage::disk('public')->delete($users->image4);
        }

        $image4 = $request->file('image4');
        $avatarPath = $image4->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image4 = $fullAvatarPath;
    }

    if ($request->hasFile('image5')) {
        // Delete the previous avatar if it exists
        if ($users->image5) {
            Storage::disk('public')->delete($users->image5);
        }

        $image5 = $request->file('image5');
        $avatarPath = $image5->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image5 = $fullAvatarPath;
    }

    if ($request->hasFile('image6')) {
        // Delete the previous avatar if it exists
        if ($users->image6) {
            Storage::disk('public')->delete($users->image6);
        }

        $image6 = $request->file('image6');
        $avatarPath = $image6->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image6 = $fullAvatarPath;
    }
    if ($request->hasFile('image7')) {
        // Delete the previous avatar if it exists
        if ($users->image7) {
            Storage::disk('public')->delete($users->image7);
        }
        $image7 = $request->file('image7');
        $avatarPath = $image7->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image7 = $fullAvatarPath;
    }


         // Handle avatar/image upload
         if ($request->hasFile('videos')) {
            // Delete the previous avatar if it exists
            if ($users->videos) {
                Storage::disk('public')->delete($users->videos);
            }
    
            $avatar = $request->file('videos');
            $avatarPath = $avatar->store('usersvideo', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->videos = $fullAvatarPath;
        }




    $users->save();

    return response()->json(['status'=>200,'user' => $users]);
}
    


public function update2(Request $request, User $users)
{
    // $users->firebase_id = $request->input('firebase_id');
    $firebase_id = $request->input('firebase_id');
    if ($firebase_id !== null) {
       $users->firebase_id = $firebase_id;
    }
    // $users->name = $request->input('name');
    $name = $request->input('name');
    if ($name !== null) {
       $users->name = $name;
    }

    $description = $request->input('description');
    if ($description !== null) {
       $users->description = $description;
    }

    $email = $request->input('email');
     if ($email !== null) {
        $users->email = $email;
    }
    // $users->email = $request->input('email');
    // $users->mobile_otp = $request->input('mobile_otp');
    // $mobile_otp = $request->input('mobile_otp');
    // if ($mobile_otp !== null) {
    //    $users->mobile_otp = $mobile_otp;
    // }
    // $users->email_otp = $request->input('email_otp');
    $email_otp = $request->input('email_otp');
    if ($email_otp !== null) {
       $users->email_otp = $email_otp;
    }
    // $users->mobile_verified = $request->input('mobile_verified');
    $mobile_verified = $request->input('mobile_verified');
    if ($mobile_verified !== null) {
       $users->mobile_verified = $mobile_verified;
    }
    // $users->email_verified = $request->input('email_verified');
    $email_verified = $request->input('email_verified');
    if ($email_verified !== null) {
       $users->email_verified = $email_verified;
    }

    $dateInput = $request->input('dob');
    if ($dateInput !== null) {
        $users->dob = $dateInput;
     
    if ($dateInput) {
        // Convert the date input to 'yyyy-mm-dd' format
        // $date = DateTime::createFromFormat('Y-m-d', $dateInput);
        // if ($date !== false) {
        //     $formattedDate = $date->format('Y-m-d');
        //     $users->dob = $formattedDate;
        // } else {
        //     // Handle invalid date input
        //     return response()->json(['status'=>400, 'error' => 'Invalid date format']);
        //     }
        $users->dob = $dateInput;
        } 

    }




    
    // $talentType = $request->input('talent_type');
    $talent_type = $request->input('talent_type');
    if ($talent_type !== null) {
        $users->talent_type = $talent_type ? 1 : 0;
    }
    // Convert the boolean value to an integer
   

    // $users->gender = $request->input('gender');
    $gender = $request->input('gender');
    if ($gender !== null) {
       $users->gender = $gender;
    }
    // $users->address = $request->input('address');
    $address = $request->input('address');
    if ($address !== null) {
       $users->address = $address;
    }

    $country = $request->input('country');
    if ($country !== null) {
       $users->country = $country;
    }

    $state = $request->input('state');
    if ($state !== null) {
       $users->state = $state;
    }

    $city = $request->input('city');
    if ($city !== null) {
       $users->city = $city;
    }
    
    // $users->password = $request->input('password');
    $password = $request->input('password');
    if ($password !== null) {
       $users->password = $password;
    }


    $facebook_link = $request->input('facebook_link');
    if ($facebook_link !== null) {
       $users->facebook_link = $facebook_link;
    }

    $instagram_link = $request->input('instagram_link');
    if ($instagram_link !== null) {
       $users->instagram_link = $instagram_link;
    }


    $linkedIn_link = $request->input('linkedIn_link');
    if ($linkedIn_link !== null) {
       $users->linkedIn_link = $linkedIn_link;
    }


    $wikipedia_link = $request->input('wikipedia_link');
    if ($wikipedia_link !== null) {
       $users->wikipedia_link = $wikipedia_link;
    }


    $imdb_link = $request->input('imdb_link');
    if ($imdb_link !== null) {
       $users->imdb_link = $imdb_link;
    }


    // $users->remember_token = $request->input('remember_token');
    $remember_token = $request->input('remember_token');
    if ($remember_token !== null) {
       $users->remember_token = $remember_token;
    }
    // $users->category_id = $request->input('category_id');
    $category_id = $request->input('category_id');
    if ($category_id !== null) {
        $users->category_id = (int) $category_id;
    }

    $category = Category::find($category_id);

        // // If a matching category is found, set the category_title attribute on the $users object
        // if ($category) {
        //     $users->category_title = $category->title;
        // } else {
        //     $users->category_title = null;
        // }

        $users->category_ids = $request->category_ids;
        if ($users->category_ids !== null) {

            $categoryIds = explode(',',$users->category_ids);

            $category = Category::whereIn('id',$categoryIds)->get();
    
            // If a matching category is found, set the category_title attribute on the $users object
    
            if ($category) {
                $temp ='';
                foreach($category as $i=> $categories){
                    if($i == 0){
                        $temp =  $categories->title;
                    }else{
                        $temp = $temp . ',' . $categories->title;
                    }
                  
                }
                $users->category_title = $temp;
            } else {
                $users->category_title = null;
            }

        }


        
    // $users->subcategory_id = $request->input('subcategory_id');
    $subcategory_id = $request->input('subcategory_id');
    if ($subcategory_id !== null) {
       $users->subcategory_id =(int) $subcategory_id;
    }

    // $users->trade_barter = $request->input('trade_barter');
    // $talent_barter = $request->input('trade_barter');
    $trade_barter = $request->input('trade_barter');
    if ($trade_barter !== null) {
        $users->trade_barter = $trade_barter ? 1 : 0;
    }
    // Convert the boolean value to an integer
    $barter_description = $request->input('barter_description');
    if ($barter_description !== null) {
        $users->barter_description = $barter_description ;
    }

    // $users->subscription_id = $request->input('subscription_id');
    $subscription_id = $request->input('subscription_id');
    if ($subscription_id !== null) {
       $users->subscription_id =(int) $subscription_id;
    }
    // $users->subscription_validity = $request->input('subscription_validity');
    $subscription_validity = $request->input('subscription_validity');
    if ($subscription_validity !== null) {
       $users->subscription_validity = $subscription_validity;
    }
    // $users->subscribed_on = $request->input('subscribed_on');
    $subscribed_on = $request->input('subscribed_on');
    if ($subscribed_on !== null) {
       $users->subscribed_on = $subscribed_on;
    }
    // $users->booking_fee = $request->input('booking_fee');
    $booking_fee = $request->input('booking_fee');
    if ($booking_fee !== null) {
       $users->booking_fee = $booking_fee;
    }

    $flag = $request->input('flag');

    if ($flag !== null) {
        if ($flag === '1') {
            $users->flag = 1;
        } elseif ($flag === '2') {
            $users->flag = 2;
        } elseif ($flag === '3') {
            $users->flag = 3;
        }
    }

     // Handle avatar/image upload
     if ($request->hasFile('avatar')) {
        // Delete the previous avatar if it exists
        if ($users->avatar) {
            Storage::disk('public')->delete($users->avatar);
        }

        $avatar = $request->file('avatar');
        $avatarPath = $avatar->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->avatar = $fullAvatarPath;
    }

    if ($request->hasFile('image1')) {
         // Delete the previous avatar if it exists
         if ($users->image1) {
            Storage::disk('public')->delete($users->image1);
        }
        $image1 = $request->file('image1');
        $avatarPath = $image1->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image1 = $fullAvatarPath;
    }

    if ($request->hasFile('image2')) {
         // Delete the previous avatar if it exists
         if ($users->image2) {
            Storage::disk('public')->delete($users->image2);
        }

        $image2 = $request->file('image2');
        $avatarPath = $image2->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image2 = $fullAvatarPath;
    }

    if ($request->hasFile('image3')) {
         // Delete the previous avatar if it exists
         if ($users->image3) {
            Storage::disk('public')->delete($users->image3);
        }

        $image3 = $request->file('image3');
        $avatarPath = $image3->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image3 = $fullAvatarPath;
    }

    if ($request->hasFile('image4')) {
         // Delete the previous avatar if it exists
         if ($users->image4) {
            Storage::disk('public')->delete($users->image4);
        }

        $image4 = $request->file('image4');
        $avatarPath = $image4->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image4 = $fullAvatarPath;
    }

    if ($request->hasFile('image5')) {
        // Delete the previous avatar if it exists
        if ($users->image5) {
            Storage::disk('public')->delete($users->image5);
        }

        $image5 = $request->file('image5');
        $avatarPath = $image5->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image5 = $fullAvatarPath;
    }

    if ($request->hasFile('image6')) {
        // Delete the previous avatar if it exists
        if ($users->image6) {
            Storage::disk('public')->delete($users->image6);
        }

        $image6 = $request->file('image6');
        $avatarPath = $image6->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image6 = $fullAvatarPath;
    }
    if ($request->hasFile('image7')) {
        // Delete the previous avatar if it exists
        if ($users->image7) {
            Storage::disk('public')->delete($users->image7);
        }
        $image7 = $request->file('image7');
        $avatarPath = $image7->store('avatarsimage', 'public');
        $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
        $users->image7 = $fullAvatarPath;
    }


         // Handle avatar/image upload
         if ($request->hasFile('videos')) {
            // Delete the previous avatar if it exists
            if ($users->videos) {
                Storage::disk('public')->delete($users->videos);
            }
    
            $avatar = $request->file('videos');
            $avatarPath = $avatar->store('usersvideo', 'public');
            $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
            $users->videos = $fullAvatarPath;
        }




    $users->save();

    return response()->json(['status'=>200,'user' => $users]);
}

    // public function destroy(User $users)
    // {

    //     if ($users->avatar) {
    //         Storage::disk('public')->delete($users->avatar);
    //     }

    //     $users->delete();

    //     return response()->json(['status'=>200, 'message' => 'User deleted successfully']);
    // }

    public function destroy(Request $request)
{
    // $userId = Auth::id();
    // $user = User::find($userId);

    // // if ($user->avatar) {
    // //     Storage::disk('public')->delete($user->avatar);
    // // }

    // if ($user->avatar) {
    //     // Extract the filename from the avatar URL
    //     $filename = basename($user->avatar);
    //     // Delete the previous avatar image from S3 storage
    //     Storage::disk("s3")->delete('users-avatar/' . $filename);
    // }

    // $user->delete();

    return response()->json(['status' => 200, 'message' => 'User deleted successfully']);
}



public function payment(Request $request){

    $order_id = $request->order_id;
    $tracking_id = $request->tracking_id;
    $price = $request->price;
    $subscription_id = $request->subscription_id;
    $user_id = $request->user_id;


    return view('CCavenue.index', [
        'order_id' => $order_id,
        'tracking_id' => $tracking_id,
        'price' => $price,
        'subscription_id' => $subscription_id,
        'user_id' => $user_id,
    ]);
    
}

public function fcm_Token_update(Request $request)
{
    try {
        $userId = Auth::id();
        $token = $request->token;

        $existingToken = FcmToken::where('user_id', $userId)
            ->where('fcm_token', $token)
            ->first();

        if (!$existingToken) {
            FcmToken::create([
                'user_id' => $userId,
                'fcm_token' => $token,
            ]);

            return response()->json(['status' => 200, 'message' => 'Token created!']);
        } else {
            return response()->json(['status' => 200, 'message' => 'Token already exists.']);
        }
    } catch (\Exception $e) {
        report($e);
        return response()->json([
            'status' => 400,
            'success' => false,
        ]);
    }
}

public function logout(Request $request)
{
    $fmcToken = FcmToken::where('fcm_token',$request->fcm_token)->first();

    if($fmcToken){
        $fmcToken->delete();
    }
    
    // $authToken =  $request->token;

    // Auth::guard('sanctum')->user()->tokens()->delete();
 
    return response()->json(['status'=>200,'message'=>'User LogOut Successfully']);
}


public function logout1(Request $request)
{
    $fmcToken = FcmToken::where('fcm_token',$request->fcm_token)->first();

    if($fmcToken){
        $fmcToken->delete();
    }
    
    $authToken = $request->token;
    $user = Auth::guard('sanctum')->user();

    // Retrieve the token by its name
   return $token = $user->tokens();

if ($token) {
    $token->delete();
    return response()->json(['status' => 200, 'message' => 'Token deleted successfully']);
} else {
    return response()->json(['status' => 404, 'message' => 'Token not found']);
}
}
// public function logout(Request $request){

//     $userId = Auth::id();
//     $fmcToken = FcmToken::where('user_id',$userId)->first();

//     if($fmcToken){
//         $fmcToken->delete();
//     }

//     Auth::logout();
//     $request->session()->invalidate();
//     $request->session()->regenerateToken();


//     return response()->json(['status'=>200,'message'=>'User LogOut Successfully']);
// }


public function chatNotification(Request $request){

     $userIds = User::whereIn('mobile',$request->receivers)->pluck('id')->toArray();
    
// return $request->isgroupchat;
     if(!$userIds){
         return response()->json(['status'=>400,'message'=>"User not found"]);
     }
 
     $existingToken = FcmToken::whereIn('user_id', $userIds)->pluck('fcm_token')->toArray();
 
     app('App\Http\Controllers\Controller')->sendNotificationPush($request->name, $request->chatid, $existingToken, 'chat',$request->isgroupchat,$request->profilepic,$request->senderid,$request->issecretservice, $request->message);
             
     return response()->json(['status'=>200,'message'=>"Chat Notification Send"]);
 
 }
 
}
