<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Recruiter;
use Aws\S3\S3Client;
use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\Facades\Storage;

class RecruitersController extends Controller
{
    public function index()
    {
        $recruiters = Recruiter::all();
        return response()->json(['status' => 200, 'recruiters' => $recruiters]);
    }

    
    public function store(Request $request)
    {
        $recruiter = new Recruiter();
        $recruiter->name = $request->input('name');
        $recruiter->description = $request->input('description');
    
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $imagePath = "recruiter/" . time() . ' ' . $file->getClientOriginalName();
    
            Storage::disk("s3")->put($imagePath, file_get_contents($file));
            Storage::disk('s3')->setVisibility($imagePath, 'public');
            $url = Storage::disk("s3")->url($imagePath);
    
            $recruiter->logo = $url; // Assign the URL to the 'logo' attribute
        }
    
        $recruiter->url = $request->input('url');
        $recruiter->link = $request->input('link');
        $recruiter->type = $request->input('type');
        $recruiter->save();
    
        return response()->json(['status' => 200, 'recruiter' => $recruiter]);
    }
    
    public function update(Request $request, Recruiter $recruiter)
    {
        // $recruiter->name = $request->input('name');
        $name = $request->input('name');
        if ($name !== null) {
            $recruiter->name = $name;
        }
    
        // $recruiter->description = $request->input('description');
        $description = $request->input('description');
        if ($description !== null) {
            $recruiter->description = $description;
        }
        // $recruiter->logo = $request->input('logo');
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $imagePath = "recruiter/" . time() . ' ' . $file->getClientOriginalName();

          

            Storage::disk("s3")->put($imagePath, file_get_contents($file));

            Storage::disk('s3')->setVisibility($imagePath,'public');
            
            $url = Storage::disk("s3")->url($imagePath);


            $recruiter->logo;
            
        }
        // $recruiter->url = $request->input('url');
        $url = $request->input('url');
        if ($url !== null) {
            $recruiter->url = $url;
        }
        // $link = $request->input('link');
        $link = $request->input('link');
        if ($link !== null) {
            $recruiter->link = $link;
        }
        // $recruiter->type = $request->input('type');
        $type = $request->input('type');
        if ($type !== null) {
            $recruiter->type = $type;
        }
        $recruiter->save();

        return response()->json(['status' => 200, 'recruiter' => $recruiter]);
    }

    public function destroy(Attribute $attribute)
    {
        $attribute->delete();

        return response()->json(['status' => 200, 'message' => 'Attribute deleted successfully']);
    }
}
