<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\State;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CityControllerAPI extends Controller
{
    public function getCitiesByState($stateId)
    {
        $state = State::findOrFail($stateId);
        $cities = $state->cities;
        return response()->json(['cities' => $cities]);
    }


    public function updateSelectedCity(Request $request)
{
    $userId = Auth::id();
    // $userId = $request->input('users_id');
    $selectedCity = $request->input('selected_city');
    $country = City::where('id', $selectedCity)->pluck('name')->first();
    // Perform validation if needed, e.g., check if the city is valid.

    // Update the selected city in the 'users' table
    DB::table('users')->where('id', $userId)->update(['city' => $country]);

    return response()->json(['status' => 200, 'message' => 'Selected city updated successfully']);
}

}