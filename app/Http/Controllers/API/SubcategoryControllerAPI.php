<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subcategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class SubcategoryControllerAPI extends Controller
{
    public function index()
    {
        $subcategories = Subcategory::with('user')->get();

        return response()->json(['subcategories' => $subcategories]);
    }

    // public function store(Request $request)
    // {
    //     $subcategory = new Subcategory();
    //     $subcategory->category_id = $request->input('category_id');
    //     $subcategory->title = $request->input('title'); // Corrected input key for title
    //     $subcategory->description = $request->input('description');
        
    //     // Handle image upload
    //     if ($request->hasFile('image')) {
    //         // return 1;
    //         $file = $request->file('image');
    //         $path = $file->store('subcategoryimages', 'public');
    //         $subcategory->image = $path;
    //     }
        
    //     $subcategory->save();
    
    //     return response()->json(['subcategory' => $subcategory], 201);
    // }

    public function store(Request $request)
{
    $subcategory = new Subcategory();
    $subcategory->category_id = $request->input('category_id');
    $subcategory->title = $request->input('title');
    $subcategory->description = $request->input('description');
    
    // Handle image upload
    if ($request->hasFile('image')) {
        $file = $request->file('image');
        $path = $file->store('subcategoryimages', 'public');
        $fullImagePath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $path;
        $subcategory->image = $fullImagePath;
    }
    
    $subcategory->save();

    return response()->json(['status'=>200,'subcategory' => $subcategory], 201);
}


    

public function update(Request $request, Subcategory $subcategory)
{
    $category_id = $request->input('category_id');
    if ($category_id !== null) {
        $subcategory->category_id = $category_id;
    }

    // $subcategory->category_id = $request->input('category_id');
    $title = $request->input('title');
    if ($title !== null) {
        $subcategory->title = $title;
    }
    // $subcategory->title = $request->input('title');
    $description = $request->input('description');
    if ($description !== null) {
        $subcategory->description = $description;
    }
    // $subcategory->description = $request->input('description');

    // Handle image upload
    if ($request->hasFile('image')) {
        // Delete previous image if exists
        if ($subcategory->image) {
            Storage::disk('public')->delete($subcategory->image);
        }

        $image = $request->file('image');
        $imagePath = $image->store('subcategoryimages', 'public');
        $fullImagePath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $imagePath;
        $subcategory->image = $fullImagePath;
    }
    


    $subcategory->save();

    return response()->json(['status'=>200,'subcategory' => $subcategory]);
}

    

    public function destroy(Subcategory $subcategory)
{
    // Delete the associated image from storage
    if ($subcategory->image) {
        Storage::disk('public')->delete($subcategory->image);
    }

    // Delete the subcategory
    $subcategory->delete();

    return response()->json(['message' => 'Subcategory deleted successfully']);
}

}
