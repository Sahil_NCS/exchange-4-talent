<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Subscription;
use App\Models\Rating;
use Illuminate\Support\Facades\Storage;
use App\Models\Tag;
use App\Models\Ads;
use App\Models\Imaged;
use Illuminate\Support\Facades\Schema;
// use Carbon\Carbon;
use \DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Media\Video;
use FFMpeg\Coordinate\TimeCode;
use Illuminate\Support\Facades\Validator;
use Aws\S3\S3Client;




class AdsControllerAPI extends Controller
{
    public function index()
    {
        $ads = Ads::all();
        return response()->json(['status'=>200,'ads' => $ads]);
    }
 

    public function store(Request $request)
    {
        $ads = new Ads();
        // $ads->title = $request->input('videos');

        $validator = Validator::make($request->all(), [
            'video' => 'required|mimetypes:video/mp4,video/x-msvideo,video/mpeg|max:102400', // Adjust max file size as needed
        ]);
    
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'message' => 'Invalid video file', 'errors' => $validator->errors()]);
        }
        

        if ($request->hasFile('video')) {
            $videoFile = $request->file('video');
            if ($videoFile) {
                $videoPath = "ads-videos/" . time() . ' ' . $videoFile->getClientOriginalName();
                
                Storage::disk("s3")->put($videoPath, file_get_contents($videoFile));
                Storage::disk('s3')->setVisibility($videoPath, 'public');
                $videoUrl = Storage::disk("s3")->url($videoPath);
                
                // Update the ads's videos field with the new URL
                $ads->videos = $videoUrl;
            }
        }
        
        if ($request->hasFile('image')) {
            $imageFile = $request->file('image');
            if ($imageFile) {
                $imagePath = "ads-images/" . time() . ' ' . $imageFile->getClientOriginalName();
                
                Storage::disk("s3")->put($imagePath, file_get_contents($imageFile));
                Storage::disk('s3')->setVisibility($imagePath, 'public');
                $imageUrl = Storage::disk("s3")->url($imagePath);
                
                // Update the ads's images field with the new URL
                $ads->images = $imageUrl;
            }
        }
        
        $ads->save();
        

        return response()->json(['status'=>200,'ads' => $ads]);
    }

 

// public function update(Request $request, User $users)
// {
//     // $users->firebase_id = $request->input('firebase_id');
//     $firebase_id = $request->input('firebase_id');
//     if ($firebase_id !== null) {
//        $users->firebase_id = $firebase_id;
//     }
//     // $users->name = $request->input('name');
//     $name = $request->input('name');
//     if ($name !== null) {
//        $users->name = $name;
//     }

//     $description = $request->input('description');
//     if ($description !== null) {
//        $users->description = $description;
//     }

//     $email = $request->input('email');
//      if ($email !== null) {
//         $users->email = $email;
//     }
//     // $users->email = $request->input('email');
//     // $users->mobile_otp = $request->input('mobile_otp');
//     // $mobile_otp = $request->input('mobile_otp');
//     // if ($mobile_otp !== null) {
//     //    $users->mobile_otp = $mobile_otp;
//     // }
//     // $users->email_otp = $request->input('email_otp');
//     $email_otp = $request->input('email_otp');
//     if ($email_otp !== null) {
//        $users->email_otp = $email_otp;
//     }
//     // $users->mobile_verified = $request->input('mobile_verified');
//     $mobile_verified = $request->input('mobile_verified');
//     if ($mobile_verified !== null) {
//        $users->mobile_verified = $mobile_verified;
//     }
//     // $users->email_verified = $request->input('email_verified');
//     $email_verified = $request->input('email_verified');
//     if ($email_verified !== null) {
//        $users->email_verified = $email_verified;
//     }

//     $dateInput = $request->input('dob');
//     if ($dateInput !== null) {
//         $users->dob = $dateInput;
     
//     if ($dateInput) {
//         // Convert the date input to 'yyyy-mm-dd' format
//         $date = DateTime::createFromFormat('d-m-Y', $dateInput);
//         if ($date !== false) {
//             $formattedDate = $date->format('Y-m-d');
//             $users->dob = $formattedDate;
//         } else {
//             // Handle invalid date input
//             return response()->json(['status'=>400, 'error' => 'Invalid date format']);
//             }
//         } 
//     }




    
//     // $talentType = $request->input('talent_type');
//     $talent_type = $request->input('talent_type');
//     if ($talent_type !== null) {
//         $users->talent_type = $talent_type ? 1 : 0;
//     }
//     // Convert the boolean value to an integer
   

//     // $users->gender = $request->input('gender');
//     $gender = $request->input('gender');
//     if ($gender !== null) {
//        $users->gender = $gender;
//     }
//     // $users->address = $request->input('address');
//     $address = $request->input('address');
//     if ($address !== null) {
//        $users->address = $address;
//     }

//     $country = $request->input('country');
//     if ($country !== null) {
//        $users->country = $country;
//     }

//     $state = $request->input('state');
//     if ($state !== null) {
//        $users->state = $state;
//     }

//     $city = $request->input('city');
//     if ($city !== null) {
//        $users->city = $city;
//     }
    
//     // $users->password = $request->input('password');
//     $password = $request->input('password');
//     if ($password !== null) {
//        $users->password = $password;
//     }


//     $facebook_link = $request->input('facebook_link');
//     if ($facebook_link !== null) {
//        $users->facebook_link = $facebook_link;
//     }

//     $instagram_link = $request->input('instagram_link');
//     if ($instagram_link !== null) {
//        $users->instagram_link = $instagram_link;
//     }


//     $linkedIn_link = $request->input('linkedIn_link');
//     if ($linkedIn_link !== null) {
//        $users->linkedIn_link = $linkedIn_link;
//     }


//     $wikipedia_link = $request->input('wikipedia_link');
//     if ($wikipedia_link !== null) {
//        $users->wikipedia_link = $wikipedia_link;
//     }


//     $imdb_link = $request->input('imdb_link');
//     if ($imdb_link !== null) {
//        $users->imdb_link = $imdb_link;
//     }


//     // $users->remember_token = $request->input('remember_token');
//     $remember_token = $request->input('remember_token');
//     if ($remember_token !== null) {
//        $users->remember_token = $remember_token;
//     }
//     // $users->category_id = $request->input('category_id');
//     $category_id = $request->input('category_id');
//     if ($category_id !== null) {
//         $users->category_id = (int) $category_id;
//     }
//     // $users->subcategory_id = $request->input('subcategory_id');
//     $subcategory_id = $request->input('subcategory_id');
//     if ($subcategory_id !== null) {
//        $users->subcategory_id =(int) $subcategory_id;
//     }

//     // $users->trade_barter = $request->input('trade_barter');
//     // $talent_barter = $request->input('trade_barter');
//     $trade_barter = $request->input('trade_barter');
//     if ($trade_barter !== null) {
//         $users->trade_barter = $trade_barter ? 1 : 0;
//     }
//     // Convert the boolean value to an integer
   

//     // $users->subscription_id = $request->input('subscription_id');
//     $subscription_id = $request->input('subscription_id');
//     if ($subscription_id !== null) {
//        $users->subscription_id =(int) $subscription_id;
//     }
//     // $users->subscription_validity = $request->input('subscription_validity');
//     $subscription_validity = $request->input('subscription_validity');
//     if ($subscription_validity !== null) {
//        $users->subscription_validity = $subscription_validity;
//     }
//     // $users->subscribed_on = $request->input('subscribed_on');
//     $subscribed_on = $request->input('subscribed_on');
//     if ($subscribed_on !== null) {
//        $users->subscribed_on = $subscribed_on;
//     }
//     // $users->booking_fee = $request->input('booking_fee');
//     $booking_fee = $request->input('booking_fee');
//     if ($booking_fee !== null) {
//        $users->booking_fee = $booking_fee;
//     }

//      // Handle avatar/image upload
//      if ($request->hasFile('avatar')) {
//         // Delete the previous avatar if it exists
//         if ($users->avatar) {
//             Storage::disk('public')->delete($users->avatar);
//         }

//         $avatar = $request->file('avatar');
//         $avatarPath = $avatar->store('avatarsimage', 'public');
//         $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//         $users->avatar = $fullAvatarPath;
//     }

//     if ($request->hasFile('image1')) {
//          // Delete the previous avatar if it exists
//          if ($users->image1) {
//             Storage::disk('public')->delete($users->image1);
//         }
//         $image1 = $request->file('image1');
//         $avatarPath = $image1->store('avatarsimage', 'public');
//         $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//         $users->image1 = $fullAvatarPath;
//     }

//     if ($request->hasFile('image2')) {
//          // Delete the previous avatar if it exists
//          if ($users->image2) {
//             Storage::disk('public')->delete($users->image2);
//         }

//         $image2 = $request->file('image2');
//         $avatarPath = $image2->store('avatarsimage', 'public');
//         $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//         $users->image2 = $fullAvatarPath;
//     }

//     if ($request->hasFile('image3')) {
//          // Delete the previous avatar if it exists
//          if ($users->image3) {
//             Storage::disk('public')->delete($users->image3);
//         }

//         $image3 = $request->file('image3');
//         $avatarPath = $image3->store('avatarsimage', 'public');
//         $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//         $users->image3 = $fullAvatarPath;
//     }

//     if ($request->hasFile('image4')) {
//          // Delete the previous avatar if it exists
//          if ($users->image4) {
//             Storage::disk('public')->delete($users->image4);
//         }

//         $image4 = $request->file('image4');
//         $avatarPath = $image4->store('avatarsimage', 'public');
//         $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//         $users->image4 = $fullAvatarPath;
//     }

//     if ($request->hasFile('image5')) {
//         // Delete the previous avatar if it exists
//         if ($users->image5) {
//             Storage::disk('public')->delete($users->image5);
//         }

//         $image5 = $request->file('image5');
//         $avatarPath = $image5->store('avatarsimage', 'public');
//         $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//         $users->image5 = $fullAvatarPath;
//     }

//     if ($request->hasFile('image6')) {
//         // Delete the previous avatar if it exists
//         if ($users->image6) {
//             Storage::disk('public')->delete($users->image6);
//         }

//         $image6 = $request->file('image6');
//         $avatarPath = $image6->store('avatarsimage', 'public');
//         $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//         $users->image6 = $fullAvatarPath;
//     }
//     if ($request->hasFile('image7')) {
//         // Delete the previous avatar if it exists
//         if ($users->image7) {
//             Storage::disk('public')->delete($users->image7);
//         }
//         $image7 = $request->file('image7');
//         $avatarPath = $image7->store('avatarsimage', 'public');
//         $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//         $users->image7 = $fullAvatarPath;
//     }


//          // Handle avatar/image upload
//          if ($request->hasFile('videos')) {
//             // Delete the previous avatar if it exists
//             if ($users->videos) {
//                 Storage::disk('public')->delete($users->videos);
//             }
    
//             $avatar = $request->file('videos');
//             $avatarPath = $avatar->store('usersvideo', 'public');
//             $fullAvatarPath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $avatarPath;
//             $users->videos = $fullAvatarPath;
//         }




//     $users->save();

//     return response()->json(['status'=>200,'user' => $users]);
// }
    

// public function destroy()
// {
//     $userId = Auth::id();
//     $user = User::find($userId);

//     if ($user->avatar) {
//         Storage::disk('public')->delete($user->avatar);
//     }

//     $user->delete();

//     return response()->json(['status' => 200, 'message' => 'User deleted successfully']);
// }



}
