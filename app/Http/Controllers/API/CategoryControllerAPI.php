<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Support\Facades\URL;

class CategoryControllerAPI extends Controller
{
    // public function index()
    // {
    //     $categories = Category::has('subcategories')->with('subcategories')->get();
    //     $random_categories = Category::inRandomOrder()->take(5)->get();
    //     return response()->json(['categories' => $categories, 'random_categories' => $random_categories]);
    // }


    public function index()
    {
        $categories = Category::all();
        // $random_categories = Category::inRandomOrder()->take(5)->get();
        return response()->json(['status'=>200,'categories' => $categories]);

    }


    public function index1()
    {
        $categories = Category::paginate(15);
        // $random_categories = Category::inRandomOrder()->take(5)->get();
        return response()->json(['status'=>200,'categories' => $categories]);

    }

    


    public function categorytitle(){
        $categoriesTitle = Category::pluck('title','id');
        return response()->json(['categoriesTitle' => $categoriesTitle]);
    }

    public function getSubcategoriesByCategory(Request $request, $categoryId)
    {
        $subcategories = Subcategory::where('category_id', $categoryId)->take(4)->get();
        $subcategoryIds = $subcategories->pluck('id')->toArray();
        $banner = User::whereIn('subcategory_id', $subcategoryIds)->take(3)->get();
        $random_categories = Category::inRandomOrder()->take(5)->get();
        return response()->json(['subcategories' => $subcategories, 'random_categories' => $random_categories , 'banner' => $banner]);
    }


    public function getSubcategoriesByCategory1(Request $request)
    {
        $categoryId = $request->input('category_id');

        $users = User::whereRaw("FIND_IN_SET('$categoryId', REPLACE(category_ids, ' ', ''))")
        ->where('subscription_id', '!=', 0)
        ->take(10)
        ->orderBy('name', 'ASC')
        ->get();
        $subcategoryIds = $users->pluck('subcategory_id')->toArray();
        $banner = User::whereIn('subcategory_id', $subcategoryIds)->take(3)->get();
        $random_categories = Category::inRandomOrder()->take(5)->get();
        return response()->json(['Users' => $users, 'random_categories' => $random_categories , 'banner' => $banner]);
    }


  
    public function getSubcategoriesByCategory2(Request $request)
    {
        $categoryId = $request->input('category_id');

        $users = User::whereRaw("FIND_IN_SET('$categoryId', REPLACE(category_ids, ' ', ''))")
             ->where('subscription_id', '!=', 0)
             ->take(10)
             ->orderBy('name', 'ASC')
             ->get();
        // $users = User::where('category_ids', $categoryId)->take(10)->get();
        $subcategoryIds = $users->pluck('subcategory_id')->toArray();
        $banner = User::whereIn('subcategory_id', $subcategoryIds)->take(3)->get();
        $random_categories = Category::inRandomOrder()->take(5)->get();
        return response()->json(['Users' => $users, 'random_categories' => $random_categories , 'banner' => $banner]);
    }


    public function getAllSubcategoriesByCategory(Request $request, $categoryId)
    {
        $subcategories = Subcategory::where('category_id', $categoryId)->get();
        $random_categories = Category::inRandomOrder()->take(5)->get();
        return response()->json(['subcategories' => $subcategories, 'random_categories' => $random_categories , ]);
    }


    public function getUsersBySubcategory(Request $request, $subcategoryId)
    {
        $users = User::where('subcategory_id' , $subcategoryId)->get();
        // $random_categories = Category::inRandomOrder()->take(5)->get();
        return response()->json(['status'=>200,'users' => $users ]);
    }



    public function getUsersByCategory(Request $request){

        $category_id = $request->input('category_id');

        $users = User::where('category_id',$category_id)->get();

        return response()->json(['status'=>200,'users' => $users ]);
    }


    //    public function getUsersByCategory(Request $request){

    //     $category_id = $request->input('category_id');

    //     $users = User::where('category_id',$category_id)->get();

    //     return response()->json(['status'=>200,'users' => $users ]);
    // }

    public function getUsersByCategory2(Request $request)
    {



        $category_id = $request->input('category_id');
     
         // Query users where category_ids contains the specified value
        //  $users = User::where('category_ids', 'LIKE', "%$category_id%")->whereNotIn('subscription_id', [0])->paginate(10);

         $users = User::whereRaw("FIND_IN_SET('$category_id', REPLACE(category_ids, ' ', ''))")
             ->where('subscription_id', '!=', 0)->orderBy('name','ASC')->paginate(10);
        
         return response()->json(['status'=>200,'data' => $users ]);
     }



     public function getUsersByCategory3(Request $request)
     {
         $category_id = $request->input('category_id');
     
         // Query users where category_ids contains the specified value
        //  $users = User::where('category_ids', 'LIKE', "%$category_id%")->whereNotIn('subscription_id', [0])->paginate(10);
         $users = User::whereRaw("FIND_IN_SET('$category_id', REPLACE(category_ids, ' ', ''))")
             ->where('subscription_id', '!=', 0)
             ->take(10)
             ->get();
     
         return response()->json(['status' => 200, 'data' => $users]);
     }


 

    public function store(CreateCategoryRequest $request)
    {
        $validatedData = $request->validated();
    
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $path = $file->store('categoryimages', 'public');
            $validatedData['image'] = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $path;
        }
    
        $category = Category::create($validatedData);
        $category->title = $request->input('title');
        $category->description = $request->input('description');
        $category->save();
    
        return response()->json(['category' => $category, 'message' => 'Category created successfully'], 201);
    }
    




    public function update(UpdateCategoryRequest $request, Category $category)
{
    $data = $request->validated();

    // Update title and description
    // $category->title = $request->input('title');
    $title = $request->input('title');
    if ($title !== null) {
        $category->title = $title;
    }

    // $category->description = $request->input('description');
    $description = $request->input('description');
    if ($description !== null) {
        $category->description = $description;
    }

    
    // Handle image upload
    if ($request->hasFile('image')) {
        // Delete previous image if exists
        if ($category->image) {
            Storage::disk('public')->delete($category->image);
        }

        $file = $request->file('image');
        $path = $file->store('categoryimages', 'public');
        $fullImagePath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $path;
        $data['image'] = $fullImagePath;
    }

    $category->update($data);

    return response()->json(['category' => $category, 'message' => 'Category updated successfully'], 200);
}


    
    public function destroy(Category $category)
    {
        // Delete user's image if exists
        if ($category->image) {
            Storage::disk('public')->delete($category->image);
        }

        $category->delete();

        return response()->json(['message' => 'Category deleted successfully']);
    }
}