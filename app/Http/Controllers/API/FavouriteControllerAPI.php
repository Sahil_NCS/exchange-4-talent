<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Favourite;
use App\Models\User;
use Auth;
// use Illuminate\Support\Facades\Storage;

class FavouriteControllerAPI extends Controller
{
    public function index(Request $request)
    {
        $users = User::where('id',Auth::id())->with('favourite')->first();
        return response()->json(['status' => 200,
        'message' => 'user favourite data.' ,'data'=>$users->favourite]);
    }


    public function index1(Request $request)
{
    $userId = Auth::id();

    $perPage = $request->input('per_page', 10); // Default to 10 if per_page is not specified

    $favouriteTalentIds = User::where('id', $userId)
        ->with('favourite')
        ->first()
        ->favourite
        ->pluck('id');

    $favouriteTalents = User::whereIn('id', $favouriteTalentIds)
        ->paginate($perPage);

    return response()->json([
        'status' => 200,
        'message' => 'User favourite talents data.',
        'data' => $favouriteTalents,
    ]);
}

    public function store(Request $request)
{
    $talentId = $request->input('talent_id');
    $userId = Auth::id();

    $userExists = User::where('id', $talentId)->exists();

    // dd($talentId);

    if (!$userExists) {
        return response()->json([
            'status' => 400,
            'message' => 'User with the given talent ID does not exist.',
            'data' => false
        ]);
    }

    $favouriteExists = Favourite::where('user_id', $userId)
        ->where('talent_id', $talentId)
        ->exists();

    if ($favouriteExists) {
        // Remove the existing favourite
        Favourite::where('user_id', $userId)
            ->where('talent_id', $talentId)
            ->delete();
        
        User::where('id', $talentId)->update(['isfavourite' => false]);

        return response()->json([
            'status' => 200,
            'message' => ' removed from favourites.',
            'data' => false
        ]);
    }

    // Create a new favourite
    $favourite = new Favourite();
    $favourite->user_id = $userId;
    $favourite->talent_id = $talentId;
    $favourite->save();

    User::where('id', $talentId)->update(['isfavourite' => true]);

    return response()->json([
        'status' => 200,
        'message' => ' added to favourites.',
        'data' => true
    ]);
}

    
//     public function updateFavourites(Request $request)
// {
//     $talentId = $request->input('talent_id');

//     // Retrieve the favourites with matching talent_id
//     $matchingFavourites = Favourite::where('talent_id', $talentId)->get();

//     // Get the user IDs from the matching favourites
//     $userIds = $matchingFavourites->pluck('user_id');

//     // Update the isfavourite column to true for the matching users
//     User::whereIn('id', $userIds)->update(['isfavourite' => true]);

//     return response()->json(['message' => 'isfavourite column updated successfully']);
// }

    // public function update(Request $request, Favourite $favourites)
    // {

    //     $favourites->talent_id = $request->input('talent_id');
    //     $favourites->save();

    //     return response()->json(['favourites' => $favourites]);
    // }

    public function destroy(Favourite $favourites)
    {
        $favourites->delete();

        return response()->json(['message' => 'Favourite deleted successfully']);
    }
}


