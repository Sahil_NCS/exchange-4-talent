<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class CountryControllerAPI extends Controller
{
    public function getCountries()
    {
        $countries = Country::all();
        return response()->json(['countries' => $countries]);
    }


    public function updateSelectedCountry(Request $request)
{
    $userId = Auth::id();
    // $userId = $request->input('users_id');
    $selectedCountry = $request->input('selected_country');
    $country = Country::where('id', $selectedCountry)->pluck('name')->first();

    // Perform validation if needed, e.g., check if the country is valid.

    // Update the selected country in the 'users' table
    DB::table('users')->where('id', $userId)->update(['country' => $country]);

    return response()->json(['status' => 200, 'message' => 'Selected country updated successfully']);
}





}