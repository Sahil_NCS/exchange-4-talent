<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscription;
use App\Models\SubscriptionsHistory;
use App\Models\User;
use App\Models\Coupon;
use App\Models\Payment;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

 
class SubscriptionControllerAPI extends Controller
{
    public function index()
    {
        // $subscriptions = Subscription::get();
        // return response()->json(['subscriptions' => $subscriptions]);

        $subscriptions = Subscription::take(3)->get();

        $formattedSubscriptions = $subscriptions->map(function ($subscription) {

            $description = $subscription->description;
            $description = str_replace(['[', ']', '\''], '', $description);
            $descriptionArray = explode(',', $description);
            $descriptionArray = array_map('trim', $descriptionArray);
        
            return [
                'id' => $subscription->id,
                'title' => $subscription->title,
                'subscriptionData' => [
                    'price' => $subscription->price,
                    'plan' => $subscription->plan,
                    'validity' => $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months',
                   
                        'description' => $descriptionArray,
                        'images' => $subscription->images,
                        'videos' => $subscription->videos,
                        'image_changes' => $subscription->image_changes,
                        'video_changes' => $subscription->video_changes,
                        'max_images_size' => $subscription->max_images_size,
                        'max_video_size' => $subscription->max_video_size,
                   
                ],
                'created_at' => $subscription->created_at,
                'updated_at' => $subscription->updated_at,
            ];
        });
        
        return response()->json(['status'=>200,'subscriptions' => $formattedSubscriptions]);

    }

    public function index1(Request $request)
    {
        // $subscriptions = Subscription::get();
        // return response()->json(['subscriptions' => $subscriptions]);
        $subscription_Id = $request->input("subscription_id");

        $subscriptions = Subscription::where('id', '>', $subscription_Id)->orderByDesc('id')
        ->skip(1) // Skip the last subscription (most recent one)
        ->take(3) // Take the next three subscriptions
        ->get();

        $formattedSubscriptions = $subscriptions->map(function ($subscription) {

            $description = $subscription->description;
            $description = str_replace(['[', ']', '\''], '', $description);
            $descriptionArray = explode(',', $description);
            $descriptionArray = array_map('trim', $descriptionArray);
        
            return [
                'id' => $subscription->id,
                'title' => $subscription->title,
                'product_id' => $subscription->productId,
                'subscriptionData' => [
                    'price' => $subscription->price,
                    'plan' => $subscription->plan,
                    
                    'validity' => $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months',
                   
                        'description' => $descriptionArray,
                        'images' => $subscription->images,
                        'videos' => $subscription->videos,
                        'image_changes' => $subscription->image_changes, 
                        'video_changes' => $subscription->video_changes,
                        'max_images_size' => $subscription->max_images_size,
                        'max_video_size' => $subscription->max_video_size,
                   
                ],
                'created_at' => $subscription->created_at,
                'updated_at' => $subscription->updated_at,
            ];
        });
        
        return response()->json(['status'=>200,'subscriptions' => $formattedSubscriptions]);

    }

    public function addSubscription(Request $request , $subscriptionId ,$price){

        $users = User::where('id',Auth::id())->with('subscription')->first();

    }

   


    public function checkout(Request $request)
    {
        $userId = Auth::id();
        $subscriptionId = $request->input('subscription_id');
        $price = $request->input('price');
        $couponCode = $request->input('coupon_code');
    
        // Perform any validation if needed, e.g., check if the subscription and user exist in the database.
    
        // Get the subscription validity and price from the 'subscriptions' table
        $subscription = DB::table('subscriptions')->select('validity')->where('id', $subscriptionId)->first();
        if (!$subscription) {
            return response()->json(['status' => 400, 'message' => 'Invalid subscription']);
        }
    
          // Apply coupon code logic
          if ($couponCode == "freegold") {
            $subscriptionId = 21;
            $subscription->color_code = "ffd9a321";
        }
        
        
        // Calculate the expire_date based on the buy_date and subscription validity
        $buyDate = Carbon::now();
        $expireDate = $buyDate->copy()->addMonths($subscription->validity);
    
        $currentDate = Carbon::now(); 
        $expire_date = Carbon::parse($expireDate);
        // $isActive = $expire_date >= $currentDate ? 1 : 0;
        // Check if the subscription has expired
        $subscriptionExpired = Carbon::now()->gte($expireDate);
    
        // You can also get the number of minutes remaining until the subscription expires
        // $minutesRemaining = Carbon::now()->diffInMinutes($expireDate);
    
        $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months';
    
        // Store the subscription and user details in the subscription_history table
        $subscriptionHistory = SubscriptionsHistory::create([
            'user_id' => $userId,
            'subscription_id' => $subscriptionId,
            'price' => $price, // Store the discounted price
            'buy_date' => $buyDate,
            'isActive' => true,
            'expire_date' => $expireDate,
            'created_at' => now(),
            'updated_at' => now()
        ]);
    
        // Update the 'users' table with subscription details if the subscription is active
        $subscriptionStatus = $subscriptionExpired ? 0 : $subscriptionId;
        $subscribedOn = $subscriptionExpired ? null : $buyDate;
    
        DB::table('users')
            ->where('id', $userId)
            ->update([
                'subscription_id' => $subscriptionStatus,
                'subscription_validity' => $expireDate,
                'subscribed_on' => $subscribedOn
            ]);
    
        // Perform any further actions like making a payment, updating user's subscription, etc.
    
        return response()->json([
            'status' => 200,
            'message' => 'Checkout successful.',
            'user_id' => $userId,
            'validityMonths' => $validityMonths,
            'coupne_code' => $couponCode,
            'subscription_history_id' => $subscriptionHistory->id,
            'final_price' => $price,
            'isActive' => $subscriptionHistory->isActive, // Access the accessor directly
            'subscription_id' =>  $subscriptionStatus,
            'subscription_expired' => $subscriptionExpired,
            // 'minutes_remaining' => $minutesRemaining,
            'expire_date' => $expireDate->toDateString() // Convert expire_date to a formatted date string
        ]);
    }
    

    // public function checkout1(Request $request)
    // {
    //     $userId = Auth::id();
    //     $subscriptionId = $request->input('subscription_id');
    //     $price = $request->input('price');
    //     $couponCode = $request->input('coupon_code');
    //     $razorpay_payment_id = $request->input('razorpay_payment_id');
    //     $razorpay_order_id = $request->input('razorpay_order_id');
    //     $razorpay_signature = $request->input('razorpay_signature');
    //     $receiptId = $request->input('receiptId');

    
    //     // Get the subscription validity and price from the 'subscriptions' table
    //     $subscription = DB::table('subscriptions')->select('validity','color_code')->where('id', $subscriptionId)->first();
    //     if (!$subscription) {
    //         return response()->json(['status' => 400, 'message' => 'Invalid subscription']);
    //     }
    
    //     // Calculate the expire_date based on the buy_date and subscription validity
    //     $buyDate = Carbon::now();
    //     $expireDate = $buyDate->copy()->addMonths($subscription->validity);
    //     // $expireDate = $buyDate->copy()->addMinutes($subscription->validity);

    
    //     // Check if the subscription has expired
    //     $subscriptionExpired = Carbon::now()->gte($expireDate);
    
    //     // You can also get the number of minutes remaining until the subscription expires
    //     // $minutesRemaining = Carbon::now()->diffInMinutes($expireDate);
    
    //     $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months';
    
    //     // Store the subscription and user details in the subscription_history table
    //     $subscriptionHistory = SubscriptionsHistory::create([
    //         'user_id' => $userId,
    //         'subscription_id' => $subscriptionId,
    //         'price' => $price, // Store the discounted price
    //         'buy_date' => $buyDate,
    //         'isActive' => true,
    //         'razorpay_payment_id' => $razorpay_payment_id,
    //         'razorpay_order_id' => $razorpay_order_id,
    //         'razorpay_signature' => $razorpay_signature,
    //         'receiptId' => $receiptId,
    //         'expire_date' => $expireDate,
    //         'created_at' => now(),
    //         'updated_at' => now()
    //     ]);
    
    //     // Update the 'users' table with subscription details if the subscription is active
    //     $subscriptionStatus = $subscriptionExpired ? 0 : $subscriptionId;
    //     $subscribedOn = $subscriptionExpired ? null : $buyDate;
    
    //     DB::table('users')
    //         ->where('id', $userId)
    //         ->update([
    //             'subscription_id' => $subscriptionStatus,
    //             'subscription_validity' => $expireDate,
    //             'subscribed_on' => $subscribedOn,
    //             'color_code' => $subscription->color_code
    //         ]);
    
    //     // Perform any further actions like making a payment, updating user's subscription, etc.
    
    //     return response()->json([
    //         'status' => 200,
    //         'message' => 'Checkout successful.',
    //         'user_id' => $userId,
    //         'validityMonths' => $validityMonths,
    //         'coupne_code' => $couponCode,
    //         'subscription_history_id' => $subscriptionHistory->id,
    //         'final_price' => $price,
    //         'isActive' => $subscriptionHistory->isActive, // Access the accessor directly
    //         'subscription_expired' => $subscriptionExpired,
    //         'razorpay_payment_id' => $razorpay_payment_id,
    //         'razorpay_order_id' => $razorpay_order_id,
    //         'razorpay_signature' => $razorpay_signature,
    //         'receiptId' => $receiptId,
    //         // 'minutes_remaining' => $minutesRemaining,
    //         'expire_date' => $expireDate->toDateString() // Convert expire_date to a formatted date string
    //     ]);
    // }

    public function checkout1(Request $request)
    {
        try {
            $userId = Auth::id();
            $subscriptionId = $request->input('subscription_id');
            $price = $request->input('price');
            $couponCode = strtolower($request->input('coupon_code'));
            
            // Fetch subscription information from the database
            $subscription = DB::table('subscriptions')
            ->select('validity', 'color_code')
            ->where('id', $subscriptionId)
            ->first();
            
            if (!$subscription) {
                return response()->json(['status' => 400, 'message' => 'Invalid subscription']);
            }
    
            // Apply coupon code logic
            if ($couponCode == "freegold") {
                $subscriptionId = 21;
                $subscription->color_code = "ffd9a321";
            }
            
        
                
            $buyDate = Carbon::now();
            $expireDate = $buyDate->copy()->addMonths($subscription->validity);
    
            // Check if the subscription has expired
            $subscriptionExpired = Carbon::now()->gte($expireDate);
            
            $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months';
    
            // Store subscription history
            $subscriptionHistoryData = [
                'user_id' => $userId,
                'subscription_id' => $subscriptionId,
                'price' => $price,
                'buy_date' => $buyDate,
                'isActive' => !$subscriptionExpired,
                'expire_date' => $expireDate,
                // Other fields related to payment
            ];
            
            $subscriptionHistory = SubscriptionsHistory::create($subscriptionHistoryData);
    
            // Update user's subscription status
            $subscriptionStatus = $subscriptionExpired ? 0 : $subscriptionId;
            $subscribedOn = $subscriptionExpired ? null : $buyDate;
    
            DB::table('users')
                ->where('id', $userId)
                ->update([
                    'subscription_id' => $subscriptionStatus,
                    'subscription_validity' => $expireDate,
                    'subscribed_on' => $subscribedOn,
                    'color_code' => $subscription->color_code
                ]);
    
            // Return response
            return response()->json([
                'status' => 200,
                'message' => 'Checkout successful.',
                'user_id' => $userId,
                'validityMonths' => $validityMonths,
                'coupne_code' => $couponCode,
                'subscription_history_id' => $subscriptionHistory->id,
                'final_price' => $price,
                'isActive' => !$subscriptionExpired,
                'subscription_expired' => $subscriptionExpired,
                'expire_date' => $expireDate->toDateString()
            ]);
    
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => 'An error occurred.']);
        }
    }

    public function checkout2(Request $request)
    {
        try {
            $userId = Auth::id();
            $subscriptionId = $request->input('subscription_id');
            $price = $request->input('price');
            $couponCode = strtolower($request->input('coupon_code'));

            $length = 16;
            $tracking_id = Str::random($length) . rand(0, 999);
            $order_id = Str::random($length) . rand(0, 999);

            $paymentType =$request->paymentType;
            $status = $request->status;
            
            if ($paymentType || $status) {
                $paymentType = $paymentType ? 'ios' : 'android'; // Set paymentType based on the condition
                try{
                  $payment =  Payment::create([
                        'user_id' => $userId,
                        'subscription_id' => $subscriptionId,
                        'tracking_id' => $tracking_id,
                        'order_id' => $order_id,
                        'amount' => $price,
                        'paymentType' => $paymentType,
                        'ccavenue_status' => $status,
                    ]);

                    info("|checkout2 Payment".$payment );
                }catch(\Exception $e){
                    return response()->json(['status' => 500, 'message' => $e]);
                } 
            }

            // Fetch subscription information from the database
             $subscription = DB::table('subscriptions')
            ->select('validity', 'color_code')
            ->where('id', $subscriptionId)
            ->first();
            
            if (!$subscription) {
                return response()->json(['status' => 400, 'message' => 'Invalid subscription']);
            }
    
            // Apply coupon code logic
            if ($couponCode == "freegold") {
                $subscriptionId = 21;
                $subscription->color_code = "ffd9a321";
            }
            
        
                
            $buyDate = Carbon::now();
            $expireDate = $buyDate->copy()->addMonths($subscription->validity);
    
            // Check if the subscription has expired
            $subscriptionExpired = Carbon::now()->gte($expireDate);
            
            $validityMonths = $subscription->validity == 1 ? 'Month' : $subscription->validity . ' Months';
    
            // Store subscription history
            $subscriptionHistoryData = [
                'user_id' => $userId,
                'subscription_id' => $subscriptionId,
                'price' => $price,
                'buy_date' => $buyDate,
                'isActive' => !$subscriptionExpired,
                'expire_date' => $expireDate,
                // Other fields related to payment
            ];
            
            $subscriptionHistory = SubscriptionsHistory::create($subscriptionHistoryData);
    
            // Update user's subscription status
            $subscriptionStatus = $subscriptionExpired ? 0 : $subscriptionId;
            $subscribedOn = $subscriptionExpired ? null : $buyDate;
    
            DB::table('users')
                ->where('id', $userId)
                ->update([
                    'subscription_id' => $subscriptionStatus,
                    'subscription_validity' => $expireDate,
                    'subscribed_on' => $subscribedOn,
                    'color_code' => $subscription->color_code
                ]);
    
            // Return response
            return response()->json([
                'status' => 200,
                'message' => 'Checkout successful.',
                'user_id' => $userId,
                'validityMonths' => $validityMonths,
                'coupne_code' => $couponCode,
                'subscription_history_id' => $subscriptionHistory->id,
                'final_price' => $price,
                'isActive' => !$subscriptionExpired,
                'subscription_expired' => $subscriptionExpired,
                'expire_date' => $expireDate->toDateString()
            ]);
    
        } catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => 'An error occurred.']);
        }
    }

        

    public function verify_coupne(Request $request){

       
        $userId = Auth::id();
        $subscriptionId = $request->input('subscription_id');
        $couponCode = $request->input('coupon_code');

        $subscription = DB::table('subscriptions')->select('validity', 'price')->where('id', $subscriptionId)->first();

        if (!$subscription) {
            return response()->json(['status' => 400, 'message' => 'Invalid subscription']);
        }

         // subscription_history 
         $subscription_coupne_Code = DB::table('subscriptions_history')
         ->select('coupon_code')
         ->where('user_id', $userId)
         ->orderBy('id', 'DESC') // Replace 'id' with the appropriate column for sorting
         ->first();
      
          // Apply coupon code logic
          if($subscription_coupne_Code == null){
            
            $discountedPrice = $subscription->price; // Initialize with original price
            if ($couponCode) {
                // Apply the coupon discount if the coupon code is provided and valid
                $coupon = DB::table('coupons')->where('coupon_code', $couponCode)->first();
                if ($coupon) {
                    $discountAmount = ($coupon->coupon_discount / 100) * $discountedPrice;
                    $finalPrice = $discountedPrice - $discountAmount;
                
                    // Check if the final price is less than 0 and set it to 0 if needed
                    $finalPrice = max(0, $finalPrice);
                } else {
                    return response()->json(['status' => 400, 'message' => 'Coupon Code is Invalid']);
                }
            }
    
            
            $finalPrice = intval($finalPrice);
            $discountAmount = intval($discountAmount);
    
    
    
             return response()->json([
                'status' => 200,
                'userId'=> $userId,
                'message' => 'Verified Coupne',
                'subscription_id' => $subscriptionId,
                'final_price' => $finalPrice,
                'discount'=>$discountAmount
            ]);

          }
          else if($couponCode == $subscription_coupne_Code->coupon_code){

              return response()->json(['status' => 400, 'message' => 'You already use this Code']);

          }else{

            $discountedPrice = $subscription->price; // Initialize with original price
            if ($couponCode) {
                // Apply the coupon discount if the coupon code is provided and valid
                $coupon = DB::table('coupons')->where('coupon_code', $couponCode)->first();
                if ($coupon) {
                    $discountAmount = ($coupon->coupon_discount / 100) * $discountedPrice;
                    $finalPrice = $discountedPrice - $discountAmount;
                
                    // Check if the final price is less than 0 and set it to 0 if needed
                    $finalPrice = max(0, $finalPrice);
                } else {
                    return response()->json(['status' => 400, 'message' => 'Coupon Code is Invalid']);
                }
            }
    
            
            $finalPrice = intval($finalPrice);
            $discountAmount = intval($discountAmount);
    
    
    
             return response()->json([
                'status' => 200,
                'userId'=> $userId,
                'message' => 'Verified Coupne',
                'subscription_id' => $subscriptionId,
                'final_price' => $finalPrice,
                'discount'=>$discountAmount
            ]);
          }
    }



    public function verify_coupne2(Request $request){

       
        $userId = Auth::id();
        $subscriptionId = $request->input('subscription_id');
        $couponCode = $request->input('coupon_code');

        $subscription = DB::table('subscriptions')->select('validity', 'price')->where('id', $subscriptionId)->first();
        if (!$subscription) {
            return response()->json(['status' => 400, 'message' => 'Invalid subscription']);
        }

         // subscription_history 
         $subscription_coupne_Code = DB::table('subscriptions_history')
         ->select('coupon_code')
         ->where('user_id', $userId)
         ->orderBy('id', 'DESC') // Replace 'id' with the appropriate column for sorting
         ->first();
      
          // Apply coupon code logic
          if($couponCode == $subscription_coupne_Code->coupon_code){

              return response()->json(['status' => 400, 'message' => 'You already use this Code']);

          }else{

            $discountedPrice = $subscription->price; // Initialize with original price
            if ($couponCode) {
                // Apply the coupon discount if the coupon code is provided and valid
                $coupon = DB::table('coupons')->where('coupon_code', $couponCode)->first();
                if ($coupon) {
                    $discountAmount = ($coupon->coupon_discount / 100) * $discountedPrice;
                    $finalPrice = $discountedPrice - $discountAmount;
                
                    // Check if the final price is less than 0 and set it to 0 if needed
                    $finalPrice = max(0, $finalPrice);
                } else {
                    return response()->json(['status' => 400, 'message' => 'Coupon Code is Invalid']);
                }
            }
    
            
            $finalPrice = intval($finalPrice);
            $discountAmount = intval($discountAmount);
    
    
    
             return response()->json([
                'status' => 200,
                'userId'=> $userId,
                'message' => 'Verified Coupne',
                'subscription_id' => $subscriptionId,
                'final_price' => $finalPrice,
                'discount'=>$discountAmount
            ]);

          }
    }


    public function availableCoupons(Request $request){

        try {

            $coupons = Coupon::all();

            if($coupons){
                return response()->json(['status'=>200,'message'=>'Coupons Code','couponCode'=>$coupons]);
            }
            else{
                return response()->json(['status'=>400,'message'=>'There is no Coupons Code','couponCode'=>$coupons]);
            }
        
            } catch (\Throwable $th) {
            return response()->json(['message'=>$th]);
            }
    }


    public function subscriptionByUsers(Request $request)
    {
        $subscriptionId = $request->input('subscription_id');
    
        $users = User::with('subscription')->where('subscription_id', $subscriptionId)->get();
    
        return response()->json(['status' => 200, 'users' => $users]);
    }


    public function subscriptionByUsers1(Request $request)
    {
        $subscriptionId = $request->input('subscription_id');
        $user_Id = Auth::id();
        
        $users = User::with('subscription')
            ->where('subscription_id', $subscriptionId)
            ->where('subscription_id', '!=', 0)
            ->where('id', '!=', $user_Id) // Exclude the currently authenticated user's ID
            ->paginate(25);

        return response()->json(['status' => 200, 'data' => $users]);
    }



    public function subscriptionByUsers2(Request $request)
{
        $subscriptionId = $request->input('subscription_id');
        $user_Id = Auth::id();
        
        $users = User::with('subscription')
            ->where('subscription_id', $subscriptionId)
            ->where('subscription_id', '!=', 0)
            ->where('id', '!=', $user_Id) // Exclude the currently authenticated user's ID
            ->paginate(25);

        return response()->json(['status' => 200, 'data' => $users]);
}




    public function store(Request $request, $userId, $subscriptionId)
{

    try {

    // Retrieve the user
    $user = User::find($userId);
    if (!$user) {
        return response()->json(['status'=>404,'error' => 'User not found']);
    }

    // Update the user's subscription ID
    $user->subscription_id = $subscriptionId;
    $user->save();


    // Save the subscription
   

    $responseData = [
        'status_code' => 200,
        'message' => 'Subscription data stored successfully',
        
    ];
    
    return response()->json(['status'=>200,$responseData]);



} catch (\Exception $e) {
    $responseData = [
        'status_code' => 200,
        'message' => 'Subscription data stored successfully',
        
    ];
    
    return response()->json(['status'=>200,$responseData]);
}


}



    public function update(Request $request, Subscription $subscription)
    {

        $data = $request->validated();

        $subscription->title = $request->input('title');
        $subscription->description = $request->input('description');
        $subscription->price = $request->input('price');
        // Delete the old image file if a new image is provided



        if ($request->hasFile('image')) {
            // Delete previous image if exists
            if ($subscription->images) {
                Storage::disk('public')->delete($subscription->images);
            }
    
            $file = $request->file('image');
            $path = $file->store('categoryimages', 'public');
            $fullImagePath = "https://demo.nimayate.com/e4t_new/storage/app/public/" . $path;
            $data['image'] = $fullImagePath;
        }

    
        // Delete the old video file if a new video is provided
        if ($request->hasFile('video')) {
            $oldVideoPath = $subscription->videos;
            if ($oldVideoPath) {
                Storage::disk('public')->delete($oldVideoPath);
            }
    
            // Handle video update
            $video = $request->file('video');
            $videoPath = $video->store('subscriptionvideos', 'public');
            $subscription->videos = $videoPath;
        }
    
        // Set other attributes
        $subscription->contacts = $request->input('contacts');
        $subscription->view_email = $request->input('view_email');
        $subscription->view_mobile = $request->input('view_mobile');
        $subscription->image_changes = $request->input('image_changes');
        $subscription->video_changes = $request->input('video_changes');
        $subscription->max_images_size = $request->input('max_images_size');
        $subscription->max_video_size = $request->input('max_video_size');
    
        // Save the updated subscription
        
        $subscription->update($data);
    
        return response()->json(['status'=>200,'subscription' => $subscription]);
    }
    

    public function destroy(Subscription $subscription)
    {
        // Delete the images and videos from storage (optional)
        if ($subscription->images) {
            Storage::disk('public')->delete($subscription->images);
        }

        if ($subscription->videos) {
            Storage::disk('public')->delete($subscription->videos);
        }

        // Delete the subscription
        $subscription->delete();

        return response()->json(['status'=>200,'message' => 'Subscription deleted successfully']);
    }


    public function paymentCheck(Request $request)
    {
        $record = DB::table('payments')->where('order_id',$request->order_id)->first();
        $payment_status = false;
        $expiry_date=null;
        $message = "Payment failed!";
        if(isset($record)){
             
            if($record->order_status == "Success"){
                $message = "Payment success!";
                $payment_status = true;

                $sub = DB::table('subscriptions_history')->where('user_id',Auth::id())->latest()->first();
                if(isset($sub))
                    $expiry_date = date("Y-m-d", strtotime($sub->expire_date));
            }
        }
        return response()->json(['status' => 200, 'payment_status' =>$payment_status,'expiry_date'=>$expiry_date,'message'=>$message ]);
    }



    public function paymentCheck1(Request $request)
    {
        $record = DB::table('payments')->where('order_id',$request->order_id)->first();
        $payment_status = false;
        $expiry_date=null;
        $message = "Payment failed!";
        if(isset($record)){
             
            if($record->order_status == "Success"){
                $message = "Payment success!";
                $payment_status = true;

                $sub = DB::table('subscriptions_history')->where('user_id',Auth::id())->latest()->first();
                if(isset($sub)){
                    $expiry_date = date("Y-m-d", strtotime($sub->expire_date));
                }

                $subscription = DB::table('subscriptions')->select('color_code')->where('id', $record->subscription_id)->first();

                DB::table('users')
                ->where('id',$record->user_id)
                ->update([
                    'subscription_id' => $record->subscription_id,
                    'subscription_validity' => date("Y-m-d", strtotime($sub->expire_date)),
                    'subscribed_on' => $sub->buy_date,
                    'color_code' => $subscription->color_code
                ]);
            }


            
        }
        return response()->json(['status' => 200, 'payment_status' =>$payment_status,'expiry_date'=>$expiry_date,'message'=>$message ]);
    }


}
