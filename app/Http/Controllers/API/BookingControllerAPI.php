<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Booking;
// use Illuminate\Support\Facades\Storage;

class BookingControllerAPI extends Controller
{
    public function index()
    {
        $bookings = Booking::all();
        return response()->json(['bookings' => $bookings]);
    }

    public function store(Request $request)
    {
        $bookings = new Booking();
        $bookings->user_id = $request->input('user_id');
        $bookings->talent_id = $request->input('talent_id');
        $bookings->booking_date = $request->input('booking_date');
        $bookings->booking_fee = $request->input('booking_fee');
        $bookings->booking_type = $request->input('booking_type');
        $bookings->appointment_date = $request->input('appointment_date');
        $bookings->payment_done = $request->input('payment_done');
        $bookings->payment_date = $request->input('payment_date');
        $bookings->payment_type = $request->input('payment_type');
        $bookings->save();

        return response()->json(['bookings' => $bookings], 201);
    }

    public function update(Request $request, Booking $bookings)
    {
        $bookings->user_id = $request->input('user_id');
        $bookings->talent_id = $request->input('talent_id');
        $bookings->booking_date = $request->input('booking_date');
        $bookings->booking_fee = $request->input('booking_fee');
        $bookings->booking_type = $request->input('booking_type');
        $bookings->appointment_date = $request->input('appointment_date');
        $bookings->payment_done = $request->input('payment_done');
        $bookings->payment_date = $request->input('payment_date');
        $bookings->payment_type = $request->input('payment_type');
        $bookings->save();

        return response()->json(['bookings' => $bookings]);
    }

    public function destroy(Booking $bookings)
    {
        $bookings->delete();

        return response()->json(['message' => 'Attribute deleted successfully']);
    }
}

