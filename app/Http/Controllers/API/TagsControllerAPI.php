<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagsControllerAPI extends Controller
{
    public function index()
    {
        $tags = Tag::all();

        return response()->json(['tags' => $tags]);
    }

    
        // Create a new tag
        public function store(Request $request)
        {
            $tag = new Tag;
            $tag->type = $request->input('type');
            $tag->tags_name = $request->input('tags_name');
            $tag->save();
    
            return response()->json($tag);
        }
    
        // Update an existing tag
        public function update(Request $request, $id)
        {
            $tag = Tag::find($id);
            $tag->type = $request->input('type');
            $tag->tags_name = $request->input('tags_name');
            $tag->save();
    
            return response()->json($tag);
        }
    
        // Delete a tag
        public function destroy($id)
        {
            $tag = Tag::find($id);
            $tag->delete();
    
            return response()->json('Tag deleted successfully');
        }
}
