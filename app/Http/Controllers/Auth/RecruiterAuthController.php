<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
class RecruiterAuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/recruiter/home';

    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }

    public function guard()
    {
     return Auth::guard('recruiter');
    }
    public function showLoginForm()
    {
        return view('recruiterdasboard.recruiter_login');
    }

    public function index()
    {
        return view('recruiterdasboard.welcome');
    }

    protected function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/recruiter/login');
    }




}
