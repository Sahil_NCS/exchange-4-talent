<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    // function sendPush1($title,$description,$uid = 0)
    // {
    //     $content = ["en" => $description];
    //     $head    = ["en" => $title];       

    //     $daTags = [];

    //     if($uid > 0)
    //     {
    //         $daTags = ["field" => "tag", "key" => "userId", "relation" => "=", "value" => $uid];
    //     }
    //     else
    //     {
    //         $daTags = ["field" => "tag", "key" => "userId", "relation" => "!=", "value" => 'NAN'];
    //     }
       
    //     $fields = array(
    //     'app_id' => "c8a558fa-860e-4ead-98f3-e25d78a5ccab",
    //     'included_segments' => array('All'),   
    //     'filters' => [$daTags],
    //     'data' => array("foo" => "bar"),
    //     'contents' => $content,
    //     'headings' => $head,
    //     );
       
    
    //     $fields = json_encode($fields);
       
    //     $ch = curl_init();
       
    //     curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    //     curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
    //     'Authorization: Basic NzhhYmMyZWMtYTY3OC00NDhhLThjYTktMjU3ZTZiYmI0MWU1'));
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    //     curl_setopt($ch, CURLOPT_HEADER, FALSE);
    //     curl_setopt($ch, CURLOPT_POST, TRUE);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    //     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    //     $response = curl_exec($ch);
    //     curl_close($ch);
      
    //     return $response;
    // }

    function sendPush($title, $description, $existingToken, $type,$jobid = 0)
{

    // Convert the $existingTokens array to a JSON string
    $registrationIds = json_encode($existingToken);

        $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
        "registration_ids":'. $registrationIds . ',
        "data": {
            "type": "' . $type . '",
            "jobid": ' . $jobid . '
        },
        "notification": {
            "body": "' . $description . '",
            "title": "' . $title . '",
        
            "sound": false
        }
    }',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: key=AAAA58r79pk:APA91bHh3Ysi03M-VmmDWETiUrg90REkAL42UhROjZ1JZaqmiGFWwwsKpnfL5IqFCNg3j1WIslVD8BLRuGL-DiKiMjGc8Rjac2hJ7gDzujoiahavS9LPEvMvg6saVwqYZFGK34i2capo'
    ),
    ));

    $response = curl_exec($curl);

    // curl_close($curl);
    // echo $response;

        if ($response === false) {
            // Handle cURL error
            $error = curl_error($curl);
            curl_close($curl);
            return "cURL error: " . $error;
        } else {
            // Handle OneSignal response
            $responseData = json_decode($response, true);
            curl_close($curl);

            if (isset($responseData['errors'])) {
                // Handle OneSignal API errors
                return "OneSignal API error: " . json_encode($responseData['errors']);
            } else {
                return $response;
            }
        }
}

function sendNotificationPush($name, $chatid, $existingToken, $type, $isgroupchat, $profilepic, $senderid, $issecretservice, $message)
{

    echo $isgroupchat;
    // Convert the $existingTokens array to a JSON string
    $registrationIds = json_encode($existingToken);
    // echo $registrationIds;
        $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://fcm.googleapis.com/fcm/send',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
        "registration_ids":'. $registrationIds . ',
        "data": {
            "type": "' . $type . '",
                "name": "' . $name . '",
                "chatid": "' . $chatid . '",
                "isgroupchat": "' . $isgroupchat . '",
                "profilepic": "' . $profilepic . '",
                "senderid": "' . $senderid . '",
                "issecretservice": "' . $issecretservice . '"
            
        },        
        "notification": {
            "body": "' . $message . '",
            "title": "' . $name . '",
        
            "sound": false
        }
    }',
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Authorization: key=AAAA58r79pk:APA91bHh3Ysi03M-VmmDWETiUrg90REkAL42UhROjZ1JZaqmiGFWwwsKpnfL5IqFCNg3j1WIslVD8BLRuGL-DiKiMjGc8Rjac2hJ7gDzujoiahavS9LPEvMvg6saVwqYZFGK34i2capo'
    ),
    ));

    $response = curl_exec($curl);

    // curl_close($curl);
    // echo $response;

        // if ($response === false) {
        //     // Handle cURL error
        //     $error = curl_error($curl);
        //     curl_close($curl);
        //     return "cURL error: " . $error;
        // } else {
        //     // Handle OneSignal response
        //     $responseData = json_decode($response, true);
        //     curl_close($curl);

        //     if (isset($responseData['errors'])) {
        //         // Handle OneSignal API errors
        //         return "OneSignal API error: " . json_encode($responseData['errors']);
        //     } else {
        //         return $response;
        //     }
        // }
}

}
