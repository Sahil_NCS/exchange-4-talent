<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\User;

class ImageController extends Controller
{
    public function index()
    {
        $img = Image::make('https://exchange4talent.s3.ap-south-1.amazonaws.com/images/Sara%20FE561C44-5523-4967-B40A-DEDC60F8F912.jpeg')->resize(300, 200);
         $img->response('jpg');

         $users = User::latest()->limit(10)->get(); // Fetch users from your database

        foreach ($users as $user) {
            // Get the images from S3 and manipulate using Intervention Image
            
                $avatar = Image::make($user->avatar)
                ->resize(200, 200)->encode('jpg')->getEncoded();
            $user->avatar = base64_encode($avatar);

            $image1 = Image::make($user->image1)
                ->resize(400, 400)->encode('jpg')->getEncoded();
            $user->image1 = base64_encode($image1);

            $image2 = Image::make($user->image2)
                ->resize(400, 400)->encode('jpg')->getEncoded();
            $user->image2 = base64_encode($image2);
        }

        return $users;


        
    }
}
