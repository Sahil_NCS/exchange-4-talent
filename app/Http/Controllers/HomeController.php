<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
 use App\Models\User;
  use App\Models\Category;
    use App\Models\Recruiter;
     use App\Models\Payment;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
            $userCount = User::count();
            $categoryCount = Category::count();
            $recruiterCount = Recruiter::count();

           $successCount = DB::table('payments')->where('order_status', 'Success')->count();
 

        return view('home', compact('userCount','categoryCount','recruiterCount','successCount'));
    }

    //  public function getUserCount()
    // {
      
    //     return $userCount = User::count();

    //     return view('home', ['userCount' => $userCount]);
    // }
}
