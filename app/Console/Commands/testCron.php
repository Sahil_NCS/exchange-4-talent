<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Kreait\Firebase\Contract\Database;
use Carbon\Carbon;

class testCron extends Command
{
    public $database;
    public $tablename;

    public function __construct(Database $database)
    {
        parent::__construct(); // Call the parent constructor
        $this->database = $database;
        $this->tablename = "/contact";
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:groupchat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete group chat messages after 5 min';

    /**
     * Execute the console command.
     */
    // public function handle()
    // {
    //     date_default_timezone_set('Asia/Kolkata'); // Set the timezone to Asia/Kolkata
    
    //     $references = $this->database->getReference($this->tablename)->getValue();
    
    //     foreach ($references as $key => $value) {
    //         if (isset($value['created_at'])) {
    //             $currentTime = Carbon::now();
    //             $deleteTime = Carbon::parse($value['created_at'])->addMinutes(5); // Add 5 minutes to the created_at time
    
    //             if ($currentTime >= $deleteTime) { // Check if the current time is greater than or equal to the delete time
    //                 $postRef = $this->database->getReference($this->tablename.'/'.$key)->remove();
    //             }
    //         }
    //     }
    // }
        public function handle()
    {
        $references = $this->database->getReference($this->tablename)->getValue();
    
        if ($references) {
            foreach ($references as $key => $value) {
                if (isset($value['created_at'])) {
                    $currentTime = Carbon::now();
                    $deleteTime = Carbon::parse($value['created_at']);
                    $timeDifference = $currentTime->diffInMinutes($deleteTime);
    
                    if ($timeDifference > 5) {
                        $postRef = $this->database->getReference($this->tablename.'/'.$key)->remove();
                    }
                }
            }
        }
    }
    
    
}
