<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Carbon\Carbon;
use App\Models\SubscriptionsHistory;
use App\Models\User;

class UpdateSubscriptionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'app:update-subscription-status';

    /**
     * The console command description.
     *
     * @var string
     */
    // protected $description = 'Command description';

    /**
     * Execute the console command.
     */

     protected $signature = 'subscription:update-status';

     protected $description = 'Update the isActive status for expired subscriptions';
 
     public function handle()
     {
         $expiredSubscriptions = SubscriptionsHistory::where('expire_date', '<', Carbon::now())
             ->where('isActive', true) // Only update active subscriptions
             ->get();
        
        info("subscription start command :=>".$expiredSubscriptions);

         foreach ($expiredSubscriptions as $subscription) {
             $subscription->update(['isActive' => false]);

     
             // Update users' table for users with this subscription_id and user_id
           User::where('subscription_id', $subscription->subscription_id)
                ->where('id', $subscription->user_id)
                ->update([
                    'subscription_id' => 0,
                    'subscription_validity' => null,
                    'subscribed_on' => null,
                 ]);
                info("subscription :=>".$subscription->subscription_id);

         }
     
         $this->info('Subscription status updated for expired subscriptions.');
     }
}
