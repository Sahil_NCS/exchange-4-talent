<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{ 
    // use HasMediaTrait;
    
    protected $fillable = ['user_id', 'video_path'];

    // Define the relationship with the User model
    public function user()
    {
        return $this->belongsTo(User::class);
    }

   

    // public function registerMediaConversions(Media $media = null): void
    // {
    //     $this->addMediaConversion('thumbnail')
    //         ->extractVideoFrameAtSecond(5) // Extracts the frame at 5 seconds, change it to your desired time
    //         ->performOnCollections('videos');
    // }
}
