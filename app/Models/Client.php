<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'recuriter_id','description','category_ids','category_title','start_date','end_date','audition_date'];

    public function posts(){
        return $this->belongToMany(Client::class);
    }

    public function recruiter(){
        return $this->hasOne(Recruiter::class,'id','recuriter_id');
    }
}
