<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Media extends Model implements HasMedia
{
    use HasFactory , InteractsWithMedia;

    protected $fillable = ['model_type','model_id','uuid', 'collection_name', 'name','file_name', 'mime_type', 'disk', 'conversions_disk', 'size', 'manipulations','custom_properties', 'generated_conversions', 'responsive_images', 'order_column'];
    

}
