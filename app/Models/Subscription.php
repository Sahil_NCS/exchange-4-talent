<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;
    protected $table = 'subscriptions';

    protected $fillable = [
        'title', 'description', 'price', 'validity' , 'images','images_limit','videos_limit','color_code','videos', 'contacts', 'view_email',
        'view_mobile', 'image_changes', 'video_changes', 'max_images_size',
        'max_video_size',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];
}
