<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Recruiter extends Authenticatable
{
    use HasFactory;

    protected $table = "recruiter";

    protected $fillable = ['email',  'password','name',
    'description',
    'logo',
    'color_code',
    'url',
    'link',
    'type',];

    protected $hidden = ['password',  'remember_token'];
}
