<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Category extends Model
{
    use HasFactory,Searchable;

    protected $table = 'categories';

    protected $fillable = [
        'title', 'description', 'image','color_code'
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];


    public function subcategories(){
        return $this->hasMany(Subcategory::class);
    }
    

    public function user(){
        return $this->hasMany(User::class);
    }


    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'description' => $this->description
        ];
    }
}
