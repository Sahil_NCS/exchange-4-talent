<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    use HasFactory;

    protected $fillable = ['rating', 'review', 'user_id', 'talent_id'];
    
    // Define the relationships if needed
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    
    public function talent()
    {
        return $this->belongsTo(User::class);
    }
}
