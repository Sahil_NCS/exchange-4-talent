<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Subcategory extends Model
{
    use HasFactory,Searchable;
    protected $table = 'subcategories';

    protected $fillable = [
        'category_id', 'title', 'description', 'image',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function attribute(){

        return $this->hasMany(Attribute::class);
    }

    public function user(){
        return $this->hasMany(User::class);
    }

    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'description' => $this->description
        ];
    }
}
