<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    protected $fillable = ['type', 'tags_name'];

    // If you want to include timestamps in your model, you can use
    public $timestamps = true;
}
