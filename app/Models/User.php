<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Scout\Searchable;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class User extends Authenticatable implements HasMedia
// implements HasMedia
{
    use HasApiTokens, HasFactory, Notifiable, Searchable, InteractsWithMedia ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'firebase_id', 'name','description', 'mobile', 'email','phone_code','mobile_otp', 'email_otp',
        'mobile_verified', 'email_verified', 'avatar','image1','image2','image3','image4','image5','image6','image7', 'dob', 'talent_type', 'gender',
        'address','country','state','city', 'password','facebook_link','instagram_link','linkedIn_link','wikipedia_link','imdb_link', 'remember_token', 'category_id', 'subcategory_id',
        'trade_barter', 'subscription_id', 'subscription_validity', 'subscribed_on','booking_fee','flag'
    ];

    // protected $dates = [
    //     'mobile_verified', 'email_verified', 'dob', 'subscription_validity',
    //     'subscribed_on', 'created_at', 'updated_at',
    // ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'attribute_user')
            ->withPivot('value')
            ->withTimestamps();
    }
    
    // public function favourite()
    // {
    //     return $this->belongsToMany(Favourite::class);
    // }

    public function favourite()
    {
        return $this->belongsToMany(User::class,'favourites','user_id','talent_id');
    }

    public function toSearchableArray()
    {
        // return [
        //     'name' => $this->name,
        //     'email' => $this->email,
        //     'phone' => $this->phone,
        // ];

        $array = $this->toArray();
 
        // Customize the data array...
 
        return $array;
    }



    public function getIsActiveAttribute()
    {
        return $this->subscription_validity >= now();
    }


    public function images()
    {
        return $this->hasMany(Image::class); // Assuming the Image model name is 'Image'
    }


    public function videos()
    {
        return $this->hasMany(Video::class); // Assuming the Image model name is 'Image'
    }


    public function userMedia(): MorphMany
    {
        return $this->morphMany(Media::class, 'model');
    }
    // public function registerMediaConversions(Media $media = null): void
    // {
    //     $this->addMediaConversion('thumbnail')
    //         ->width(200) // Set the width you want for the thumbnail
    //         ->height(200) // Set the height you want for the thumbnail
    //         ->performOnCollections('images');
    // }
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
        ->width(500)
        ->height(350)
        ->quality(90) // Adjust this value to control JPEG quality
        ->sharpen(5);
              

            //   $this->addMediaCollection('avatar')
            //   ->singleFile() // Assuming each user has a single avatar
            //   ->useDisk('s3');


            $this->addMediaConversion('high_quality')
            ->width(800)
            ->height(600)
            ->quality(90) // Adjust this value to control JPEG quality
            ->sharpen(5);


            // Define a conversion named 'thumbnail' for generating video thumbnails
            $this->addMediaConversion('thumbnail')
            ->format('jpg') // Set the format for the thumbnail
            ->performOnCollections('subscription-videos') // Apply this conversion only on the 'videos' collection
            ->width(200)
            ->height(150)
            ->extractVideoFrameAtSecond(5); // Extract thumbnail from the 5th second of the video
            
    }

    public function posts()
{
    return $this->belongsToMany(Client::class, 'user_post_mappings', 'user_id', 'post_id');
}

    
}
