<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPostMapping extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'post_id'
    ];

    public function user(){
        return $this->belongsToMany(Post::class);
    }

    public function post(){
        return $this->belongToMany(User::class);
    }
}
