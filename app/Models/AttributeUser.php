<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeUser extends Model
{
    use HasFactory;
    protected $table = 'attribute_user';

    protected $fillable = [
        'attribute_id', 'user_id', 'value',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];

    public function attribute()
    {
        return $this->belongsTo(Attribute::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
