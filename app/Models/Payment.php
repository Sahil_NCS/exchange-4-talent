<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $fillable = [
        // Define all the columns that can be mass assigned here
        'user_id', 'subscription_id', 'order_id', 'tracking_id', 'bank_ref_no', 'order_status',
        'payment_mode','paymentType', 'card_name', 'status_code', 'status_message', 'currency', 'billing_name',
        'billing_address', 'billing_state', 'billing_zip', 'billing_country', 'billing_tel',
        'billing_email', 'trans_date', 'token_eligibility', 'response_code', 'amount',
        'ccavenue_status',
    ];
}
