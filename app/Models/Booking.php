<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $table = 'bookings';

    protected $fillable = [
        'user_id', 'talent_id', 'booking_date', 'booking_fee', 'booking_type',
        'appointment_date', 'payment_done', 'payment_date', 'payment_type',
    ];

    protected $dates = [
        'booking_date', 'appointment_date', 'payment_date', 'created_at', 'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function talent()
    {
        return $this->belongsTo(User::class);
    }
}
