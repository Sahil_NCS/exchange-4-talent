<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;
    protected $table = 'attributes';

    protected $fillable = [
        'title', 'data_type', 'subcategory_id',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];
    
    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'attribute_user')
            ->withPivot('value')
            ->withTimestamps();
    }
}
