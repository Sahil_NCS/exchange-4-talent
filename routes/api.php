<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CategoryControllerAPI;
use App\Http\Controllers\API\AttributeControllerAPI;
use App\Http\Controllers\API\SubcategoryControllerAPI;
use App\Http\Controllers\API\SubscriptionControllerAPI;
use App\Http\Controllers\API\AttributeUserControllerAPI;
use App\Http\Controllers\API\UsersControllerAPI;
use App\Http\Controllers\API\BookingControllerAPI;
use App\Http\Controllers\API\FavouriteControllerAPI;
use App\Http\Controllers\API\HomeControllerAPI;
use App\Http\Controllers\API\SearchControllerAPI;
use App\Http\Controllers\API\BannerControllerAPI;
use App\Http\Controllers\API\AuthControllerAPI;
use App\Http\Controllers\API\RatingControllerAPI;
use App\Http\Controllers\API\TagsControllerAPI;
use App\Http\Controllers\API\CityControllerAPI;
use App\Http\Controllers\API\CountryControllerAPI;
use App\Http\Controllers\API\StateControllerAPI;
use App\Http\Controllers\API\SubscriptionsHistoryAPIController;
use App\Http\Controllers\API\CredentialControllerAPI;
use App\Http\Controllers\API\RecruitersController;
use App\Http\Controllers\API\AdsControllerAPI;
use App\Http\Controllers\API\FeedBackFormControllerAPI;
use App\Http\Controllers\API\RecruiterPostControllerAPI;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::get('/recruiter', [RecruitersController::class, 'index']);
Route::post('/addrecruiter', [RecruitersController::class, 'store']);
Route::post('/updaterecruiter/{recruiter}/update', [RecruitersController::class, 'update']);
Route::delete('/recruiter/{recruiter}/destroy', [RecruitersController::class, 'destroy']);



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});




Route::post('/login', [AuthControllerAPI::class, 'login']);
Route::post('v4/login', [AuthControllerAPI::class, 'login1']);
Route::post('v3/login', [AuthControllerAPI::class, 'login2']);
Route::post('/otp', [AuthControllerAPI::class, 'mobile_otp_check']);
Route::post('v1/otp', [AuthControllerAPI::class, 'mobile_otp_check1']);
Route::post('v3/otp', [AuthControllerAPI::class, 'mobile_otp_check2']);
// Route::post('/logout', [AuthControllerAPI::class, 'logout'])->name('logout');

Route::middleware('auth:sanctum')->group(function () {
// AttributesUsers API routes
Route::get('/attribute_user', [AttributeUserControllerAPI::class, 'index']);
Route::post('/attribute_users', [AttributeUserControllerAPI::class, 'store']);
Route::put('/attribute_user/{attribute_user}', [AttributeUserControllerAPI::class, 'update']);
Route::delete('/attribute_users/{attribute_user}', [AttributeUserControllerAPI::class, 'destroy']);

// Attributes API routes
Route::get('/attributes', [AttributeControllerAPI::class, 'index']);
Route::post('/attributes', [AttributeControllerAPI::class, 'store']);
Route::put('/attributes/{attribute}', [AttributeControllerAPI::class, 'update']);
Route::delete('/attributes/{attribute}', [AttributeControllerAPI::class, 'destroy']);


// Users API routes
Route::get('/users', [UsersControllerAPI::class, 'index']);

Route::get('v2/users', [UsersControllerAPI::class, 'index1']);
Route::post('/addusers', [UsersControllerAPI::class, 'store']);
Route::post('/updateusers/{users}/update', [UsersControllerAPI::class, 'update']);
Route::post('v1/updateusers/{users}/update', [UsersControllerAPI::class, 'update1']);
Route::post('v2/updateusers/{users}/update', [UsersControllerAPI::class, 'update2']);
Route::delete('/users/destroy', [UsersControllerAPI::class, 'destroy']);
Route::post('logout', [UsersControllerAPI::class, 'logout'])->name('logout');
Route::post('logout1', [UsersControllerAPI::class, 'logout1'])->name('logout');
//hireTalent API
Route::post('/hireTalent', [UsersControllerAPI::class, 'hireTalent']);
Route::post('v1/hireTalentBySubscriptionUsers', [UsersControllerAPI::class, 'hireTalentBySubscriptionUsers']);
Route::post('v2/hireTalentBySubscriptionUsers', [UsersControllerAPI::class, 'hireTalentBySubscriptionUsers1']);


Route::post('v1/uploadImages', [UsersControllerAPI::class, 'uploadImages']);
Route::post('v1/updateImage/{imageId}', [UsersControllerAPI::class, 'updateImage']);
Route::delete('v1/deleteImage/{imageId}', [UsersControllerAPI::class, 'deleteImage']);


Route::post('v1/uploadVideos', [UsersControllerAPI::class, 'uploadVideos']);

// get single User
Route::post('getSingleUser', [UsersControllerAPI::class, 'getSingleUser']);
Route::post('v1/getSingleUser', [UsersControllerAPI::class, 'getSingleUser1']);
Route::post('v2/getSingleUser', [UsersControllerAPI::class, 'getSingleUser2']);
Route::post('v3/getSingleUser', [UsersControllerAPI::class, 'getSingleUser3']);

Route::post('/addImage', [UsersControllerAPI::class, 'addImage']);
Route::post('/fcm-token', [UsersControllerAPI::class, 'fcm_Token_update'])->name('fcmToken');
Route::post('/chatNotification', [UsersControllerAPI::class,'chatNotification'])->name('chatNotification');

// barter
Route::get('v2/availableBarterUsers', [UsersControllerAPI::class, 'availableBarterUsers']);

// Route::get('subcategories/{category}', 'SubcategoryController@getSubcategoriesByCategory');
// Route::patch('/users/{users}/update', [UsersControllerAPI::class, 'update']);



Route::post('/update-selected-country', [CountryControllerAPI::class, 'updateSelectedCountry']);
Route::post('/update-selected-state', [StateControllerAPI::class, 'updateSelectedState']);
Route::post('/update-selected-city', [CityControllerAPI::class, 'updateSelectedCity']);



// Bookings API routes
Route::get('/bookings', [BookingControllerAPI::class, 'index']);
Route::post('/bookings', [BookingControllerAPI::class, 'store']);
Route::put('/bookings/{bookings}', [BookingControllerAPI::class, 'update']);
Route::delete('/bookings/{bookings}', [BookingControllerAPI::class, 'destroy']);


// Bookings API routes
Route::get('/favourites', [FavouriteControllerAPI::class, 'index']);
Route::get('v2/favourites', [FavouriteControllerAPI::class, 'index1']);
Route::post('/addfavourites', [FavouriteControllerAPI::class, 'store']);
// Route::put('/favourites/{favourites}', [FavouriteControllerAPI::class, 'update']);
Route::delete('/favourites/{favourites}/destroy', [FavouriteControllerAPI::class, 'destroy']);

// Subcategories API routes
Route::get('/subcategories', [SubcategoryControllerAPI::class, 'index']);
Route::post('/subcategories', [SubcategoryControllerAPI::class, 'store']);
Route::post('/subcategories/{subcategory}/update', [SubcategoryControllerAPI::class, 'update']);
Route::delete('/subcategories/{subcategory}/destroy', [SubcategoryControllerAPI::class, 'destroy']);


// Subscription API routes
Route::get('/subscriptions', [SubscriptionControllerAPI::class, 'index']);
Route::post('v1/subscriptions', [SubscriptionControllerAPI::class, 'index1']);
Route::post('/subscriptions', [SubscriptionControllerAPI::class, 'store']);
Route::put('/subscriptions/{subscription}/update', [SubscriptionControllerAPI::class, 'update']);
Route::delete('/subscriptions/{subscription}/destroy', [SubscriptionControllerAPI::class, 'destroy']);

Route::post('/checkout', [SubscriptionControllerAPI::class, 'checkout']);
Route::post('v1/checkout', [SubscriptionControllerAPI::class, 'checkout1']);
Route::post('v2/checkout', [SubscriptionControllerAPI::class, 'checkout2']);

Route::post('/verify_coupne', [SubscriptionControllerAPI::class, 'verify_coupne']);
Route::post('v3/verify_coupne', [SubscriptionControllerAPI::class, 'verify_coupne2']);

Route::post('v1/subscriptionByusers', [SubscriptionControllerAPI::class, 'subscriptionByusers']);
Route::post('v2/subscriptionByusers', [SubscriptionControllerAPI::class, 'subscriptionByusers1']);
Route::post('v3/subscriptionByusers', [SubscriptionControllerAPI::class, 'subscriptionByusers2']);

// SubscriptionsHistoryAPIController API routes
Route::post('/userSubscriptionsHistory', [SubscriptionsHistoryAPIController::class, 'userSubscriptionsHistory']);
Route::post('v3/userSubscriptionsHistory', [SubscriptionsHistoryAPIController::class, 'userSubscriptionsHistory1']);
// Categories API routes
Route::get('/categories', [CategoryControllerAPI::class, 'index']);
Route::get('v2/categories', [CategoryControllerAPI::class, 'index1']);

Route::post('/categories', [CategoryControllerAPI::class, 'store']);
Route::post('/categories/{category}/update', [CategoryControllerAPI::class, 'update']);
Route::delete('/categories/{category}/destroy', [CategoryControllerAPI::class, 'destroy']);

Route::get('/categoryTitle',[CategoryControllerAPI::class,'categorytitle']);
Route::get('/subcategoryBycategories/{categoryId}',[CategoryControllerAPI::class, 'getSubcategoriesByCategory']);
Route::get('/getUsersBySubcategory/{subcategoryId}',[CategoryControllerAPI::class, 'getUsersBySubcategory']);
Route::get('/allsubcategoryBycategories/{categoryId}',[CategoryControllerAPI::class, 'getAllSubcategoriesByCategory']);


Route::post('v1/getUsersByCategory',[CategoryControllerAPI::class, 'getUsersByCategory']);

Route::post('v3/getUsersByCategory',[CategoryControllerAPI::class, 'getUsersByCategory2']);
Route::post('v4/getUsersByCategory',[CategoryControllerAPI::class, 'getUsersByCategory3']);

Route::post('v1/getSubcategoriesByCategory1',[CategoryControllerAPI::class, 'getSubcategoriesByCategory1']);

Route::post('v3/getSubcategoriesByCategory2',[CategoryControllerAPI::class, 'getSubcategoriesByCategory2']);

//Home Page API routes
Route::get('/home', [HomeControllerAPI::class, 'index']);

Route::get('v1/home', [HomeControllerAPI::class, 'index1']);
Route::get('v2/home', [HomeControllerAPI::class, 'index2']);
//Search Page API routes
Route::get('/search/categories', [SearchControllerAPI::class, 'index']);
Route::get('/search', [SearchControllerAPI::class, 'search']);
Route::post('v2/search', [SearchControllerAPI::class, 'search1']);
Route::post('v4/search', [SearchControllerAPI::class, 'search2']);
// Route::match(['get', 'post'], 'v2/search', [SearchControllerAPI::class, 'search1'])->name('v2/search');


//Banner Page API routes
Route::get('/banners', [BannerControllerAPI::class, 'index']);
Route::post('/banners', [BannerControllerAPI::class, 'store']);
Route::post('/banners/{banner}/update', [BannerControllerAPI::class, 'update']);
Route::delete('/banners/{banner}/destroy', [BannerControllerAPI::class, 'destroy']);

Route::post('v1/banners', [BannerControllerAPI::class, 'store1']);

//Rating Page API routes
Route::post('/addRatingsReview', [RatingControllerAPI::class, 'store']);
Route::post('/allreview', [RatingControllerAPI::class, 'allreview']);

//Tag Page API routes
Route::get('/tags', [TagsControllerAPI::class, 'index']);
Route::post('/tags', [TagsControllerAPI::class, 'store']);
Route::put('/tags/{tags}/update', [TagsControllerAPI::class, 'update']);
Route::delete('/tags/{tags}/destroy', [TagsControllerAPI::class, 'destroy']);



Route::post('v1/getRazorCredential', [CredentialControllerAPI::class, 'getRazorCredential']);

Route::post('v1/payment', [UsersControllerAPI::class, 'payment']);

Route::post('/payment/check', [SubscriptionControllerAPI::class, 'paymentCheck']);

Route::post('v4/payment/check', [SubscriptionControllerAPI::class, 'paymentCheck1']);

// Feedback Form

Route::post('/feedbackform',[FeedBackFormControllerAPI::class,'feedbackform'])->name('feedbackform');

// recruiters
Route::post('/getRecruiterPostByUserId',[RecruiterPostControllerAPI::class,'getRecruiterPostByUserId'])->name('getRecruiterPostByUserId');
Route::post('/applyJobs',[RecruiterPostControllerAPI::class,'applyJobs'])->name('applyJobs');
Route::post('/appliedJobs',[RecruiterPostControllerAPI::class,'appliedJobs'])->name('appliedJobs');
Route::post('/getRecruiterSinglePost',[RecruiterPostControllerAPI::class,'getRecruiterSinglePost'])->name('getRecruiterSinglePost');
Route::post('/getRecruitersWithPostCount',[RecruiterPostControllerAPI::class,'getRecruitersWithPostCount'])->name('getRecruitersWithPostCount');
Route::post('/getPostsOfRecruiter',[RecruiterPostControllerAPI::class,'getPostsOfRecruiter'])->name('getPostsOfRecruiter');
Route::post('/getRecruitersWithPostCount1',[RecruiterPostControllerAPI::class,'getRecruitersWithPostCount1'])->name('getRecruitersWithPostCount1');

Route::post('/getActivePostsOfRecruiter',[RecruiterPostControllerAPI::class,'getActivePostsOfRecruiter'])->name('getActivePostsOfRecruiter');
Route::post('/getInActivePostsOfRecruiter',[RecruiterPostControllerAPI::class,'getInActivePostsOfRecruiter'])->name('getInActivePostsOfRecruiter');
Route::post('/removePost',[RecruiterPostControllerAPI::class,'removePost'])->name('removePost');
});

Route::post('/countries', [CountryControllerAPI::class, 'getCountries']);
Route::post('/states/{countryId}', [StateControllerAPI::class, 'getStatesByCountry']);
Route::post('/cities/{stateId}', [CityControllerAPI::class, 'getCitiesByState']);

// Ads API routes
Route::get('v1/ads', [AdsControllerAPI::class, 'index']);
Route::post('v1/addads', [AdsControllerAPI::class, 'store']);

Route::post('/availableCoupons', [SubscriptionControllerAPI::class, 'availableCoupons']);