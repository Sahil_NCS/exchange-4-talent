<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\CCavenuepgController;

use App\Http\Controllers\HomeController;

use App\Http\Controllers\Dashboard\UsersController;
use App\Http\Controllers\Dashboard\CategoryController;
use App\Http\Controllers\Dashboard\RecruiterController;
use App\Http\Controllers\Dashboard\AdController;
use App\Http\Controllers\Dashboard\CouponsController;
use App\Http\Controllers\Dashboard\PaymentController;
use App\Http\Controllers\Dashboard\StarryController;
use App\Http\Controllers\Dashboard\GoldController;
use App\Http\Controllers\Dashboard\SilverController;
use App\Http\Controllers\Dashboard\PlatinumController;
use App\Http\Controllers\Dashboard\SubscriptionPlanController;
use App\Http\Controllers\Recuriterdashboard\ClientController;
use App\Http\Controllers\Auth\RecruiterAuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [RegisterController::class, 'showRegistrationForm']);

Route::get('/privacy', function () {
    return view('privacy');
});


// Route::get('/', function () {
//     return "Welcome to Talent Management System";
// });


Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [RegisterController::class, 'register']);

Route::get('/success', [RegisterController::class, 'success'])->name('success');

Route::get('/view-records',[RegisterController::class, 'show']);

Route::get('/ccavenue-index',[CCavenuepgController::class, 'index']);


// Route::post('/ccavRequestHandler',[CCavenuepgController::class, 'ccavRequestHandler']);
Route::post('/ccavRequestHandler', [CCavenuepgController::class, 'ccavRequestHandler'])->name('ccavRequestHandler');

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/user/count', [HomeController::class, 'getUserCount']);

// Route::get('/image', [ImageController::class, 'index']);

Auth::routes([
    'register' => false,
]);

Route::middleware(['auth'])->group(function (){

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/users', [UsersController::class, 'index'])->name('users.index');
Route::get('/userregister', [UsersController::class, 'showRegistrationForm'])->name('userregisterform');
Route::post('/create/users', [UsersController::class, 'create'])->name('users.create');
Route::get('/users/{user}/edit', [UsersController::class, 'edit'])->name('users.edit');
Route::put('/updateusers/{users}/update', [UsersController::class, 'update'])->name('users.update');
Route::get('/users/{users}/destroy', [UsersController::class, 'destroy'])->name('users.destroy');



Route::get('/starry-eyed', [StarryController::class, 'index'])->name('starry.index');
Route::get('/starryregister', [StarryController::class, 'show'])->name('starryregisterform');
Route::post('/create/starry-eyed', [StarryController::class, 'create'])->name('starry.create');
Route::get('/starry-eyed/{user}/edit', [StarryController::class, 'edit'])->name('starry.edit');
Route::put('/updatestarry-eyed/{users}/update', [StarryController::class, 'update'])->name('starry.update');
Route::get('/starry-eyed/{users}/destroy', [StarryController::class, 'destroy'])->name('starry.destroy');

Route::get('/gold', [GoldController::class, 'index'])->name('gold.index');
Route::get('/goldregister', [GoldController::class, 'show'])->name('goldregisterform');
Route::post('/create/gold', [GoldController::class, 'create'])->name('gold.create');
Route::get('/gold/{user}/edit', [GoldController::class, 'edit'])->name('gold.edit');
Route::put('/updategold/{users}/update', [GoldController::class, 'update'])->name('gold.update');
Route::get('/gold/{users}/destroy', [GoldController::class, 'destroy'])->name('gold.destroy');


Route::get('/silver', [SilverController::class, 'index'])->name('silver.index');
Route::get('/silverregister', [SilverController::class, 'show'])->name('silverregisterform');
Route::post('/create/silver', [SilverController::class, 'create'])->name('silver.create');
Route::get('/silver/{user}/edit', [SilverController::class, 'edit'])->name('silver.edit');
Route::put('/updatesilver/{users}/update', [SilverController::class, 'update'])->name('silver.update');
Route::get('/silver/{users}/destroy', [SilverController::class, 'destroy'])->name('silver.destroy');


Route::get('/platinum', [PlatinumController::class, 'index'])->name('platinum.index');
Route::get('/platinumregister', [PlatinumController::class, 'show'])->name('platinumregisterform');
Route::post('/create/platinum', [PlatinumController::class, 'create'])->name('platinum.create');
Route::get('/platinum/{user}/edit', [PlatinumController::class, 'edit'])->name('platinum.edit');
Route::put('/updateplatinum/{users}/update', [PlatinumController::class, 'update'])->name('platinum.update');
Route::get('/platinum/{users}/destroy', [PlatinumController::class, 'destroy'])->name('platinum.destroy');


// category routes
Route::get('categories', [CategoryController::class, 'index'])->name('categories.index');
Route::get('categories/create', [CategoryController::class, 'create'])->name('categories.create');
Route::post('categories', [CategoryController::class, 'store'])->name('categories.store');
Route::get('categories/{category}/edit', [CategoryController::class, 'edit'])->name('categories.edit');
Route::put('categories/{category}', [CategoryController::class, 'update'])->name('categories.update');
Route::get('categories/{category}', [CategoryController::class, 'destroy'])->name('categories.destroy');



// Recruiter routes
Route::get('recruiters', [RecruiterController::class, 'index'])->name('recruiters.index');
Route::get('recruiters/create', [RecruiterController::class, 'create'])->name('recruiters.create');
Route::post('recruiters', [RecruiterController::class, 'store'])->name('recruiters.store');
Route::get('recruiters/{recruiter}/edit', [RecruiterController::class, 'edit'])->name('recruiters.edit');
Route::put('recruiters/{recruiter}', [RecruiterController::class, 'update'])->name('recruiters.update');
Route::get('recruiters/{recruiter}', [RecruiterController::class, 'destroy'])->name('recruiters.destroy');

//  Ads routes
Route::get('/ads', [AdController::class, 'index'])->name('ads.index');
Route::get('/ads/create', [AdController::class, 'create'])->name('ads.create');
Route::post('/ads', [AdController::class, 'store1'])->name('ads.store');
Route::get('/ads/{ad}/edit', [AdController::class, 'edit'])->name('ads.edits.test');
Route::put('/ads/{ad}', [AdController::class, 'update'])->name('ads.update');
Route::get('/ads/{ad}', [AdController::class, 'destroy'])->name('ads.destroy');

//  Coupon Routes
Route::get('/coupons', [CouponsController::class, 'index'])->name('coupons.index');
Route::get('/coupons/create', [CouponsController::class, 'create'])->name('coupons.create');
Route::post('/coupons', [CouponsController::class, 'store'])->name('coupons.store');
Route::get('/coupons/{coupon}/edit', [CouponsController::class, 'edit'])->name('coupons.edit');
Route::put('/coupons/{coupon}', [CouponsController::class, 'update'])->name('coupons.update');
Route::get('/coupons/{coupon}', [CouponsController::class, 'destroy'])->name('coupons.destroy');

//  Payment Routes
Route::get('/payments', [PaymentController::class, 'index'])->name('payments.index');

//  Subscription Routes
Route::get('/subscription', [SubscriptionPlanController::class, 'index'])->name('subscription.index');
Route::get('/subscription/create', [SubscriptionPlanController::class, 'create'])->name('subscription.create');
Route::post('/subscription', [SubscriptionPlanController::class, 'store'])->name('subscription.store');
Route::get('/subscription/{subscriptionPlan}/edit', [SubscriptionPlanController::class, 'edit'])->name('subscription.edit');
Route::put('/subscription/{subscriptionPlan}', [SubscriptionPlanController::class, 'update'])->name('subscription.update');
Route::get('/subscription/{subscriptionPlan}', [SubscriptionPlanController::class, 'destroy'])->name('subscription.destroy');


});

Route::get('/recruiter/login', [RecruiterAuthController::class, 'showLoginForm'])->name('recruiter.login');
Route::post('/recruiter/login', [RecruiterAuthController::class,'login'])->name('recruiter.login.post');
Route::post('/recruiter/logout', [RecruiterAuthController::class,'logout'])->name('recruiter.logout');

//recruiter Home page after login
Route::group(['middleware' => ['web', 'recruiter']], function() {
    Route::get('/recruiter/home', [RecruiterAuthController::class,'index']);
    Route::get('/recruiter/id', [ClientController::class,'checkid']);


    Route::get('/clients', [ClientController::class, 'index'])->name('clients.index');
    Route::get('/clients/create', [ClientController::class, 'create'])->name('clients.create');
    Route::post('/clients', [ClientController::class, 'store'])->name('clients.store');
    Route::get('/clients/{client}/edit', [ClientController::class, 'edit'])->name('clients.edit');
    Route::put('/clients/{client}', [ClientController::class, 'update'])->name('clients.update');
    Route::get('/clients/{client}', [ClientController::class, 'destroy'])->name('clients.destroy');

    Route::get('/clients/{client}', [ClientController::class, 'destroy'])->name('clients.destroy');

    Route::post('clients/updateStatus/{client}', [ClientController::class,'updateStatus'])->name('clients.updateStatus');
    // Route::put('clients/updateStatus/{client}', 'ClientController@updateStatus')->name('clients.updateStatus');

    Route::get('/getAppliedUser', [ClientController::class,'getAppliedUser'])->name('clients.getAppliedUser');
    Route::post('/updateUserStatus', [ClientController::class,'updateUserStatus'])->name('updateUserStatus');
   

});