<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsHistoryTable extends Migration
{
    public function up()
    {
        Schema::create('subscriptions_history', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('subscription_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        });

        // If you have already created the 'subscriptions' table, you can uncomment the following line
        // Schema::table('subscriptions_history', function (Blueprint $table) {
        //     $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
        // });
    }

    public function down()
    {
        Schema::dropIfExists('subscriptions_history');
    }
}