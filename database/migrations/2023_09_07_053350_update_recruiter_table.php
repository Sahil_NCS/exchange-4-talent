<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRecruiterTable extends Migration
{
    public function up()
    {
        Schema::table('recruiter', function (Blueprint $table) {
            $table->string('email')->unique()->after('type');
            $table->string('password')->after('email');
            $table->rememberToken()->after('password');
        });
    }

    public function down()
    {
        Schema::table('recruiter', function (Blueprint $table) {
            $table->dropUnique('email');
            $table->dropColumn('password');
            $table->dropColumn('remember_token');
        });
    }
}
